(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 11.0' *)

(*************************************************************************)
(*                                                                       *)
(*                                                                       *)
(*  This file was created under the Wolfram Enterprise licensing terms.  *)
(*                                                                       *)
(*       For additional information concerning CDF licensing see:        *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*                                                                       *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[    333389,       8216]
NotebookOptionsPosition[    321826,       7994]
NotebookOutlinePosition[    322371,       8016]
CellTagsIndexPosition[    322328,       8013]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Assembly language compilation", "Title",ExpressionUUID->"201660c2-92e8-4e15-8dcb-9b99754ea4dc"],

Cell[CellGroupData[{

Cell["AS Computer Science, South Devon College", "Chapter",ExpressionUUID->"03a0a00b-b0e6-4866-954c-fcee234b8af7"],

Cell["\<\
Copyright \[Copyright] Matthew Fairtlough & South Devon College, November, \
2017\
\>", "Text",ExpressionUUID->"037f078b-a1b4-4ca9-8a50-c87f42294165"],

Cell[CellGroupData[{

Cell["This directory:", "Subsubsection",ExpressionUUID->"9ce0362a-8db3-453f-93d9-9e426f612ef8"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NotebookDirectory", "[", "]"}]], "Code",
 InitializationCell->
  True,ExpressionUUID->"f6f41542-74bb-4f5e-831e-91d06bdd8aa9"],

Cell[BoxData["\<\"/Users/matthew/southDevonCollege/CS/CAS/\"\>"], "Output",ExpressionUUID->"4d80afa2-affa-4858-8bac-80e533b0acf1"]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["This file:", "Subsubsection",ExpressionUUID->"07fa5cb8-3dae-4efe-a154-3f926c175961"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NotebookFileName", "[", "]"}]], "Code",ExpressionUUID->"0b8e8ab5-a533-4c7c-a476-3fc45cd4585e"],

Cell[BoxData["\<\"/Users/matthew/southDevonCollege/CS/CAS/AQA_assembler_for_\
CAS.cdf\"\>"], "Output",ExpressionUUID->"5fbd6e65-8cae-4c1b-8b6b-\
62243820b369"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Version and word length", "Subsubsection",ExpressionUUID->"008e5049-00ea-4b3a-b738-4ebfbce95d68"],

Cell["Version 2.21w1 (word version)", "Text"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"$wordLength", "=", "16"}], ";"}], " "}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "change", " ", "me", " ", "before", " ", "activating", " ", "assembler", 
    " ", "to", " ", "alter", " ", "the", " ", "word", " ", "length"}], " ", 
   "*)"}]}]}], "Code",
 InitializationCell->
  True,ExpressionUUID->"ce5a2722-8515-42b7-b4f1-d6859bb221df"],

Cell[BoxData[
 RowBox[{
  RowBox[{"$version", "=", "\"\<2.21w1\>\""}], ";"}]], "Code",ExpressionUUID->\
"b971c2a9-9e74-43e6-8c03-8ef3c816f375"],

Cell[BoxData[
 RowBox[{
  RowBox[{"$assemblyVersion", "=", "2.21"}], ";"}]], "Code",ExpressionUUID->\
"ec9bfa1c-9544-4242-8c03-36d68617ddc4"]
}, Closed]],

Cell[CellGroupData[{

Cell["Activate assembler (for full Mathematica version)", "Subsubsection",ExpressionUUID->"d725a7b2-4bff-4efd-a197-f13156733a60"],

Cell["\<\
Click the button below to evaluate initialization cells to add the code for \
assembly language execution to this and other notebooks:\
\>", "Text",ExpressionUUID->"6026ea4f-b730-4a95-98e8-ceacfeb007db"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Button", "[", 
  RowBox[{"\"\<Activate assembler\>\"", ",", 
   RowBox[{"FrontEndExecute", "[", 
    RowBox[{"FrontEndToken", "[", 
     RowBox[{
      RowBox[{"InputNotebook", "[", "]"}], ",", 
      "\"\<EvaluateInitialization\>\""}], "]"}], "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 ButtonBox["\<\"Activate assembler\"\>",
  Appearance->Automatic,
  ButtonFunction:>FrontEndExecute[
    FrontEndToken[
     InputNotebook[], "EvaluateInitialization"]],
  Evaluator->Automatic,
  Method->"Preemptive"]], "Output",ExpressionUUID->"cf007ccb-27b7-4dc6-8894-\
adda7a186e84"]
}, {2}]],

Cell[TextData[{
 "Note that this should be done ",
 StyleBox["before",
  FontSlant->"Italic"],
 " loading any code below."
}], "Text",ExpressionUUID->"b43b87ab-a5f9-4069-8746-ea0cffad3ed0"],

Cell["\<\
Now continue to explore the assembler or return to your code...\
\>", "Text",ExpressionUUID->"f15b3aaa-8dd7-4c16-aa34-8f66b2389877"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Introduction", "Section",ExpressionUUID->"5c2a3222-c7eb-4037-a159-d58f45ca9fe2"],

Cell["\<\
This notebook implements an assembler for the assembly language specified for \
the AQA A level in Computer Science.\
\>", "Text",ExpressionUUID->"f9425d64-983c-40a8-b637-930e91a22d96"],

Cell[CellGroupData[{

Cell["Limitations and variations from the published specifications", \
"Subsubsection",ExpressionUUID->"58b67e43-0ea4-44aa-a940-88514b524587"],

Cell["You can now include blank lines in your code.", "Text",ExpressionUUID->"9f667fe6-1d27-4937-a710-6dca6e566117"],

Cell["\<\
Comments in code are now supported; they must begin and end with \
\[OpenCurlyQuote];\[CloseCurlyQuote].  The colon after an instruction label \
must have at least one space before and after.  I recommend exactly one space \
in each case for maximum compatibility with the published syntax.  \
\>", "Text",ExpressionUUID->"e6585e7c-a59a-45f1-8aaf-5ceee4645c39"],

Cell["See the code examples below for what is required.", "Text",ExpressionUUID->"92414e67-4d74-47ac-b487-0c25c17573bb"],

Cell["\<\
Support for binary or hexadecimal notation for numerical literals is not yet \
provided; all values are in decimal. This accords with the format of exam \
questions, by the way.  Bit shifts and logical operations work by first \
translating to binary, then carrying out the operation and finally \
translating back to decimal.  Arithmetic overflow may result in negative \
decimal numbers.\
\>", "Text",ExpressionUUID->"40620cb3-f24f-4e2f-b24b-20393e6321fe"]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Coding", "Chapter",ExpressionUUID->"f06d27b6-f0f5-45b4-9236-f76f3670f688"],

Cell[CellGroupData[{

Cell["Setup", "Section",
 InitializationGroup->True],

Cell[CellGroupData[{

Cell["Load and save", "Subsubsection"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"originalDirectory", "=", 
  RowBox[{"Directory", "[", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"NotebookDirectory", "[", "]"}]}], "Code",ExpressionUUID->"b3e9acc0-\
65e2-4ba2-bf22-498801a7a3c4"],

Cell[BoxData["\<\"/Users/matthew\"\>"], "Output",ExpressionUUID->"1ad0687e-c460-44e2-abbd-61b982ddcf4b"],

Cell[BoxData["\<\"/Users/matthew/southDevonCollege/CS/CAS/\"\>"], "Output",ExpressionUUID->"3e70ad2d-2d97-4905-823d-c340c2cc820d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SetDirectory", "[", 
  RowBox[{"NotebookDirectory", "[", "]"}], "]"}]], "Code",ExpressionUUID->\
"50594c05-d359-456a-a3d2-24918dfe60e7"],

Cell[BoxData["\<\"/Users/matthew/southDevonCollege/CS/CAS\"\>"], "Output",ExpressionUUID->"ec663dca-b721-4404-847a-c28691eb5870"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<FunctionalParsers`\>\"", "]"}]], "Code",Expression\
UUID->"8832f2b7-a9bd-49e1-8603-945deca17feb"],

Cell[BoxData[
 TemplateBox[{
  "Get","noopen",
   "\"Cannot open \
\\!\\(\\*RowBox[{\\\"\\\\\\\"FunctionalParsers`\\\\\\\"\\\"}]\\).\"",2,9,1,
   18130571224170341501,"Player"},
  "MessageTemplate"]], "Message", \
"MSG",ExpressionUUID->"2a29adf8-d038-4b54-895f-a1e64b7e75f4"],

Cell[BoxData[
 TemplateBox[{
  "Needs","nocont",
   "\"Context \
\\!\\(\\*RowBox[{\\\"\\\\\\\"FunctionalParsers`\\\\\\\"\\\"}]\\) was not \
created when Needs was evaluated.\"",2,9,2,18130571224170341501,"Player"},
  "MessageTemplate"]], "Message", \
"MSG",ExpressionUUID->"d9f4f6dd-c73f-4262-9a08-2c47334a10b5"],

Cell[BoxData["$Failed"], "Output",ExpressionUUID->"a05af518-9e09-42b7-b0ea-f0ff2c710352"]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["Utilities", "Subsubsection",ExpressionUUID->"a6798124-2b60-4d44-9bae-76e6227e7053"],

Cell[BoxData[
 RowBox[{
  RowBox[{"mkConst", "[", "f_", "]"}], ":=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"ClearAll", "[", "f", "]"}], ";", 
    RowBox[{
     RowBox[{"f", "[", "]"}], ":=", "f"}]}], ")"}]}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"6c8d5fdf-3982-4751-ab7a-eedd70c1ec83"],

Cell[BoxData[
 RowBox[{
  RowBox[{"mkConst", "/@", 
   RowBox[{"{", 
    RowBox[{
    "asmBcond", ",", "asmMemRef", ",", "asmInstr", ",", "asmReg", ",", 
     "asmLabel", ",", "asmOperand", ",", "asmLiteral", ",", "asmLine"}], 
    "}"}]}], ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"1bb7769d-03af-48b8-8cd8-edabdd4c4ca0"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"Clear", "[", 
   RowBox[{"asmBcond", ",", "asmRegProcess", ",", "asmLiteralProcess"}], 
   "]"}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "it", " ", "is", " ", "possible", " ", "just", " ", "to", " ", "set", " ", 
    "up", " ", "the", " ", "computations", " ", "that", " ", "the", " ", 
    "functions", " ", "do", " ", "in", " ", "advance", " ", 
    RowBox[{"e", ".", "g", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"asmBcond", "[", "\"\<BEQ\>\"", "]"}], ":=", "asmBranchOnEq"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"asmBcond", "[", "p_", "]"}], ":=", "p"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"asmRegProcess", "=", 
   RowBox[{
    RowBox[{"asmReg", "[", 
     RowBox[{"ToExpression", "[", 
      RowBox[{"StringDrop", "[", 
       RowBox[{"#", ",", "1"}], "]"}], "]"}], "]"}], "&"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"asmLiteralProcess", "[", "n_", "]"}], ":=", 
  RowBox[{"asmLiteral", "[", 
   RowBox[{"to2sComplement", "[", 
    RowBox[{"n", ",", "$wordLength"}], "]"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "this", " ", "is", " ", "not", " ", "perfect", " ", "but", " ", 
    "represents", " ", "a", " ", "compromise", " ", "solution"}], " ", 
   "*)"}]}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"afcbc36c-2dae-42b4-ba05-64be0f9c928e"]
}, Closed]]
}, Closed]],

Cell[CellGroupData[{

Cell["The parser generator", "Section"],

Cell[CellGroupData[{

Cell["The grammar of the assembly language", "Subsection"],

Cell[CellGroupData[{

Cell["Grammar for recognition only", "Subsubsection"],

Cell[TextData[{
 "This grammar is in EBNF (Extended Backus-Naur Form), which is a common \
format for specifying and implementing grammars.  BNF is not an AS-level \
topic but is useful background information and will be examined in the A \
level (but not ",
 StyleBox["Extended",
  FontSlant->"Italic"],
 " BNF).  It formally specifies the grammar of the assembly language \
implemented in this notebook.  The grammar below does not specify a result \
for the parser; it just parses it, essentially returning a yes/no answer, \
where \[OpenCurlyQuote]yes\[CloseCurlyQuote] means: the program to be parsed \
is syntactically valid and \[OpenCurlyQuote]no\[CloseCurlyQuote] means: the \
program to be parsed is not valid."
}], "Text"],

Cell["\<\
Further development could allow you to check your assembly language program \
for syntactic correctness before trying to run it.\
\>", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "assemblyGrammar", "=", 
   "\"\<\n<assembly> = <line> | (<line> , <assembly>) ;\n<line> = \
[<labelling>] , <instruction> , [(';' , [<comments>], ';')] ;\n<comments> = \
<comment> | (<comment> , <comments>) ;\n<comment> = '_WordString' ;\n\
<labelling> = <label> , ':' ;\n<label> = '_WordString' ;\n<instruction> = \n\
(<opcode1> , (<reg> , ',') , <mem-ref>) | \n(<opcode2> , (<reg> , ',') , \
((<reg> , ',') , <operand>)) | \n(<opcode3> , (<reg> , ',') , <operand>) | \n\
('CMP' , (<reg> , ',') , <operand>) | \n(<bcond> , <label>) | 'HALT' ;\n\
<opcode1> = 'LDR' | 'STR' ;\n<opcode2> = 'ADD' | 'SUB' | 'AND' | 'ORR' | \
'EOR' | 'LSL' | 'LSR' ;\n<opcode3> = 'MOV' | 'CMP' | 'MVN' ;\n<bcond> = 'B' | \
'BEQ' | 'BNE' | 'BGT' | 'BLT' ;\n<reg> = ('R0' | 'R1' | 'R2' | 'R3' | 'R4' | \
'R5' | 'R6' | 'R7' | 'R8' | 'R9' | 'R10' | 'R11' | 'R12') ;\n<mem-ref> = \
'Range[0,65535]' ;\n<literal> = ('#' , 'Range[-4294967296,4294967295]') ;\n\
<operand> = (<literal> | <reg>) ;\n\>\""}], " ", ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"f880d6e2-deeb-430b-a30e-f94e22d1b97b"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Grammar to compute elements of the parse tree", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "operationalAssemblyGrammar", "=", 
   "\"\<\n<assembly> = <line> | (<line> , <assembly>) ;\n<line> = \
([<labelling>] , [<instruction>]) \[LeftTriangle] [(';' , [<comments>], ';')] \
<@ asmLine ;\n<comments> = <comment> | (<comment> , <comments>) ;\n<comment> \
= '_WordString' ;\n<labelling> = <label> \[LeftTriangle] ':' <@ asmLabel ;\n\
<label> = '_WordString' ;\n<instruction> = \n('OUT' , '_WordString') |\n\
(<opcode1> , (<reg> \[LeftTriangle] ',') , <mem-ref>) | \n(<opcode2> , (<reg> \
\[LeftTriangle] ',') , ((<reg> \[LeftTriangle] ',') , <operand>)) | \n\
(<opcode3> , (<reg> \[LeftTriangle] ',') , <operand>) | \n('CMP' , (<reg> \
\[LeftTriangle] ',') , <operand>) | \n(<bcond> , <label>) | 'HALT' <@ \
asmInstr ;\n<opcode1> = 'LDR' | 'STR' ;\n<opcode2> = 'ADD' | 'SUB' | 'AND' | \
'ORR' | 'EOR' | 'LSL' | 'LSR' ;\n<opcode3> = 'MOV' | 'CMP' | 'MVN' ;\n<bcond> \
= 'B' | 'BEQ' | 'BNE' | 'BGT' | 'BLT' ;\n<reg> = ('R0' | 'R1' | 'R2' | 'R3' | \
'R4' | 'R5' | 'R6' | 'R7' | 'R8' | 'R9' | 'R10' | 'R11' | 'R12') <@ \
asmRegProcess ;\n<mem-ref> = 'Range[0,65535]' | <reg> <@ asmMemRef ;\n\
<literal> = ('#' \[RightTriangle] 'Range[-4294967296,4294967295]') <@ \
asmLiteralProcess ;\n<operand> = (<literal> | <reg>) <@ asmOperand ;\n\>\""}],
   " ", ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"f52ee5a9-14d6-4ee7-aa46-c7111ce13280"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
This next short \[Section] generates the definitions for the parser e.g. pLINE\
\>", "Subsubsection"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"Clear", "[", "\"\<p*\>\"", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"currentParser", "=", 
   RowBox[{"GenerateParsersFromEBNF", "@", 
    RowBox[{"ParseToEBNFTokens", "@", "operationalAssemblyGrammar"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"LeafCount", "@", "currentParser"}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"611f070b-8ec0-4712-96de-4312f26442e4"],

Cell[BoxData["3"], "Output",ExpressionUUID->"eb437226-940e-4b64-beb1-0959e29c98b9"]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["The operational assembler", "Section",ExpressionUUID->"5b21c775-e4e8-4cb1-bde0-711ddef16d8d"],

Cell[CellGroupData[{

Cell["Code for 2-s complement", "Subsection"],

Cell[BoxData[{
 RowBox[{"Clear", "[", 
  RowBox[{
  "binChop", ",", "bf", ",", "fits", ",", "binNeg", ",", "to2sComplement", 
   ",", "from2sComplement", ",", "overflow"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"bf", "[", 
   RowBox[{"x_", ",", 
    RowBox[{"b_:", "2"}]}], "]"}], ":=", 
  RowBox[{"BaseForm", "[", 
   RowBox[{"x", ",", "b"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"binChop", "[", 
   RowBox[{"x_", ",", 
    RowBox[{"numBits_:", "16"}]}], "]"}], ":=", 
  RowBox[{"Mod", "[", 
   RowBox[{"x", ",", 
    SuperscriptBox["2", "numBits"]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"binNeg", "[", 
    RowBox[{"x_", ",", 
     RowBox[{"numBits_:", "16"}]}], "]"}], ":=", 
   RowBox[{"BitXor", "[", 
    RowBox[{"x", ",", 
     RowBox[{
      SuperscriptBox["2", "numBits"], "-", "1"}]}], "]"}]}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"note", " ", "this", " ", 
    StyleBox["preserves",
     FontSlant->"Italic"], " ", "any", " ", "bits", " ", "of", " ", "x", " ", 
    "of", " ", "significance", " ", "of", " ", "16", " ", "or", " ", 
    "above"}], " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"fits", "[", 
    RowBox[{"n_", ",", 
     RowBox[{"numBits_:", "16"}]}], "]"}], ":=", 
   RowBox[{
    RowBox[{"BitLength", "[", "n", "]"}], "\[LessEqual]", "numBits"}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{"SetAttributes", "[", 
  RowBox[{"to2sComplement", ",", "Listable"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"to2sComplement", "[", 
    RowBox[{"n_Integer", ",", 
     RowBox[{"numBits_Integer:", "16"}]}], "]"}], "/;", 
   RowBox[{"n", "\[Equal]", 
    RowBox[{"-", 
     SuperscriptBox["2", 
      RowBox[{"numBits", "-", "1"}]]}]}]}], 
  RowBox[{"(*", " ", 
   RowBox[{"special", " ", "case"}], " ", "*)"}], ":=", "\[IndentingNewLine]", 
  SuperscriptBox["2", 
   RowBox[{"numBits", "-", "1"}]]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"to2sComplement", "[", 
   RowBox[{
    RowBox[{"n_Integer", "/;", 
     RowBox[{"n", "<", "0"}]}], ",", 
    RowBox[{"numBits_Integer:", "16"}]}], "]"}], 
  RowBox[{"(*", " ", 
   RowBox[{"other", " ", "negative", " ", "numbers"}], " ", "*)"}], ":=", 
  "\[IndentingNewLine]", 
  RowBox[{"With", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"bitForm", "=", 
      RowBox[{"binChop", "[", 
       RowBox[{
        RowBox[{"-", "n"}], ",", 
        RowBox[{"numBits", "-", "1"}]}], "]"}]}], "}"}], ",", 
    RowBox[{
     RowBox[{"binNeg", "[", 
      RowBox[{"bitForm", ",", "numBits"}], "]"}], "+", "1"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{"binChop", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"binNeg", "[", 
        RowBox[{"bitForm", ",", "numBits"}], "]"}], "+", "1"}], ",", 
      "numBits"}], "]"}], "*)"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"to2sComplement", "[", 
    RowBox[{
     RowBox[{"n_Integer", "/;", 
      RowBox[{"n", "\[GreaterEqual]", "0"}]}], ",", 
     RowBox[{"numBits_Integer:", "16"}]}], "]"}], 
   RowBox[{"(*", " ", 
    RowBox[{"positive", " ", "numbers"}], " ", "*)"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"binChop", "[", 
    RowBox[{"n", ",", 
     RowBox[{"numBits", "-", "1"}]}], "]"}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{"SetAttributes", "[", 
  RowBox[{"from2sComplement", ",", "Listable"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"from2sComplement", "[", 
    RowBox[{"n_Integer", ",", 
     RowBox[{"numBits_Integer:", "16"}]}], "]"}], "/;", 
   RowBox[{"fits", "[", 
    RowBox[{"n", ",", 
     RowBox[{"numBits", "-", "1"}]}], "]"}]}], ":=", 
  "n"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"from2sComplement", "[", 
    RowBox[{"n_Integer", ",", 
     RowBox[{"numBits_Integer:", "16"}]}], "]"}], "/;", 
   RowBox[{"fits", "[", 
    RowBox[{"n", ",", "numBits"}], "]"}]}], ":=", 
  RowBox[{
   RowBox[{"-", 
    RowBox[{"binNeg", "[", 
     RowBox[{"n", ",", "numBits"}], "]"}]}], "-", 
   "1"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"from2sComplement", "[", 
   RowBox[{"n_Integer", ",", 
    RowBox[{"numBits_Integer:", "16"}]}], "]"}], ":=", 
  RowBox[{"(*", " ", 
   RowBox[{"not", " ", "really", " ", "allowed"}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"from2sComplement", "[", 
   RowBox[{
    RowBox[{"binChop", "[", 
     RowBox[{"n", ",", "numBits"}], "]"}], ",", "numBits"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"overflow", "[", 
   RowBox[{"n_", ",", 
    RowBox[{"numBits_:", "16"}]}], "]"}], ":=", 
  RowBox[{"Not", "[", 
   RowBox[{"fits", "[", 
    RowBox[{"n", ",", 
     RowBox[{"numBits", "-", "1"}]}], "]"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "see", " ", "if", " ", "a", " ", "number", " ", "cannot", " ", "be", " ", 
     "represented", " ", "in", " ", "a", " ", "number", " ", 
     RowBox[{"(", 
      RowBox[{"default", ":", "16"}], ")"}], " ", "of", " ", "bits", " ", 
     "using", " ", "2"}], "-", 
    RowBox[{"s", " ", "complement"}]}], " ", "*)"}]}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"855effa7-79a1-42df-9d0a-3e61a5aa88dc"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"254", "//", "bf"}], "\[IndentingNewLine]", 
 RowBox[{"BitShiftLeft", "[", "254", "]"}], "\[IndentingNewLine]", 
 RowBox[{"%", "//", "bf"}], "\[IndentingNewLine]", 
 RowBox[{"binChop", "[", 
  RowBox[{"%%", ",", "8"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"%", "//", "bf"}]}], "Input"],

Cell[BoxData[
 TagBox[
  InterpretationBox[
   SubscriptBox["\<\"11111110\"\>", "\<\"2\"\>"],
   254,
   Editable->False],
  BaseForm[#, 2]& ]], "Output"],

Cell[BoxData["508"], "Output"],

Cell[BoxData[
 TagBox[
  InterpretationBox[
   SubscriptBox["\<\"111111100\"\>", "\<\"2\"\>"],
   508,
   Editable->False],
  BaseForm[#, 2]& ]], "Output"],

Cell[BoxData["252"], "Output"],

Cell[BoxData[
 TagBox[
  InterpretationBox[
   SubscriptBox["\<\"11111100\"\>", "\<\"2\"\>"],
   252,
   Editable->False],
  BaseForm[#, 2]& ]], "Output"]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["The code to load, decode and execute", "Subsection"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"Clear", "[", 
   RowBox[{"asGrab", ",", "asLocations", ",", "asLoad"}], "]"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "grab", " ", "code", " ", "in", " ", "a", " ", "string", " ", "and", " ", 
    "split", " ", "off", " ", "any", " ", "labels"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asGrab", "[", "code_String", "]"}], ":=", 
   RowBox[{"(*", 
    RowBox[{"Association", "@"}], "*)"}], 
   RowBox[{"Map", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"Switch", "[", 
       RowBox[{"#", ",", 
        RowBox[{"{", "_", "}"}], ",", 
        RowBox[{"\"\<\>\"", "\[Rule]", 
         RowBox[{"#", "[", 
          RowBox[{"[", "1", "]"}], "]"}]}], ",", 
        RowBox[{"{", 
         RowBox[{"_", ",", "_"}], "}"}], ",", 
        RowBox[{
         RowBox[{"#", "[", 
          RowBox[{"[", "1", "]"}], "]"}], "\[Rule]", 
         RowBox[{"#", "[", 
          RowBox[{"[", "2", "]"}], "]"}]}], ",", 
        RowBox[{"{", "}"}], ",", 
        RowBox[{
         RowBox[{"##", "&"}], "[", "]"}]}], "\[IndentingNewLine]", 
       RowBox[{"(*", " ", 
        RowBox[{"ignore", " ", "any", " ", "blank", " ", "lines"}], " ", 
        "*)"}], "]"}], "&"}], ",", 
     RowBox[{"Map", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"StringSplit", "[", 
         RowBox[{"#", ",", "\"\< : \>\""}], "]"}], "&"}], ",", 
       RowBox[{"StringSplit", "[", 
        RowBox[{"code", ",", "\"\<\\n\>\""}], "]"}]}], "]"}]}], "]"}]}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "StringSplit", " ", "threaded", " ", "over", " ", "lists", " ", "so", " ", 
    "the", " ", "Map", " ", "may", " ", "be", " ", "unnecessary"}], " ", 
   "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "use", " ", "memory", " ", "offset", " ", "to", " ", "store", " ", 
    "locations", " ", "for", " ", "labelled", " ", "instructions"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asLocations", "[", 
    RowBox[{"p_", ",", 
     RowBox[{"offset_:", "0"}]}], "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{"Association", "@", 
    RowBox[{"MapIndexed", "[", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"First", "[", "#1", "]"}], "\[Rule]", 
        RowBox[{
         RowBox[{"First", "[", "#2", "]"}], "+", "offset"}]}], "&"}], ",", 
      RowBox[{"Normal", "@", "p"}]}], "]"}]}]}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"load", " ", "code", " ", "into", " ", "\"\<memory\>\""}], ":"}], 
   " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"asLoad", "[", 
   RowBox[{"p_", ",", 
    RowBox[{"offset_:", "0"}], ",", 
    RowBox[{"wl_:", "16"}]}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{"not", " ", "needed", " ", "here"}], " ", "*)"}], "]"}], ":=", 
  "\[IndentingNewLine]", 
  RowBox[{"With", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"len", "=", 
      RowBox[{"Length", "[", "p", "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"SparseArray", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"Range", "[", "len", "]"}], "+", "offset"}], "\[Rule]", 
      RowBox[{"Map", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"#", "[", 
          RowBox[{"[", "2", "]"}], "]"}], "&"}], ",", "p"}], "]"}]}], "]"}]}],
    "]"}]}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"0fb47de0-6a58-489e-a9af-75aad127ffef"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"Clear", "[", 
   RowBox[{
   "asInterpretStep", ",", "operation", ",", " ", "compare", ",", "asFetch", 
    ",", "asRun", ",", " ", "asDecodeDebugging", ",", "asExecute", ",", 
    "asRunDecoded", ",", "asRun", ",", "asLoadRun", ",", "asLoadRunDecoded", 
    ",", " ", "asPrint"}], "]"}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"binary", " ", "operations"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<ADD\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], ":=", 
   RowBox[{
    RowBox[{"binChop", "[", 
     RowBox[{
      RowBox[{"Plus", "[", 
       RowBox[{"#1", ",", "#2"}], "]"}], ",", "wl"}], "]"}], "&"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<SUB\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], ":=", 
   RowBox[{
    RowBox[{"binChop", "[", 
     RowBox[{
      RowBox[{"Plus", "[", 
       RowBox[{"#1", ",", 
        RowBox[{
         RowBox[{"binNeg", "[", 
          RowBox[{"#2", ",", "wl"}], "]"}], "+", "1"}]}], "]"}], ",", "wl"}], 
     "]"}], "&"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<AND\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], "=", "BitAnd"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<ORR\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], "=", "BitOr"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<EOR\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], "=", "BitXor"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<LSL\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], ":=", 
   RowBox[{
    RowBox[{"binChop", "[", 
     RowBox[{
      RowBox[{"BitShiftLeft", "[", "#", "]"}], ",", "wl"}], "]"}], "&"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"operation", "[", 
     RowBox[{"\"\<LSR\>\"", ",", 
      RowBox[{"wl_:", "16"}]}], "]"}], "=", "BitShiftRight"}], ";"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"unary", " ", "operations"}], " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"operation", "[", 
     RowBox[{"\"\<MOV\>\"", ",", 
      RowBox[{"wl_:", "16"}]}], "]"}], "=", "Identity"}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"no", " ", "need", " ", "to", " ", "truncate"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"operation", "[", 
     RowBox[{"\"\<MVN\>\"", ",", 
      RowBox[{"wl_:", "16"}]}], "]"}], ":=", 
    RowBox[{
     RowBox[{"binNeg", "[", 
      RowBox[{"#", ",", "wl"}], "]"}], "&"}]}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"no", " ", "need", " ", "to", " ", 
    RowBox[{"truncate", "?"}]}], " ", "*)"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{"Clear", "[", "asFetch", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asFetch", "[", 
    RowBox[{"mem_", ",", "pc_"}], "]"}], ":=", 
   RowBox[{"mem", "[", 
    RowBox[{"[", "pc", "]"}], "]"}]}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{
   "now", " ", "fetches", " ", "directly", " ", "from", " ", "memory"}], " ", 
   "*)"}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asDecodeDebugging", "[", "p_String", "]"}], ":=", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"ParseShortest", "[", "pINSTRUCTION", "]"}], "[", 
      RowBox[{"ToTokens", "[", 
       RowBox[{"p", ",", 
        RowBox[{"{", 
         RowBox[{
         "\"\<;\>\"", ",", "\"\<#\>\"", ",", "\"\<:\>\"", ",", "\"\<,\>\""}], 
         "}"}]}], "]"}], "]"}], ")"}], "\[LeftDoubleBracket]", 
    RowBox[{"1", ",", "2"}], "\[RightDoubleBracket]"}]}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"asDecodeDebugging", "[", "p_String", "]"}], ":=", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"ParseShortest", "[", "pINSTRUCTION", "]"}], "[", 
       RowBox[{"ToTokens", "[", 
        RowBox[{"p", ",", 
         RowBox[{"{", "}"}]}], "]"}], "]"}], ")"}], "\[LeftDoubleBracket]", 
     RowBox[{"1", ",", "2"}], "\[RightDoubleBracket]"}]}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asDecodeDebugging", "[", "v_", "]"}], ":=", "v"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"compare", "[", 
    RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
   RowBox[{"Which", "[", 
    RowBox[{
     RowBox[{"x", "\[Equal]", "y"}], ",", "\"\<EQ\>\"", ",", 
     RowBox[{"x", "<", "y"}], ",", "\"\<LT\>\"", ",", 
     RowBox[{"x", ">", "y"}], ",", "\"\<GT\>\"", ",", "True", ",", 
     "\"\<Fail\>\""}], "]"}]}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "needs", " ", "extensive", " ", "testing", " ", "to", " ", "ensure", " ", 
    "processor", " ", "operates", " ", "correctly", " ", "with", " ", 
    "finite", " ", "word", " ", "length"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<STR\>\"", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", 
           RowBox[{"(*", " ", 
            RowBox[{"counting", " ", "from", " ", "0"}], " ", "*)"}], "]"}], 
          ",", 
          RowBox[{"asmMemRef", "[", "m_", "]"}]}], "}"}]}], "}"}], "]"}], ",",
      "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"ReplacePart", "[", 
     RowBox[{"mem", ",", 
      RowBox[{"m", "\[Rule]", 
       RowBox[{"registers", "[", 
        RowBox[{"[", 
         RowBox[{"r", "+", "1"}], "]"}], "]"}]}]}], "]"}], ",", 
    RowBox[{"pc", "+", "1"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<LDR\>\"", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", "]"}], ",", 
          RowBox[{"asmMemRef", "[", "m_", "]"}]}], "}"}]}], "}"}], "]"}], ",",
      "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"pc", "+", "1"}], ",", "status", ",", 
    RowBox[{"ReplacePart", "[", 
     RowBox[{"registers", ",", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"r", "+", "1"}], ")"}], "\[Rule]", 
       RowBox[{"mem", "[", 
        RowBox[{"[", "m", "]"}], "]"}]}]}], "]"}]}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<STR\>\"", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", 
           RowBox[{"(*", " ", 
            RowBox[{"counting", " ", "from", " ", "0"}], " ", "*)"}], "]"}], 
          ",", 
          RowBox[{"asmMemRef", "[", 
           RowBox[{"asmReg", "[", "m_", "]"}], "]"}]}], "}"}]}], "}"}], "]"}],
      ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"ReplacePart", "[", 
     RowBox[{"mem", ",", 
      RowBox[{
       RowBox[{"registers", "[", 
        RowBox[{"[", 
         RowBox[{"m", "+", "1"}], "]"}], "]"}], "\[Rule]", 
       RowBox[{"registers", "[", 
        RowBox[{"[", 
         RowBox[{"r", "+", "1"}], "]"}], "]"}]}]}], "]"}], ",", 
    RowBox[{"pc", "+", "1"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asInterpretStep", "[", 
     RowBox[{
      RowBox[{"asmInstr", "[", 
       RowBox[{"{", 
        RowBox[{"\"\<LDR\>\"", ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"asmReg", "[", "r_", "]"}], ",", 
           RowBox[{"asmMemRef", "[", 
            RowBox[{"asmReg", "[", "m_", "]"}], "]"}]}], "}"}]}], "}"}], 
       "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
     "}"}], "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"mem", ",", 
     RowBox[{"pc", "+", "1"}], ",", "status", ",", 
     RowBox[{"ReplacePart", "[", 
      RowBox[{"registers", ",", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"r", "+", "1"}], ")"}], "\[Rule]", 
        RowBox[{"mem", "[", 
         RowBox[{"[", 
          RowBox[{"registers", "[", 
           RowBox[{"[", 
            RowBox[{"m", "+", "1"}], "]"}], "]"}], "]"}], "]"}]}]}], "]"}]}], 
    "}"}]}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "Actually", " ", "MOV", " ", "and", " ", "MVN", " ", "are", " ", "both", 
    " ", "unary", " ", "operations", " ", "with", " ", "Identity", " ", "for",
     " ", "MOV"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
     RowBox[{"asInterpretStep", "[", 
      RowBox[{
       RowBox[{"asmInstr", "[", 
        RowBox[{"{", 
         RowBox[{"\"\<MOV\>\"", ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"asmReg", "[", "r_", "]"}], ",", 
            RowBox[{"asmOperand", "[", 
             RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], "}"}], 
        "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
     RowBox[{"{", 
      RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
      "}"}], "]"}], ":=", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{
      RowBox[{"{", 
       RowBox[{"mem", ",", 
        RowBox[{"pc", "+", "1"}], ",", "status", ",", 
        RowBox[{"ReplacePart", "[", 
         RowBox[{"registers", ",", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{"r", "+", "1"}], ")"}], "\[Rule]", 
           RowBox[{
            RowBox[{"operation", "[", "\"\<MOV\>\"", "]"}], "[", "l", 
            "]"}]}]}], "]"}]}], "}"}], "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"asInterpretStep", "[", 
        RowBox[{
         RowBox[{"asmInstr", "[", 
          RowBox[{"{", 
           RowBox[{"\"\<MOV\>\"", ",", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
              RowBox[{"asmOperand", "[", 
               RowBox[{"asmReg", "[", "r2_", "]"}], "]"}]}], "}"}]}], "}"}], 
          "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
       RowBox[{"{", 
        RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
        "}"}], "]"}]}], ":=", "\[IndentingNewLine]", 
     RowBox[{"{", 
      RowBox[{"mem", ",", 
       RowBox[{"pc", "+", "1"}], ",", "status", ",", 
       RowBox[{"ReplacePart", "[", 
        RowBox[{"registers", ",", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"r1", "+", "1"}], ")"}], "\[Rule]", 
          RowBox[{
           RowBox[{"operation", "[", "\"\<MOV\>\"", "]"}], "[", 
           RowBox[{"registers", "[", 
            RowBox[{"[", 
             RowBox[{"r2", "+", "1"}], "]"}], "]"}], "]"}]}]}], "]"}]}], 
      "}"}]}]}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"op", ":", 
         RowBox[{"\"\<MOV\>\"", "|", "\"\<MVN\>\""}]}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"pc", "+", "1"}], ",", "status", ",", 
    RowBox[{"ReplacePart", "[", 
     RowBox[{"registers", ",", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"r", "+", "1"}], ")"}], "\[Rule]", 
       RowBox[{
        RowBox[{"operation", "[", 
         RowBox[{"op", ",", "$wordLength"}], "]"}], "[", "l", "]"}]}]}], 
     "]"}]}], "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"op", ":", 
         RowBox[{"\"\<MOV\>\"", "|", "\"\<MVN\>\""}]}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmReg", "[", "r2_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"pc", "+", "1"}], ",", "status", ",", 
    RowBox[{"ReplacePart", "[", 
     RowBox[{"registers", ",", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"r1", "+", "1"}], ")"}], "\[Rule]", 
       RowBox[{
        RowBox[{"operation", "[", 
         RowBox[{"op", ",", "$wordLength"}], "]"}], "[", 
        RowBox[{"registers", "[", 
         RowBox[{"[", 
          RowBox[{"r2", "+", "1"}], "]"}], "]"}], "]"}]}]}], "]"}]}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<CMP\>\"", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmReg", "[", "r2_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", " ", 
    RowBox[{"pc", "+", "1"}], ",", 
    RowBox[{"compare", "[", 
     RowBox[{
      RowBox[{"registers", "[", 
       RowBox[{"[", 
        RowBox[{"r1", "+", "1"}], "]"}], "]"}], ",", 
      RowBox[{"registers", "[", 
       RowBox[{"[", 
        RowBox[{"r2", "+", "1"}], "]"}], "]"}]}], "]"}], ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<CMP\>\"", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", " ", 
    RowBox[{"pc", "+", "1"}], ",", 
    RowBox[{"compare", "[", 
     RowBox[{
      RowBox[{"registers", "[", 
       RowBox[{"[", 
        RowBox[{"r", "+", "1"}], "]"}], "]"}], ",", "l"}], "]"}], ",", 
    "registers"}], "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"op_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"asmReg", "[", "r2_", "]"}], ",", 
            RowBox[{"asmOperand", "[", 
             RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], 
         "}"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"With", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"f", "=", 
      RowBox[{"operation", "[", 
       RowBox[{"op", ",", "$wordLength"}], "]"}]}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"{", 
     RowBox[{"mem", ",", 
      RowBox[{"pc", "+", "1"}], ",", "status", ",", 
      RowBox[{"ReplacePart", "[", 
       RowBox[{"registers", ",", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"r1", "+", "1"}], ")"}], "\[Rule]", 
         RowBox[{"f", "[", 
          RowBox[{
           RowBox[{"registers", "[", 
            RowBox[{"[", 
             RowBox[{"r2", "+", "1"}], "]"}], "]"}], ",", "l"}], "]"}]}]}], 
       "]"}]}], "}"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"op_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"asmReg", "[", "r2_", "]"}], ",", 
            RowBox[{"asmOperand", "[", 
             RowBox[{"asmReg", "[", "r3_", "]"}], "]"}]}], "}"}]}], "}"}]}], 
       "}"}], "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"With", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"f", "=", 
      RowBox[{"operation", "[", 
       RowBox[{"op", ",", "$wordLength"}], "]"}]}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"{", 
     RowBox[{"mem", ",", 
      RowBox[{"pc", "+", "1"}], ",", "status", ",", 
      RowBox[{"ReplacePart", "[", 
       RowBox[{"registers", ",", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"r1", "+", "1"}], ")"}], "\[Rule]", 
         RowBox[{"f", "[", 
          RowBox[{
           RowBox[{"registers", "[", 
            RowBox[{"[", 
             RowBox[{"r2", "+", "1"}], "]"}], "]"}], ",", 
           RowBox[{"registers", "[", 
            RowBox[{"[", 
             RowBox[{"r3", "+", "1"}], "]"}], "]"}]}], "]"}]}]}], "]"}]}], 
     "}"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<B\>\"", ",", "l_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"labels", "[", "l", "]"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<BEQ\>\"", ",", "l_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{"status", "\[Equal]", "\"\<EQ\>\""}], ",", 
      RowBox[{"labels", "[", "l", "]"}], ",", 
      RowBox[{"pc", "+", "1"}]}], "]"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<BNE\>\"", ",", "l_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{"status", "\[NotEqual]", "\"\<EQ\>\""}], ",", 
      RowBox[{"labels", "[", "l", "]"}], ",", 
      RowBox[{"pc", "+", "1"}]}], "]"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<BLT\>\"", ",", "l_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{"status", "\[Equal]", "\"\<LT\>\""}], ",", 
      RowBox[{"labels", "[", "l", "]"}], ",", 
      RowBox[{"pc", "+", "1"}]}], "]"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<BGT\>\"", ",", "l_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{"status", "\[Equal]", "\"\<GT\>\""}], ",", 
      RowBox[{"labels", "[", "l", "]"}], ",", 
      RowBox[{"pc", "+", "1"}]}], "]"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<OUT\>\"", ",", "msg_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"MessageDialog", "[", 
     RowBox[{"\"\<PC: \>\"", "<>", 
      RowBox[{"ToString", "[", "pc", "]"}], "<>", "\"\<\\n\>\"", "<>", 
      "msg"}], "]"}], ";", 
    RowBox[{"{", 
     RowBox[{"mem", ",", 
      RowBox[{"pc", " ", "+", "1"}], ",", "status", ",", "registers"}], 
     "}"}]}], ")"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", "\"\<HALT\>\"", "]"}], ",", "labels_"}], "]"}], 
   "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", 
  RowBox[{"{", 
   RowBox[{"mem", ",", "0", " ", 
    RowBox[{"(*", " ", 
     RowBox[{
     "special", " ", "value", " ", "not", " ", "otherwise", " ", "used"}], 
     " ", "*)"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asInterpretStep", "[", 
     RowBox[{"instr_", ",", "labels_"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
     "}"}], "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{"(*", "  ", 
    RowBox[{
     RowBox[{"preconditions", ":", " ", "\[IndentingNewLine]", "   ", 
      RowBox[{
      "the", " ", "instructions", " ", "are", " ", "loaded", " ", "into", " ",
        "memory", " ", "already", " ", "and", " ", "we", " ", "know", " ", 
       "their", " ", 
       StyleBox["locations",
        FontSlant->"Italic"]}]}], ";", "\[IndentingNewLine]", "\t", 
     RowBox[{
     "we", " ", "have", " ", "stored", " ", "all", " ", "previous", " ", 
      RowBox[{"labels", " ", "'"}], 
      RowBox[{"appropriately", "'"}], " ", "in", " ", "labels"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"(", 
      RowBox[{
      "actually", " ", "labels", " ", "is", " ", "an", " ", "association", 
       " ", "between", " ", "labels", " ", "and", " ", "instructions", " ", 
       "now"}], ")"}], ";", "\[IndentingNewLine]", "\t", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
       "note", " ", "this", " ", "means", " ", "that", " ", "we", " ", "need",
         " ", "a", " ", "first", " ", "pass", " ", "to", " ", "do", " ", 
        "this", " ", "step"}], ",", "\[IndentingNewLine]", "\t", 
       RowBox[{
        RowBox[{
        "and", " ", "further", " ", "that", " ", "this", " ", "will", " ", 
         "mean", " ", "a", " ", "re"}], "-", 
        RowBox[{"implementation", " ", "of", " ", "map"}]}]}], ")"}], ";", 
     "\[IndentingNewLine]", "\t", 
     RowBox[{
     "the", " ", "registers", " ", "are", " ", "all", " ", "correctly", " ", 
      "filled"}], ";", "\[IndentingNewLine]", "\t", 
     RowBox[{
     "the", " ", "current", " ", "instruction", " ", "does", " ", "not", " ", 
      "reference", " ", "a", " ", "memory", " ", "location", " ", "outside", 
      " ", "of", " ", "mem"}], ";", "\[IndentingNewLine]", "\t", 
     RowBox[{
     "the", " ", "current", " ", "instruction", " ", "does", " ", "not", " ", 
      "reference", " ", "a", " ", "label", " ", "outside", " ", "of", " ", 
      "labels"}], ";", "\[IndentingNewLine]", "\t", 
     RowBox[{
      RowBox[{"we", " ", "are", " ", "not", " ", "writing", " ", "self"}], 
      "-", 
      RowBox[{
      "modifying", " ", "code", " ", "so", " ", "we", " ", "cannot", " ", 
       "branch", " ", "to", " ", "an", " ", "arbitrary", " ", 
       "\[IndentingNewLine]", "  ", "point", " ", "in", " ", "memory"}]}], 
     ";"}], "\[IndentingNewLine]", "*)"}], "\[IndentingNewLine]", 
   RowBox[{"(*", "instr", 
    RowBox[{"(*", "UNDEFINED", "*)"}], "*)"}], "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"mem", ",", 
     RowBox[{"pc", " ", "+", "1"}], ",", "status", ",", "registers"}], 
    "}"}]}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", 
           RowBox[{"(*", " ", 
            RowBox[{"counting", " ", "from", " ", "0"}], " ", "*)"}], "]"}], 
          ",", 
          RowBox[{"asmMemRef", "[", "m_", "]"}]}], "}"}]}], "}"}], "]"}], ",",
      "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r", "]"}], "<>", "\"\< , \>\"", "<>", 
    RowBox[{"ToString", "[", "m", "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", 
           RowBox[{"(*", " ", 
            RowBox[{"counting", " ", "from", " ", "0"}], " ", "*)"}], "]"}], 
          ",", 
          RowBox[{"asmMemRef", "[", 
           RowBox[{"asmReg", "[", "m_", "]"}], "]"}]}], "}"}]}], "}"}], "]"}],
      ",", "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r", "]"}], "<>", "\"\< , R\>\"", "<>", 
    RowBox[{"ToString", "[", "m", "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r", "]"}], "<>", "\"\< , # \>\"", "<>", 
    RowBox[{"ToString", "[", 
     RowBox[{"If", "[", 
      RowBox[{
       RowBox[{"format", "\[Equal]", "\"\<signed\>\""}], ",", 
       RowBox[{"from2sComplement", "@", "l"}], ",", "l"}], "]"}], "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmReg", "[", "r2_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r1", "]"}], "<>", "\"\< , R\>\"", "<>", 
    RowBox[{"ToString", "[", "r2", "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"asmReg", "[", "r2_", "]"}], ",", 
            RowBox[{"asmOperand", "[", 
             RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], 
         "}"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r1", "]"}], "<>", "\"\< , R\>\"", "<>", 
    RowBox[{"ToString", "[", "r2", "]"}], "<>", "\"\< , # \>\"", "<>", 
    RowBox[{"ToString", "[", 
     RowBox[{"If", "[", 
      RowBox[{
       RowBox[{"format", "\[Equal]", "\"\<signed\>\""}], ",", 
       RowBox[{"from2sComplement", "@", "l"}], ",", "l"}], "]"}], "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"asmReg", "[", "r2_", "]"}], ",", 
            RowBox[{"asmOperand", "[", 
             RowBox[{"asmReg", "[", "r3_", "]"}], "]"}]}], "}"}]}], "}"}]}], 
       "}"}], "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r1", "]"}], "<>", "\"\< , R\>\"", "<>", 
    RowBox[{"ToString", "[", "r2", "]"}], "<>", "\"\< , R\>\"", "<>", 
    RowBox[{"ToString", "[", "r3", "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"branchString_", ",", "l_"}], "}"}], "]"}], ",", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   RowBox[{"branchString", "<>", "\"\< \>\"", "<>", 
    RowBox[{"ToString", "[", "l", "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", "\"\<HALT\>\"", "]"}], ",", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", "\"\<HALT\>\""}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{"instr_", ",", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"ToString", "[", "instr", "]"}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asExecute", "[", 
     RowBox[{"instr_", ",", "labels_"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
    "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"asInterpretStep", "[", 
     RowBox[{"instr", ",", "labels"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem", ",", "pc", ",", "status", ",", "registers"}], "}"}], 
    "]"}]}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "seems", " ", "to", " ", "work", " ", "out", " ", "of", " ", "the", " ", 
    "box", " ", "now"}], " ", "*)"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asStep", "[", "labels_", "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
    "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"asExecute", "[", 
     RowBox[{
      RowBox[{"asDecodeDebugging", "[", 
       RowBox[{"asFetch", "[", 
        RowBox[{"mem", ",", "pc"}], "]"}], "]"}], ",", "labels"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem", ",", "pc", ",", "status", ",", "registers"}], "}"}], 
    "]"}]}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asRunDecoded", "[", 
     RowBox[{"program_List", ",", "labels_"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
     "}"}], "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{"FoldList", "[", 
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"asExecute", "[", 
        RowBox[{"#2", ",", "labels"}], "]"}], "[", "#1", "]"}], "&"}], ",", 
     RowBox[{"{", 
      RowBox[{"mem", ",", "pc", ",", "status", ",", "registers"}], "}"}], ",",
      "program"}], "]"}]}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "ok", " ", "that", " ", "does", " ", "not", " ", "really", " ", "work", 
    " ", "since", " ", "the", " ", "function", " ", "needs", " ", "to", " ", 
    "access", " ", "the", " ", "pc", " ", "first"}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "but", " ", "it", " ", "can", " ", "be", " ", "used", " ", "to", " ", 
    "work", " ", "with", " ", "a", " ", "limited", " ", "number", " ", "of", 
    " ", "instructions", " ", "that", " ", "do", " ", "not", " ", 
    StyleBox["read",
     FontSlant->"Italic"], 
    StyleBox[" ",
     FontSlant->"Plain"], 
    StyleBox["the",
     FontSlant->"Plain"], 
    StyleBox[" ",
     FontSlant->"Plain"], 
    StyleBox["pc",
     FontSlant->"Plain"]}], 
   StyleBox[" ",
    FontSlant->"Plain"], 
   StyleBox["*)",
    FontSlant->"Plain"]}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asRun", "[", 
     RowBox[{"code_List", ",", "labels_"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
     "}"}], "]"}], ":=", "\[IndentingNewLine]", "l"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"not", " ", "so", " ", "far", " ", "needed"}], " ", "*)"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asLoadRun", "[", 
    RowBox[{"code_String", ",", 
     RowBox[{"offset_:", "0"}], ",", 
     RowBox[{"maxSteps_:", "100"}]}], "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{"With", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"iniPc", "=", 
        RowBox[{"1", "+", "offset"}]}], ",", 
       RowBox[{"iniStatus", "=", "\"\<EQ\>\""}], ",", 
       RowBox[{"iniRegisters", "=", 
        RowBox[{"Range", "[", 
         RowBox[{"0", ",", "12"}], "]"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"codeArray", "=", 
        RowBox[{"asGrab", "[", "code", "]"}]}]}], "}"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"With", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"iniMem", "=", 
          RowBox[{"asLoad", "[", 
           RowBox[{"codeArray", ",", "offset"}], "]"}]}], ",", 
         RowBox[{"locs", "=", 
          RowBox[{"asLocations", "[", 
           RowBox[{"codeArray", ",", "offset"}], "]"}]}]}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"NestWhileList", "[", 
        RowBox[{
         RowBox[{"asStep", "[", "locs", "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
          "iniMem", ",", "iniPc", ",", "iniStatus", ",", "iniRegisters"}], 
          "}"}], ",", 
         RowBox[{
          RowBox[{
           RowBox[{"#", "[", 
            RowBox[{"[", "2", "]"}], "]"}], ">", "0"}], "&"}], ",", "1", ",", 
         "maxSteps"}], "]"}]}], "]"}]}], "]"}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"asLoadRunDecoded", "[", 
   RowBox[{"code_String", ",", 
    RowBox[{"offset_:", "0"}], ",", 
    RowBox[{"maxSteps_:", "100"}], ",", 
    RowBox[{"wl_Integer:", "16"}]}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"With", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"iniPc", "=", 
       RowBox[{"1", "+", "offset"}]}], ",", 
      RowBox[{"iniStatus", "=", "\"\<EQ\>\""}], ",", 
      RowBox[{"iniRegisters", "=", 
       RowBox[{"Range", "[", 
        RowBox[{"0", ",", "12"}], "]"}]}], ",", "\[IndentingNewLine]", 
      RowBox[{"codeArray", "=", 
       RowBox[{"asGrab", "[", "code", "]"}]}]}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"With", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"loadedCode", "=", 
         RowBox[{"asLoad", "[", 
          RowBox[{"codeArray", ",", "offset", ",", "wl"}], "]"}]}], ",", 
        RowBox[{"locs", "=", 
         RowBox[{"asLocations", "[", 
          RowBox[{"codeArray", ",", "offset"}], "]"}]}]}], "}"}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"With", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"iniMem", "=", 
          RowBox[{"Map", "[", 
           RowBox[{"asDecodeDebugging", ",", "loadedCode"}], "]"}]}], "}"}], 
        ",", "\[IndentingNewLine]", 
        RowBox[{"NestWhileList", "[", 
         RowBox[{
          RowBox[{"asStep", "[", "locs", "]"}], ",", 
          RowBox[{"{", 
           RowBox[{
           "iniMem", ",", "iniPc", ",", "iniStatus", ",", "iniRegisters"}], 
           "}"}], ",", 
          RowBox[{
           RowBox[{
            RowBox[{"#", "[", 
             RowBox[{"[", "2", "]"}], "]"}], ">", "0"}], "&"}], ",", "1", ",",
           "maxSteps"}], "]"}]}], "]"}]}], "]"}]}], "]"}]}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"0c54ee79-400b-455d-a0b3-eddc0a85c01a"]
}, Closed]],

Cell[CellGroupData[{

Cell["The interface to inspect the results of running the code", "Subsection"],

Cell[BoxData[{
 RowBox[{"Clear", "[", "runMyProgram", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"runMyProgram", "[", 
   RowBox[{"program_", ",", 
    RowBox[{"start_Integer:", "50"}], ",", " ", 
    RowBox[{"limit_Integer:", "100"}], ",", 
    RowBox[{"wl_:", "16"}]}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"DynamicModule", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
     "tab", ",", "code", ",", "codeRun", ",", "memoryRules", ",", "len", ",", 
      "keys", ",", "pcList", ",", "statusList", ",", "registerList", ",", 
      "memories", ",", 
      RowBox[{"showCode", "=", "False"}]}], "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"Column", "[", 
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"Panel", "[", 
        RowBox[{"\"\<Assembler version \>\"", "<>", 
         RowBox[{"ToString", "@", "$assemblyVersion"}]}], "]"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"Panel", "[", 
        RowBox[{"Dynamic", "@", 
         RowBox[{"If", "[", 
          RowBox[{"showCode", ",", 
           RowBox[{"(*", 
            RowBox[{"Dynamic", "@"}], "*)"}], "code", ",", 
           "\"\<Code hidden\>\""}], "]"}]}], "]"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"Manipulate", "[", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"Column", "[", 
          RowBox[{"{", "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"Row", "@", 
             RowBox[{"{", "\[IndentingNewLine]", 
              RowBox[{
               RowBox[{"Button", "[", 
                RowBox[{"\"\<Reload code\>\"", ",", 
                 RowBox[{"(", 
                  RowBox[{
                   RowBox[{"code", "=", "program"}], ";", 
                   RowBox[{"codeRun", "=", 
                    RowBox[{"asLoadRunDecoded", "[", 
                    RowBox[{"code", ",", "offset", ",", "lim", ",", "wl"}], 
                    "]"}]}], ";", 
                   RowBox[{"len", "=", 
                    RowBox[{"Length", "[", "codeRun", "]"}]}], ";", 
                   RowBox[{
                    RowBox[{"{", 
                    RowBox[{
                    "memories", ",", "pcList", ",", "statusList", ",", 
                    "registerList"}], "}"}], "=", 
                    RowBox[{"Transpose", "[", "codeRun", "]"}]}], ";", 
                   "\[IndentingNewLine]", 
                   RowBox[{"memoryRules", "=", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"Drop", "[", 
                    RowBox[{
                    RowBox[{"ArrayRules", "[", "#", "]"}], ",", 
                    RowBox[{"-", "1"}]}], "]"}], "&"}], "/@", "memories"}]}], 
                   " ", ";", "\[IndentingNewLine]", 
                   RowBox[{"memoryRules", "=", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"Map", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{
                    RowBox[{"#", "[", 
                    RowBox[{"[", 
                    RowBox[{"1", ",", "1"}], "]"}], "]"}], "\[Rule]", 
                    RowBox[{"asPrint", "[", 
                    RowBox[{
                    RowBox[{"#1", "[", 
                    RowBox[{"[", "2", "]"}], "]"}], ",", "format"}], "]"}]}], 
                    "&"}], ",", "#"}], "]"}], "&"}], "/@", "memoryRules"}]}], 
                   ";", "\[IndentingNewLine]", 
                   RowBox[{"memoryRules", "=", 
                    RowBox[{"Map", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"SortBy", "[", 
                    RowBox[{"#", ",", 
                    RowBox[{
                    RowBox[{"#", "[", 
                    RowBox[{"[", "1", "]"}], "]"}], "&"}]}], "]"}], "&"}], 
                    ",", "memoryRules"}], "]"}]}], ";", "\[IndentingNewLine]", 
                   RowBox[{"keys", "=", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"Map", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"#", "[", 
                    RowBox[{"[", "1", "]"}], "]"}], "&"}], ",", "#"}], "]"}], 
                    "&"}], "/@", "memoryRules"}]}], ";"}], ")"}]}], "]"}], 
               ",", "\[IndentingNewLine]", 
               RowBox[{"Button", "[", 
                RowBox[{"\"\<Show code\>\"", ",", 
                 RowBox[{"(", 
                  RowBox[{"showCode", "=", "True"}], ")"}]}], "]"}], ",", 
               "\[IndentingNewLine]", 
               RowBox[{"Button", "[", 
                RowBox[{"\"\<Hide code\>\"", ",", 
                 RowBox[{"(", 
                  RowBox[{"showCode", "=", "False"}], ")"}]}], "]"}]}], 
              "}"}]}], ",", "\[IndentingNewLine]", 
            RowBox[{"TabView", "[", "\[IndentingNewLine]", 
             RowBox[{
              RowBox[{"{", 
               RowBox[{
                RowBox[{"\"\<base 10\>\"", "\[Rule]", "\[IndentingNewLine]", 
                 RowBox[{"Column", "[", 
                  RowBox[{"{", "\[IndentingNewLine]", 
                   RowBox[{
                    RowBox[{"Grid", "[", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"Text", "@", "\"\<Memory\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<PC\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<Status\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<CIR\>\""}]}], "}"}], ",", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"TabView", "[", " ", 
                    RowBox[{
                    RowBox[{"Thread", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{"#1", ",", "#2"}], "}"}], "&"}], "[", 
                    RowBox[{
                    RowBox[{"keys", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ",", 
                    RowBox[{"Normal", "[", 
                    RowBox[{"memoryRules", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "]"}]}], "]"}], "]"}], 
                    ",", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ",", 
                    RowBox[{"Appearance", "\[Rule]", 
                    RowBox[{"{", 
                    RowBox[{"\"\<Limited\>\"", ",", "10"}], "}"}]}]}], "]"}], 
                    ",", "\[IndentingNewLine]", 
                    RowBox[{"Text", "@", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                    RowBox[{"Text", "@", 
                    RowBox[{"statusList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                    RowBox[{"Text", "@", 
                    RowBox[{"asPrint", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"memories", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "[", 
                    RowBox[{"[", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "]"}], "]"}], ",", 
                    "format"}], "]"}]}]}], "}"}]}], "}"}], "]"}], ",", 
                    "\[IndentingNewLine]", 
                    RowBox[{"Grid", "[", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{"Text", "@", "\"\<Registers\>\""}], "}"}], ",", 
                    RowBox[{"{", 
                    RowBox[{"Text", "@", 
                    RowBox[{"Grid", "[", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{
                    "R0", ",", "R1", ",", "R2", ",", "R3", ",", "R4", ",", 
                    "R5", ",", "R6", ",", "R7", ",", "R8", ",", "R9", ",", 
                    "R10", ",", "R11", ",", "R12"}], "}"}], ",", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"If", "[", 
                    RowBox[{
                    RowBox[{"format", "\[Equal]", "\"\<signed\>\""}], ",", 
                    RowBox[{"from2sComplement", "[", 
                    RowBox[{"#", ",", "$wordLength"}], "]"}], ",", "#"}], 
                    "]"}], "&"}], "/@", 
                    RowBox[{"registerList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}]}], "}"}], "]"}]}], 
                    "}"}]}], "}"}], "]"}]}], "\[IndentingNewLine]", "}"}], 
                  "]"}]}], ",", "\[IndentingNewLine]", 
                RowBox[{"\"\<base 2\>\"", "\[Rule]", "\[IndentingNewLine]", 
                 RowBox[{"Column", "[", 
                  RowBox[{"{", "\[IndentingNewLine]", 
                   RowBox[{
                    RowBox[{"Grid", "[", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"Text", "@", "\"\<Memory\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<PC\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<Status\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<CIR\>\""}]}], "}"}], ",", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"TabView", "[", " ", 
                    RowBox[{
                    RowBox[{"Thread", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{"#1", ",", "#2"}], "}"}], "&"}], "[", 
                    RowBox[{
                    RowBox[{"keys", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ",", 
                    RowBox[{"Normal", "[", 
                    RowBox[{"memoryRules", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "]"}]}], "]"}], "]"}], 
                    ",", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ",", 
                    RowBox[{"Appearance", "\[Rule]", 
                    RowBox[{"{", 
                    RowBox[{"\"\<Limited\>\"", ",", "10"}], "}"}]}]}], "]"}], 
                    ",", "\[IndentingNewLine]", 
                    RowBox[{"Text", "@", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                    RowBox[{"Text", "@", 
                    RowBox[{"statusList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                    "\[IndentingNewLine]", 
                    RowBox[{"Text", "@", 
                    RowBox[{"asPrint", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"memories", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "[", 
                    RowBox[{"[", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "]"}], "]"}], ",", 
                    "format"}], "]"}]}]}], "}"}]}], "}"}], "]"}], ",", 
                    "\[IndentingNewLine]", 
                    RowBox[{"Grid", "[", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{"Text", "@", "\"\<Registers\>\""}], "}"}], ",", 
                    RowBox[{"{", "\[IndentingNewLine]", 
                    RowBox[{"Text", "@", "\[IndentingNewLine]", 
                    RowBox[{"Grid", "[", 
                    RowBox[{
                    RowBox[{"Transpose", "@", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{
                    "R0", ",", "R1", ",", "R2", ",", "R3", ",", "R4", ",", 
                    "R5", ",", "R6", ",", "R7", ",", "R8", ",", "R9", ",", 
                    "R10", ",", "R11", ",", "R12"}], "}"}], ",", 
                    "\[IndentingNewLine]", 
                    RowBox[{"bf", "/@", 
                    RowBox[{"(", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"If", "[", 
                    RowBox[{
                    RowBox[{"format", "\[Equal]", "\"\<signed\>\""}], ",", 
                    RowBox[{"from2sComplement", "[", 
                    RowBox[{"#", ",", "$wordLength"}], "]"}], ",", "#"}], 
                    "]"}], "&"}], "/@", 
                    RowBox[{"registerList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ")"}]}], ",", 
                    "\[IndentingNewLine]", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"If", "[", 
                    RowBox[{
                    RowBox[{"format", "\[Equal]", "\"\<signed\>\""}], ",", 
                    RowBox[{"from2sComplement", "[", 
                    RowBox[{"#", ",", "$wordLength"}], "]"}], ",", "#"}], 
                    "]"}], "&"}], "/@", 
                    RowBox[{"registerList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}]}], "}"}]}], ",", 
                    RowBox[{"Alignment", "\[Rule]", "Right"}]}], "]"}]}], 
                    "}"}]}], "}"}], "]"}]}], "\[IndentingNewLine]", "}"}], 
                  "]"}]}]}], "}"}], ",", 
              RowBox[{"Dynamic", "[", "tab", "]"}]}], "\[IndentingNewLine]", 
             "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"offset", ",", "50", ",", "\"\<memory start\>\""}], "}"}],
            ",", "0", ",", "512", ",", "1"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"lim", ",", "limit", ",", "\"\<computation limit\>\""}], 
            "}"}], ",", "1", ",", "10000", ",", "10"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"i", ",", "1", ",", "\"\<computation step\>\""}], "}"}], 
           ",", "1", ",", 
           RowBox[{"Dynamic", "@", "len"}], ",", "1", ",", 
           RowBox[{"Appearance", "\[Rule]", "\"\<Open\>\""}]}], "}"}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"format", ",", "\"\<signed\>\"", ",", "\"\<format\>\""}], 
            "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"\"\<signed\>\"", ",", "\"\<2's complement\>\""}], 
            "}"}]}], "}"}], ",", "\[IndentingNewLine]", 
         RowBox[{"Initialization", "\[RuleDelayed]", "\[IndentingNewLine]", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"code", "=", "program"}], ";", 
            RowBox[{"codeRun", "=", 
             RowBox[{"asLoadRunDecoded", "[", 
              RowBox[{"code", ",", "start", ",", "lim", ",", "wl"}], "]"}]}], 
            ";", 
            RowBox[{"len", "=", 
             RowBox[{"Length", "[", "codeRun", "]"}]}], ";", 
            RowBox[{
             RowBox[{"{", 
              RowBox[{
              "memories", ",", "pcList", ",", "statusList", ",", 
               "registerList"}], "}"}], "=", 
             RowBox[{"Transpose", "[", "codeRun", "]"}]}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"memoryRules", "=", 
             RowBox[{
              RowBox[{
               RowBox[{"Drop", "[", 
                RowBox[{
                 RowBox[{"ArrayRules", "[", "#", "]"}], ",", 
                 RowBox[{"-", "1"}]}], "]"}], "&"}], "/@", "memories"}]}], 
            " ", ";", "\[IndentingNewLine]", 
            RowBox[{"memoryRules", "=", 
             RowBox[{
              RowBox[{
               RowBox[{"Map", "[", 
                RowBox[{
                 RowBox[{
                  RowBox[{
                   RowBox[{"#", "[", 
                    RowBox[{"[", 
                    RowBox[{"1", ",", "1"}], "]"}], "]"}], "\[Rule]", 
                   RowBox[{"asPrint", "[", 
                    RowBox[{
                    RowBox[{"#1", "[", 
                    RowBox[{"[", "2", "]"}], "]"}], ",", "format"}], "]"}]}], 
                  "&"}], ",", "#"}], "]"}], "&"}], "/@", "memoryRules"}]}], 
            ";", "\[IndentingNewLine]", 
            RowBox[{"memoryRules", "=", 
             RowBox[{"Map", "[", 
              RowBox[{
               RowBox[{
                RowBox[{"SortBy", "[", 
                 RowBox[{"#", ",", 
                  RowBox[{
                   RowBox[{"#", "[", 
                    RowBox[{"[", "1", "]"}], "]"}], "&"}]}], "]"}], "&"}], 
               ",", "memoryRules"}], "]"}]}], ";", "\[IndentingNewLine]", 
            RowBox[{"keys", "=", 
             RowBox[{
              RowBox[{
               RowBox[{"Map", "[", 
                RowBox[{
                 RowBox[{
                  RowBox[{"#", "[", 
                   RowBox[{"[", "1", "]"}], "]"}], "&"}], ",", "#"}], "]"}], 
               "&"}], "/@", "memoryRules"}]}], ";"}], ")"}]}]}], "]"}]}], 
      "}"}], "]"}]}], "]"}]}]}], "Input",
 InitializationCell->
  True,ExpressionUUID->"59444a36-8be1-47fa-93b9-74f7546e6e1a"]
}, Closed]]
}, Closed]]
}, Closed]],

Cell[CellGroupData[{

Cell["Running", "Chapter",ExpressionUUID->"7b8347bf-ba03-4cba-98e9-fda7bd0d6fbf"],

Cell[CellGroupData[{

Cell["Input and output of code", "Section",ExpressionUUID->"bde34c08-97df-454b-b27d-fc11fa523c81"],

Cell[CellGroupData[{

Cell["Interface to loading from file", "Subsection",ExpressionUUID->"e463df51-951f-42dc-8e4d-f112e55e2c59"],

Cell["Use the interface below to load your code and assemble it", "Text",ExpressionUUID->"6967238a-fe68-4e59-8035-2477d466a4ad"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"DynamicModule", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"file", "=", "\"\<File not yet chosen\>\""}], ",", "code", ",", 
     RowBox[{"codeLoaded", "=", "False"}], ",", "interface", ",", 
     RowBox[{"interfaceLoaded", "=", "False"}]}], "}"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"Column", "[", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Row", "@", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"Item", "[", 
          RowBox[{"FileNameSetter", "[", 
           RowBox[{"Dynamic", "[", "file", "]"}], "]"}], "]"}], ",", 
         RowBox[{"Spacer", "@", "10"}], ",", 
         RowBox[{"Panel", "[", 
          RowBox[{"Dynamic", "[", "file", "]"}], "]"}]}], "}"}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"Row", "@", 
       RowBox[{"{", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"Button", "[", 
          RowBox[{
          "\"\<Import assembly language program\>\"", ",", 
           "\[IndentingNewLine]", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"code", "=", 
              RowBox[{"Import", "[", 
               RowBox[{"file", ",", "\"\<Text\>\""}], "]"}]}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"codeLoaded", "=", "True"}], ";", 
             RowBox[{"interfaceLoaded", "=", "False"}]}], ")"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"Method", "\[Rule]", "\"\<Queued\>\""}]}], "]"}], ",", 
         RowBox[{"Spacer", "@", "10"}], ",", "\[IndentingNewLine]", 
         RowBox[{"Button", "[", 
          RowBox[{"\"\<Run program\>\"", ",", "\[IndentingNewLine]", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"If", "[", 
              RowBox[{"codeLoaded", ",", "None", ",", 
               RowBox[{
                RowBox[{"code", "=", 
                 RowBox[{"Import", "[", 
                  RowBox[{"file", ",", "\"\<Text\>\""}], "]"}]}], ";", 
                "\[IndentingNewLine]", 
                RowBox[{"codeLoaded", "=", "True"}]}]}], "]"}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"interface", "=", 
              RowBox[{"runMyProgram", "[", "\[IndentingNewLine]", 
               RowBox[{"code", ",", "  ", 
                RowBox[{"(*", " ", 
                 RowBox[{
                 "the", " ", "code", " ", "you", " ", "are", " ", "running"}],
                  " ", "*)"}], "\[IndentingNewLine]", "60", "           ", 
                RowBox[{"(*", " ", 
                 RowBox[{"memory", " ", "start", " ", "address"}], " ", 
                 "*)"}], ",", "\[IndentingNewLine]", "100"}], "         ", 
               RowBox[{"(*", " ", 
                RowBox[{
                "maximum", " ", "number", " ", "of", " ", "program", " ", 
                 "steps"}], " ", "*)"}], "]"}]}], ";", "\[IndentingNewLine]", 
             RowBox[{"interfaceLoaded", "=", "True"}]}], ")"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"Method", "\[Rule]", "\"\<Queued\>\""}]}], "]"}]}], 
        "}"}]}], ",", "\[IndentingNewLine]", 
      RowBox[{"Dynamic", "@", "\[IndentingNewLine]", 
       RowBox[{"If", "[", 
        RowBox[{
        "interfaceLoaded", ",", "interface", ",", "\[IndentingNewLine]", 
         RowBox[{"If", "[", 
          RowBox[{"codeLoaded", ",", "code", ",", 
           RowBox[{"Panel", "@", "\"\<Waiting for instructions...\>\""}]}], 
          "]"}]}], "]"}]}]}], "\[IndentingNewLine]", "}"}], "]"}], ",", 
   RowBox[{"SaveDefinitions", "\[Rule]", "True"}]}], "]"}]], "Input"],

Cell[BoxData[
 DynamicModuleBox[{$CellContext`file$$ = 
  "/Users/matthew/southDevonCollege/CS/Teaching/Bubble.asm", \
$CellContext`code$$ = 
  "MOV R0 , # 58 ; data initialisation ;\nSTR R0 , 1\nMOV R0 , # 1\nSTR R0 , \
2\nMOV R0 , # 199 ; change data so we need to bubble more than once ;\nSTR R0 \
, 3\nMOV R0 , # 142\nSTR R0 , 4\nMOV R0 , # 130\nSTR R0 , 5   ; data now in \
place ;\n\nMOV R0 , # 0 ; clear temp register R0 ;\nMOV R3 , # 0 ; clear \
register R3 ;\nMOV R4 , # 0 ; clear register R4 ;\nMOV R5 , # 0 ; clear \
register R5 ;\nMOV R6 , # 0 ; clear temp register R6 ;\nMOV R7 , # 0 ; clear \
'swapped' register R7 ;\n\nMOV R1 , # 5 ; 5 elements in the array ;\nMOV R2 , \
# 1 ; 1st element is in location 1 ;\nMOV R8 , R2  ; now we need to save the \
initial memory location ;\n\nstart : CMP R1 , # 2\nBLT cleanUp ; finished if \
there are fewer than 2 locations to handle ;\n\nloopOuter : MOV R2 , R8 ; \
start at beginning of loop ;\nMOV R7 , # 0 ; this is a repeat loop so clear \
the swapped flag ;\n\nloopInner : LDR R3 , R2 ; 1st memory value of 2 ;\nADD \
R2 , R2 , # 1\nLDR R4 , R2 ; 2nd memory value ;\nCMP R3 , R4\nBGT swap ; we \
will sort in ascending order ;\ncond : CMP R1 , R2\nBEQ testOuter ; inner \
loop is finished ;\nB loopInner   ; go around the loop1 again ;\n\nswap : MOV \
R0 , R0 ; label: swap memory locations ;\nMOV R7 , # 1 ; we have a swap so \
update value of R7 to reflect this ;\nMOV R6 , R2 ; save current value of R2 \
;\nSTR R3 , R2 ; R2 holds pointer to the 2nd value which should become the \
1st ;\nSUB R2 , R2 , # 1 ; back up memory pointer ;\nSTR R4 , R2 ; 1st value \
now becomes the 2nd ;\nMOV R2 , R6 ; restore previous value of R2 ;\nB cond ; \
resume inner loop ;\n\ntestOuter : CMP R7 , # 1 ; have elements been swapped? \
;\nBLT cleanUp ; No: outer loop is finished ;\nB loopOuter ; Yes: go around \
the outer loop again ;\n\ncleanUp : MOV R0 , # 0 ; clean up &c ;\n\nHALT", \
$CellContext`codeLoaded$$ = True, $CellContext`interface$$ = 
  DynamicModule[{$CellContext`tab, $CellContext`code$$37, \
$CellContext`codeRun, $CellContext`memoryRules, $CellContext`len, \
$CellContext`keys, $CellContext`pcList, $CellContext`statusList, \
$CellContext`registerList, $CellContext`memories, $CellContext`showCode = 
    False}, 
   Column[{
     Panel["Assembler version 2.2"], 
     Panel[
      Dynamic[
       If[$CellContext`showCode, $CellContext`code$$37, "Code hidden"]]], 
     Manipulate[
      Column[{
        Row[{
          Button[
          "Reload code", $CellContext`code$$37 = 
            "MOV R0 , # 58 ; data initialisation ;\nSTR R0 , 1\nMOV R0 , # 1\n\
STR R0 , 2\nMOV R0 , # 199 ; change data so we need to bubble more than once \
;\nSTR R0 , 3\nMOV R0 , # 142\nSTR R0 , 4\nMOV R0 , # 130\nSTR R0 , 5   ; \
data now in place ;\n\nMOV R0 , # 0 ; clear temp register R0 ;\nMOV R3 , # 0 \
; clear register R3 ;\nMOV R4 , # 0 ; clear register R4 ;\nMOV R5 , # 0 ; \
clear register R5 ;\nMOV R6 , # 0 ; clear temp register R6 ;\nMOV R7 , # 0 ; \
clear 'swapped' register R7 ;\n\nMOV R1 , # 5 ; 5 elements in the array ;\n\
MOV R2 , # 1 ; 1st element is in location 1 ;\nMOV R8 , R2  ; now we need to \
save the initial memory location ;\n\nstart : CMP R1 , # 2\nBLT cleanUp ; \
finished if there are fewer than 2 locations to handle ;\n\nloopOuter : MOV \
R2 , R8 ; start at beginning of loop ;\nMOV R7 , # 0 ; this is a repeat loop \
so clear the swapped flag ;\n\nloopInner : LDR R3 , R2 ; 1st memory value of \
2 ;\nADD R2 , R2 , # 1\nLDR R4 , R2 ; 2nd memory value ;\nCMP R3 , R4\nBGT \
swap ; we will sort in ascending order ;\ncond : CMP R1 , R2\nBEQ testOuter ; \
inner loop is finished ;\nB loopInner   ; go around the loop1 again ;\n\nswap \
: MOV R0 , R0 ; label: swap memory locations ;\nMOV R7 , # 1 ; we have a swap \
so update value of R7 to reflect this ;\nMOV R6 , R2 ; save current value of \
R2 ;\nSTR R3 , R2 ; R2 holds pointer to the 2nd value which should become the \
1st ;\nSUB R2 , R2 , # 1 ; back up memory pointer ;\nSTR R4 , R2 ; 1st value \
now becomes the 2nd ;\nMOV R2 , R6 ; restore previous value of R2 ;\nB cond ; \
resume inner loop ;\n\ntestOuter : CMP R7 , # 1 ; have elements been swapped? \
;\nBLT cleanUp ; No: outer loop is finished ;\nB loopOuter ; Yes: go around \
the outer loop again ;\n\ncleanUp : MOV R0 , # 0 ; clean up &c ;\n\nHALT"; \
$CellContext`codeRun = $CellContext`asLoadRunDecoded[$CellContext`code$$37, \
$CellContext`offset, $CellContext`lim, 16]; $CellContext`len = 
            Length[$CellContext`codeRun]; {$CellContext`memories, \
$CellContext`pcList, $CellContext`statusList, $CellContext`registerList} = 
            Transpose[$CellContext`codeRun]; $CellContext`memoryRules = 
            Map[Drop[
               
               ArrayRules[#], -1]& , $CellContext`memories]; \
$CellContext`memoryRules = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                 
                 Part[#, 
                  2], $CellContext`format]& , #]& , $CellContext`memoryRules]; \
$CellContext`memoryRules = 
            Map[SortBy[#, 
               Part[#, 1]& ]& , $CellContext`memoryRules]; $CellContext`keys = 
            Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules]; Null], 
          Button["Show code", $CellContext`showCode = True], 
          Button["Hide code", $CellContext`showCode = False]}], 
        TabView[{"base 10" -> Column[{
             Grid[{{
                Text["Memory"], 
                Text["PC"], 
                Text["Status"], 
                Text["CIR"]}, {
                TabView[
                 Thread[
                  ({#, #2}& )[
                   Part[$CellContext`keys, $CellContext`i], 
                   Normal[
                    Part[$CellContext`memoryRules, $CellContext`i]]]], 
                 Part[$CellContext`pcList, $CellContext`i], 
                 Appearance -> {"Limited", 10}], 
                Text[
                 Part[$CellContext`pcList, $CellContext`i]], 
                Text[
                 Part[$CellContext`statusList, $CellContext`i]], 
                Text[
                 $CellContext`asPrint[
                  Part[
                   Part[$CellContext`memories, $CellContext`i], 
                   
                   Part[$CellContext`pcList, $CellContext`i]], \
$CellContext`format]]}}], 
             Grid[{{
                Text["Registers"]}, {
                Text[
                 
                 Grid[{{$CellContext`R0, $CellContext`R1, $CellContext`R2, \
$CellContext`R3, $CellContext`R4, $CellContext`R5, $CellContext`R6, \
$CellContext`R7, $CellContext`R8, $CellContext`R9, $CellContext`R10, \
$CellContext`R11, $CellContext`R12}, 
                   Map[If[$CellContext`format == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList, $CellContext`i]]}]]}}]}], 
          "base 2" -> Column[{
             Grid[{{
                Text["Memory"], 
                Text["PC"], 
                Text["Status"], 
                Text["CIR"]}, {
                TabView[
                 Thread[
                  ({#, #2}& )[
                   Part[$CellContext`keys, $CellContext`i], 
                   Normal[
                    Part[$CellContext`memoryRules, $CellContext`i]]]], 
                 Part[$CellContext`pcList, $CellContext`i], 
                 Appearance -> {"Limited", 10}], 
                Text[
                 Part[$CellContext`pcList, $CellContext`i]], 
                Text[
                 Part[$CellContext`statusList, $CellContext`i]], 
                Text[
                 $CellContext`asPrint[
                  Part[
                   Part[$CellContext`memories, $CellContext`i], 
                   
                   Part[$CellContext`pcList, $CellContext`i]], \
$CellContext`format]]}}], 
             Grid[{{
                Text["Registers"]}, {
                Text[
                 Grid[
                  
                  Transpose[{{$CellContext`R0, $CellContext`R1, \
$CellContext`R2, $CellContext`R3, $CellContext`R4, $CellContext`R5, \
$CellContext`R6, $CellContext`R7, $CellContext`R8, $CellContext`R9, \
$CellContext`R10, $CellContext`R11, $CellContext`R12}, 
                    Map[$CellContext`bf, 
                    Map[If[$CellContext`format == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList, $CellContext`i]]], 
                    Map[If[$CellContext`format == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList, $CellContext`i]]}], 
                  Alignment -> Right]]}}]}]}, 
         Dynamic[$CellContext`tab]]}], {{$CellContext`offset, 50, 
        "memory start"}, 0, 512, 
       1}, {{$CellContext`lim, 100, "computation limit"}, 1, 10000, 
       10}, {{$CellContext`i, 1, "computation step"}, 1, 
       Dynamic[$CellContext`len], 1, Appearance -> 
       "Open"}, {{$CellContext`format, "signed", "format"}, {
       "signed", "2's complement"}}, 
      Initialization :> ($CellContext`code$$37 = 
        "MOV R0 , # 58 ; data initialisation ;\nSTR R0 , 1\nMOV R0 , # 1\nSTR \
R0 , 2\nMOV R0 , # 199 ; change data so we need to bubble more than once ;\n\
STR R0 , 3\nMOV R0 , # 142\nSTR R0 , 4\nMOV R0 , # 130\nSTR R0 , 5   ; data \
now in place ;\n\nMOV R0 , # 0 ; clear temp register R0 ;\nMOV R3 , # 0 ; \
clear register R3 ;\nMOV R4 , # 0 ; clear register R4 ;\nMOV R5 , # 0 ; clear \
register R5 ;\nMOV R6 , # 0 ; clear temp register R6 ;\nMOV R7 , # 0 ; clear \
'swapped' register R7 ;\n\nMOV R1 , # 5 ; 5 elements in the array ;\nMOV R2 , \
# 1 ; 1st element is in location 1 ;\nMOV R8 , R2  ; now we need to save the \
initial memory location ;\n\nstart : CMP R1 , # 2\nBLT cleanUp ; finished if \
there are fewer than 2 locations to handle ;\n\nloopOuter : MOV R2 , R8 ; \
start at beginning of loop ;\nMOV R7 , # 0 ; this is a repeat loop so clear \
the swapped flag ;\n\nloopInner : LDR R3 , R2 ; 1st memory value of 2 ;\nADD \
R2 , R2 , # 1\nLDR R4 , R2 ; 2nd memory value ;\nCMP R3 , R4\nBGT swap ; we \
will sort in ascending order ;\ncond : CMP R1 , R2\nBEQ testOuter ; inner \
loop is finished ;\nB loopInner   ; go around the loop1 again ;\n\nswap : MOV \
R0 , R0 ; label: swap memory locations ;\nMOV R7 , # 1 ; we have a swap so \
update value of R7 to reflect this ;\nMOV R6 , R2 ; save current value of R2 \
;\nSTR R3 , R2 ; R2 holds pointer to the 2nd value which should become the \
1st ;\nSUB R2 , R2 , # 1 ; back up memory pointer ;\nSTR R4 , R2 ; 1st value \
now becomes the 2nd ;\nMOV R2 , R6 ; restore previous value of R2 ;\nB cond ; \
resume inner loop ;\n\ntestOuter : CMP R7 , # 1 ; have elements been swapped? \
;\nBLT cleanUp ; No: outer loop is finished ;\nB loopOuter ; Yes: go around \
the outer loop again ;\n\ncleanUp : MOV R0 , # 0 ; clean up &c ;\n\nHALT"; \
$CellContext`codeRun = $CellContext`asLoadRunDecoded[$CellContext`code$$37, 
          60, $CellContext`lim, 16]; $CellContext`len = 
        Length[$CellContext`codeRun]; {$CellContext`memories, \
$CellContext`pcList, $CellContext`statusList, $CellContext`registerList} = 
        Transpose[$CellContext`codeRun]; $CellContext`memoryRules = Map[Drop[
           
           ArrayRules[#], -1]& , $CellContext`memories]; \
$CellContext`memoryRules = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
             
             Part[#, 2], $CellContext`format]& , #]& , \
$CellContext`memoryRules]; $CellContext`memoryRules = 
        Map[SortBy[#, 
           Part[#, 1]& ]& , $CellContext`memoryRules]; $CellContext`keys = 
        Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules]; Null)]}], 
   DynamicModuleValues :> {}], $CellContext`interfaceLoaded$$ = True}, 
  TagBox[GridBox[{
     {
      TemplateBox[{ItemBox[
         TemplateBox[{
           Dynamic[$CellContext`file$$], "Open", All}, "FileNameSetterBoxes"],
          StripOnInput -> False],InterpretationBox[
         StyleBox[
          GraphicsBox[{}, ImageSize -> {10, 0}, BaselinePosition -> Baseline],
           "CacheGraphics" -> False], 
         Spacer[10]],PanelBox[
         DynamicBox[
          ToBoxes[$CellContext`file$$, StandardForm]]]},
       "RowDefault"]},
     {
      TemplateBox[{
       ButtonBox[
        "\"Import assembly language program\"", 
         ButtonFunction :> ($CellContext`code$$ = 
           Import[$CellContext`file$$, "Text"]; $CellContext`codeLoaded$$ = 
           True; $CellContext`interfaceLoaded$$ = False), Method -> "Queued", 
         Appearance -> Automatic, Evaluator -> Automatic],InterpretationBox[
         StyleBox[
          GraphicsBox[{}, ImageSize -> {10, 0}, BaselinePosition -> Baseline],
           "CacheGraphics" -> False], 
         Spacer[10]],ButtonBox[
        "\"Run program\"", 
         ButtonFunction :> (
          If[$CellContext`codeLoaded$$, 
            None, $CellContext`code$$ = 
             Import[$CellContext`file$$, "Text"]; $CellContext`codeLoaded$$ = 
             True]; $CellContext`interface$$ = \
$CellContext`runMyProgram[$CellContext`code$$, 60, 
             100]; $CellContext`interfaceLoaded$$ = True), Method -> "Queued",
          Appearance -> Automatic, Evaluator -> Automatic]},
       "RowDefault"]},
     {
      DynamicBox[ToBoxes[
        If[$CellContext`interfaceLoaded$$, $CellContext`interface$$, 
         If[$CellContext`codeLoaded$$, $CellContext`code$$, 
          Panel["Waiting for instructions..."]]], StandardForm],
       ImageSizeCache->{790., {499., 508.}}]}
    },
    DefaultBaseStyle->"Column",
    GridBoxAlignment->{"Columns" -> {{Left}}},
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
   "Column"],
  DynamicModuleValues:>{},
  Initialization:>{$CellContext`runMyProgram[
      Pattern[$CellContext`program$, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`start$, 
        Blank[Integer]], 50], 
      Optional[
       Pattern[$CellContext`limit$, 
        Blank[Integer]], 100], 
      Optional[
       Pattern[$CellContext`wl$, 
        Blank[]], 16]] := 
    DynamicModule[{$CellContext`tab, 
       FE`code$$37, $CellContext`codeRun, $CellContext`memoryRules, \
$CellContext`len, $CellContext`keys, $CellContext`pcList, \
$CellContext`statusList, $CellContext`registerList, $CellContext`memories, \
$CellContext`showCode = False}, 
      Column[{
        Panel[
         StringJoin["Assembler version ", 
          ToString[$CellContext`$assemblyVersion]]], 
        Panel[
         Dynamic[
          If[$CellContext`showCode, FE`code$$37, "Code hidden"]]], 
        Manipulate[
         Column[{
           Row[{
             Button[
             "Reload code", 
              FE`code$$37 = $CellContext`program$; $CellContext`codeRun = \
$CellContext`asLoadRunDecoded[
                FE`code$$37, $CellContext`offset, $CellContext`lim, \
$CellContext`wl$]; $CellContext`len = 
               Length[$CellContext`codeRun]; {$CellContext`memories, \
$CellContext`pcList, $CellContext`statusList, $CellContext`registerList} = 
               Transpose[$CellContext`codeRun]; $CellContext`memoryRules = 
               Map[Drop[
                  
                  ArrayRules[#], -1]& , $CellContext`memories]; \
$CellContext`memoryRules = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                    
                    Part[#, 
                    2], $CellContext`format]& , #]& , \
$CellContext`memoryRules]; $CellContext`memoryRules = 
               Map[SortBy[#, 
                  Part[#, 
                   1]& ]& , $CellContext`memoryRules]; $CellContext`keys = 
               Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules]; Null], 
             Button["Show code", $CellContext`showCode = True], 
             Button["Hide code", $CellContext`showCode = False]}], 
           TabView[{"base 10" -> Column[{
                Grid[{{
                   Text["Memory"], 
                   Text["PC"], 
                   Text["Status"], 
                   Text["CIR"]}, {
                   TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys, $CellContext`i], 
                    Normal[
                    Part[$CellContext`memoryRules, $CellContext`i]]]], 
                    Part[$CellContext`pcList, $CellContext`i], 
                    Appearance -> {"Limited", 10}], 
                   Text[
                    Part[$CellContext`pcList, $CellContext`i]], 
                   Text[
                    Part[$CellContext`statusList, $CellContext`i]], 
                   Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories, $CellContext`i], 
                    
                    Part[$CellContext`pcList, $CellContext`i]], \
$CellContext`format]]}}], 
                Grid[{{
                   Text["Registers"]}, {
                   Text[
                    
                    Grid[{{$CellContext`R0, $CellContext`R1, $CellContext`R2, \
$CellContext`R3, $CellContext`R4, $CellContext`R5, $CellContext`R6, \
$CellContext`R7, $CellContext`R8, $CellContext`R9, $CellContext`R10, \
$CellContext`R11, $CellContext`R12}, 
                    Map[If[$CellContext`format == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList, $CellContext`i]]}]]}}]}], 
             "base 2" -> Column[{
                Grid[{{
                   Text["Memory"], 
                   Text["PC"], 
                   Text["Status"], 
                   Text["CIR"]}, {
                   TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys, $CellContext`i], 
                    Normal[
                    Part[$CellContext`memoryRules, $CellContext`i]]]], 
                    Part[$CellContext`pcList, $CellContext`i], 
                    Appearance -> {"Limited", 10}], 
                   Text[
                    Part[$CellContext`pcList, $CellContext`i]], 
                   Text[
                    Part[$CellContext`statusList, $CellContext`i]], 
                   Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories, $CellContext`i], 
                    
                    Part[$CellContext`pcList, $CellContext`i]], \
$CellContext`format]]}}], 
                Grid[{{
                   Text["Registers"]}, {
                   Text[
                    Grid[
                    
                    Transpose[{{$CellContext`R0, $CellContext`R1, \
$CellContext`R2, $CellContext`R3, $CellContext`R4, $CellContext`R5, \
$CellContext`R6, $CellContext`R7, $CellContext`R8, $CellContext`R9, \
$CellContext`R10, $CellContext`R11, $CellContext`R12}, 
                    Map[$CellContext`bf, 
                    Map[If[$CellContext`format == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList, $CellContext`i]]], 
                    Map[If[$CellContext`format == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList, $CellContext`i]]}], 
                    Alignment -> Right]]}}]}]}, 
            Dynamic[$CellContext`tab]]}], {{$CellContext`offset, 50, 
           "memory start"}, 0, 512, 
          1}, {{$CellContext`lim, $CellContext`limit$, "computation limit"}, 
          1, 10000, 10}, {{$CellContext`i, 1, "computation step"}, 1, 
          Dynamic[$CellContext`len], 1, Appearance -> 
          "Open"}, {{$CellContext`format, "signed", "format"}, {
          "signed", "2's complement"}}, 
         Initialization :> (
          FE`code$$37 = $CellContext`program$; $CellContext`codeRun = \
$CellContext`asLoadRunDecoded[
            FE`code$$37, $CellContext`start$, $CellContext`lim, \
$CellContext`wl$]; $CellContext`len = 
           Length[$CellContext`codeRun]; {$CellContext`memories, \
$CellContext`pcList, $CellContext`statusList, $CellContext`registerList} = 
           Transpose[$CellContext`codeRun]; $CellContext`memoryRules = 
           Map[Drop[
              
              ArrayRules[#], -1]& , $CellContext`memories]; \
$CellContext`memoryRules = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                
                Part[#, 
                 2], $CellContext`format]& , #]& , $CellContext`memoryRules]; \
$CellContext`memoryRules = 
           Map[SortBy[#, 
              Part[#, 1]& ]& , $CellContext`memoryRules]; $CellContext`keys = 
           Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules]; Null)]}]], 
    Attributes[$CellContext`start$] = {Temporary}, 
    Attributes[$CellContext`wl$] = {Temporary}, FE`code$$37 = 
    "STR R11 , 0010 ; this is a comment and is terminated by: ;\nl2 : LDR R1 \
, 0012 ; this is another comment: note spaces! ;\nl1 : SUB R10 , R8 , R3\nl3 \
: MOV R7 , # -0019 ; not every line needs a comment ;\nl3a : MVN R8 , # 4096 \
; may create a negative number! ;\nl4 : MOV R5 , R2 ; ;\nl5 : CMP R2 , R3 ; \
empty comments (see above) must also have spaces ;\nl6 : BEQ l11\nl7 : ADD \
R12 , R10 , # 00066\nMOV R3 , R2\n\nl10 : B l2\nl11 : HALT", \
$CellContext`$assemblyVersion = 2.2, $CellContext`asLoadRunDecoded[
      Pattern[$CellContext`code, 
       Blank[String]], 
      Optional[
       Pattern[$CellContext`offset, 
        Blank[]], 0], 
      Optional[
       Pattern[$CellContext`maxSteps, 
        Blank[]], 100], 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[Integer]], 16]] := 
    With[{$CellContext`iniPc = 
       1 + $CellContext`offset, $CellContext`iniStatus = 
       "EQ", $CellContext`iniRegisters = 
       Range[
        0, 12], $CellContext`codeArray = \
$CellContext`asGrab[$CellContext`code]}, 
      With[{$CellContext`loadedCode = \
$CellContext`asLoad[$CellContext`codeArray, $CellContext`offset, \
$CellContext`wl], $CellContext`locs = \
$CellContext`asLocations[$CellContext`codeArray, $CellContext`offset]}, 
       With[{$CellContext`iniMem = 
         Map[$CellContext`asDecodeDebugging, $CellContext`loadedCode]}, 
        NestWhileList[
         $CellContext`asStep[$CellContext`locs], {$CellContext`iniMem, \
$CellContext`iniPc, $CellContext`iniStatus, $CellContext`iniRegisters}, 
         Part[#, 2] > 0& , 
         1, $CellContext`maxSteps]]]], $CellContext`codeArray = {
     "" -> "STR R11,0010;this is a comment and is terminated by;", "l2" -> 
      "LDR R1,0012 ;this is another comment:note lack of spaces!;", "l1" -> 
      "SUB R10, R8, R3", "l3" -> 
      "MOV R7,#-0019 ; not every line needs a comment ;", "l3a" -> 
      "MVN R8, #4096 ; may create a negative number! ;", "l4" -> 
      "MOV R5, R2;;", "l5" -> 
      "CMP R2, R3 ; empty comments (see above) need no longer have spaces ;", 
      "l6" -> "BEQ l11", "l7" -> "ADD R12, R10, #00066", "" -> "MOV R3, R2", 
      "l10" -> "B l2", "l11" -> "HALT"}, $CellContext`asGrab[
      Pattern[$CellContext`code, 
       Blank[String]]] := Map[Switch[#, {
        Blank[]}, "" -> Part[#, 1], {
        Blank[], 
        Blank[]}, Part[#, 1] -> Part[#, 2], {}, 
       (SlotSequence[1]& )[]]& , 
      Map[StringSplit[#, " : "]& , 
       StringSplit[$CellContext`code, "\n"]]], $CellContext`loadedCode = 
    SparseArray[
     Automatic, {2567}, 0, {
      1, {{0, 12}, {{2556}, {2557}, {2558}, {2559}, {2560}, {2561}, {2562}, {
        2563}, {2564}, {2565}, {2566}, {2567}}}, {
       "STR R11,0010;this is a comment and is terminated by;", 
        "LDR R1,0012 ;this is another comment:note lack of spaces!;", 
        "SUB R10, R8, R3", "MOV R7,#-0019 ; not every line needs a comment ;",
         "MVN R8, #4096 ; may create a negative number! ;", "MOV R5, R2;;", 
        "CMP R2, R3 ; empty comments (see above) need no longer have spaces \
;", "BEQ l11", "ADD R12, R10, #00066", "MOV R3, R2", "B l2", 
        "HALT"}}], $CellContext`asLoad[
      Pattern[$CellContext`p, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`offset, 
        Blank[]], 0], 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] := With[{$CellContext`len = Length[$CellContext`p]}, 
      SparseArray[
      Range[$CellContext`len] + $CellContext`offset -> 
       Map[Part[#, 2]& , $CellContext`p]]], $CellContext`locs = 
    Association[
     "" -> 2565, "l2" -> 2557, "l1" -> 2558, "l3" -> 2559, "l3a" -> 2560, 
      "l4" -> 2561, "l5" -> 2562, "l6" -> 2563, "l7" -> 2564, "l10" -> 2566, 
      "l11" -> 2567], $CellContext`asLocations[
      Pattern[$CellContext`p, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`offset, 
        Blank[]], 0]] := Association[
      MapIndexed[First[#] -> First[#2] + $CellContext`offset& , 
       Normal[$CellContext`p]]], $CellContext`iniMem = 
    "MOV R0 , # 91 ; data initialisation ;\nSTR R0 , 2\nMOV R0 , # 101\nSTR \
R0 , 3\nMOV R0 , # 23\nSTR R0 , 4\nMOV R0 , # 0\nMOV R1 , # 3 ; 3 elements in \
the array ;\nMOV R2 , # 2 ; 1st element is in location 2 ;\n", \
$CellContext`asDecodeDebugging[
      Pattern[$CellContext`p, 
       Blank[String]]] := Part[
      FunctionalParsers`ParseShortest[$CellContext`pINSTRUCTION][
       FunctionalParsers`ToTokens[$CellContext`p, {";", "#", ":", ","}]], 1, 
      2], $CellContext`asDecodeDebugging[
      Pattern[$CellContext`v, 
       Blank[]]] := $CellContext`v, FunctionalParsers`ParseShortest[
      Pattern[FunctionalParsers`Private`p, 
       Blank[]]] := 
    With[{FunctionalParsers`Private`parsed = FunctionalParsers`Private`p[#]}, 
      If[FunctionalParsers`Private`parsed === {}, 
       FunctionalParsers`Private`parsed, {
        First[
         SortBy[FunctionalParsers`Private`parsed, Length[
           Part[#, 1]]& ]]}]]& , 
    TagSet[FunctionalParsers`ParseShortest, 
     MessageName[FunctionalParsers`ParseShortest, "usage"], 
     "ParseShortest[p] takes the output with the shortests rest string."], \
$CellContext`pINSTRUCTION[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseApply[
      ToExpression["asmInstr"], 
      FunctionalParsers`ParseAlternativeComposition[
       FunctionalParsers`ParseSequentialComposition[If[
         And[Length[#] > 0, "OUT" === First[#]], {{
           Rest[#], "OUT"}}, {}]& , 
        FunctionalParsers`ParsePredicate[StringMatchQ[#, 
          Repeated[
           Alternatives[WordCharacter, "_"]]]& ]], 
       FunctionalParsers`ParseSequentialComposition[$CellContext`pOPCODE1, 
        FunctionalParsers`ParseSequentialComposition[
         FunctionalParsers`ParseSequentialCompositionPickLeft[$CellContext`\
pREG, If[
           And[Length[#] > 0, "," === First[#]], {{
             Rest[#], ","}}, {}]& ], $CellContext`pMEMREF]], 
       FunctionalParsers`ParseSequentialComposition[$CellContext`pOPCODE2, 
        FunctionalParsers`ParseSequentialComposition[
         FunctionalParsers`ParseSequentialCompositionPickLeft[$CellContext`\
pREG, If[
           And[Length[#] > 0, "," === First[#]], {{
             Rest[#], ","}}, {}]& ], 
         FunctionalParsers`ParseSequentialComposition[
          
          FunctionalParsers`ParseSequentialCompositionPickLeft[$CellContext`\
pREG, If[
            And[Length[#] > 0, "," === First[#]], {{
              Rest[#], ","}}, {}]& ], $CellContext`pOPERAND]]], 
       FunctionalParsers`ParseSequentialComposition[$CellContext`pOPCODE3, 
        FunctionalParsers`ParseSequentialComposition[
         FunctionalParsers`ParseSequentialCompositionPickLeft[$CellContext`\
pREG, If[
           And[Length[#] > 0, "," === First[#]], {{
             Rest[#], ","}}, {}]& ], $CellContext`pOPERAND]], 
       FunctionalParsers`ParseSequentialComposition[If[
         And[Length[#] > 0, "CMP" === First[#]], {{
           Rest[#], "CMP"}}, {}]& , 
        FunctionalParsers`ParseSequentialComposition[
         FunctionalParsers`ParseSequentialCompositionPickLeft[$CellContext`\
pREG, If[
           And[Length[#] > 0, "," === First[#]], {{
             Rest[#], ","}}, {}]& ], $CellContext`pOPERAND]], 
       FunctionalParsers`ParseSequentialComposition[$CellContext`pBCOND, \
$CellContext`pLABEL], If[
        And[Length[#] > 0, "HALT" === First[#]], {{
          Rest[#], "HALT"}}, {}]& ]][FunctionalParsers`Private`xs$], 
    Attributes[FunctionalParsers`Private`xs$] = {Temporary}, 
    FunctionalParsers`ParseApply[
      Pattern[FunctionalParsers`Private`f, 
       Blank[]], 
      Pattern[FunctionalParsers`Private`p, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := Map[{
       Part[#, 1], 
       FunctionalParsers`Private`f[
        Part[#, 2]]}& , 
      FunctionalParsers`Private`p[FunctionalParsers`Private`xs]], 
    FunctionalParsers`ParseApply[{
       Pattern[FunctionalParsers`Private`fNo, 
        Blank[]], 
       Pattern[FunctionalParsers`Private`fYes, 
        Blank[]]}, 
      Pattern[FunctionalParsers`Private`p, 
       Blank[]]] := 
    With[{FunctionalParsers`Private`res = FunctionalParsers`Private`p[#]}, 
      Map[{
        Part[#, 1], 
        If[Part[#, 2] === {}, FunctionalParsers`Private`fNo, 
         FunctionalParsers`Private`fYes[
          Part[#, 2]]]}& , FunctionalParsers`Private`res]]& , 
    TagSet[FunctionalParsers`ParseApply, 
     MessageName[FunctionalParsers`ParseApply, "usage"], 
     "ParseApply[f,p] applies the function f to the output of p. \
ParseApply[{fNo, fYes}, p] applies the function fNo not unsuccessful parsing \
and the function fYes the output of successful parsing using p."], 
    FunctionalParsers`ParseAlternativeComposition[
      Pattern[FunctionalParsers`Private`args, 
       BlankSequence[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := Apply[Join, 
      Map[#[FunctionalParsers`Private`xs]& , {
       FunctionalParsers`Private`args}]], 
    TagSet[FunctionalParsers`ParseAlternativeComposition, 
     MessageName[FunctionalParsers`ParseAlternativeComposition, "usage"], 
     "ParseAlternativeComposition parses a composition of two or more \
alternative parsers."], FunctionalParsers`ParseSequentialComposition[
      Pattern[FunctionalParsers`Private`p1, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := 
    FunctionalParsers`Private`p1[FunctionalParsers`Private`xs], 
    FunctionalParsers`ParseSequentialComposition[
      Pattern[FunctionalParsers`Private`args, 
       BlankSequence[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := Condition[
      With[{
       FunctionalParsers`Private`parsers = {FunctionalParsers`Private`args}}, 
       Fold[FunctionalParsers`Private`ParseComposeWithResults[#2][#]& , 
        First[FunctionalParsers`Private`parsers][
        FunctionalParsers`Private`xs], 
        Rest[FunctionalParsers`Private`parsers]]], 
      Length[{FunctionalParsers`Private`args}] > 1], 
    TagSet[FunctionalParsers`ParseSequentialComposition, 
     MessageName[FunctionalParsers`ParseSequentialComposition, "usage"], 
     "ParseSequentialComposition parses a sequential composition of two or \
more parsers."], FunctionalParsers`Private`ParseComposeWithResults[
      Pattern[FunctionalParsers`Private`p, 
       Blank[]]][{}] := {}, FunctionalParsers`Private`ParseComposeWithResults[
      Pattern[FunctionalParsers`Private`p, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`res, {
        Repeated[{
          Blank[], 
          Blank[]}]}]] := Block[{FunctionalParsers`Private`t}, 
      (Flatten[#, 1]& )[
       Map[
        Function[{FunctionalParsers`Private`r}, 
         If[
         FunctionalParsers`Private`r === {}, {}, 
          FunctionalParsers`Private`t = FunctionalParsers`Private`p[
             Part[FunctionalParsers`Private`r, 1]]; 
          If[FunctionalParsers`Private`t === {}, {}, 
            Map[{
              Part[#, 1], {
               Part[FunctionalParsers`Private`r, 2], 
               Part[#, 2]}}& , FunctionalParsers`Private`t]]]], 
        FunctionalParsers`Private`res]]], FunctionalParsers`ParsePredicate[
      Pattern[FunctionalParsers`Private`pred, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := If[
      TrueQ[
       And[Length[FunctionalParsers`Private`xs] > 0, 
        FunctionalParsers`Private`pred[
         First[FunctionalParsers`Private`xs]]]], {{
        Rest[FunctionalParsers`Private`xs], 
        First[FunctionalParsers`Private`xs]}}, {}], 
    TagSet[FunctionalParsers`ParsePredicate, 
     MessageName[FunctionalParsers`ParsePredicate, "usage"], 
     "ParsePredicate[p] parses strings that give True for the predicate p."], \
$CellContext`pOPCODE1[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseAlternativeComposition[If[
       And[Length[#] > 0, "LDR" === First[#]], {{
         Rest[#], "LDR"}}, {}]& , If[
       And[Length[#] > 0, "STR" === First[#]], {{
         Rest[#], "STR"}}, {}]& ][FunctionalParsers`Private`xs$], 
    FunctionalParsers`ParseSequentialCompositionPickLeft[
      Pattern[FunctionalParsers`Private`p1, 
       Blank[]], 
      Pattern[FunctionalParsers`Private`p2, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := FunctionalParsers`ParseApply[Part[#, 1]& , 
      FunctionalParsers`ParseSequentialComposition[
      FunctionalParsers`Private`p1, FunctionalParsers`Private`p2]][
     FunctionalParsers`Private`xs], 
    TagSet[FunctionalParsers`ParseSequentialCompositionPickLeft, 
     MessageName[
     FunctionalParsers`ParseSequentialCompositionPickLeft, "usage"], 
     "ParseSequentialCompositionPickLeft[p1,p2] drops the output of the p2 \
parser."], $CellContext`pREG[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseApply[
      ToExpression["asmRegProcess"], 
      FunctionalParsers`ParseAlternativeComposition[If[
        And[Length[#] > 0, "R0" === First[#]], {{
          Rest[#], "R0"}}, {}]& , If[
        And[Length[#] > 0, "R1" === First[#]], {{
          Rest[#], "R1"}}, {}]& , If[
        And[Length[#] > 0, "R2" === First[#]], {{
          Rest[#], "R2"}}, {}]& , If[
        And[Length[#] > 0, "R3" === First[#]], {{
          Rest[#], "R3"}}, {}]& , If[
        And[Length[#] > 0, "R4" === First[#]], {{
          Rest[#], "R4"}}, {}]& , If[
        And[Length[#] > 0, "R5" === First[#]], {{
          Rest[#], "R5"}}, {}]& , If[
        And[Length[#] > 0, "R6" === First[#]], {{
          Rest[#], "R6"}}, {}]& , If[
        And[Length[#] > 0, "R7" === First[#]], {{
          Rest[#], "R7"}}, {}]& , If[
        And[Length[#] > 0, "R8" === First[#]], {{
          Rest[#], "R8"}}, {}]& , If[
        And[Length[#] > 0, "R9" === First[#]], {{
          Rest[#], "R9"}}, {}]& , If[
        And[Length[#] > 0, "R10" === First[#]], {{
          Rest[#], "R10"}}, {}]& , If[
        And[Length[#] > 0, "R11" === First[#]], {{
          Rest[#], "R11"}}, {}]& , If[
        And[Length[#] > 0, "R12" === First[#]], {{
          Rest[#], "R12"}}, {}]& ]][
     FunctionalParsers`Private`xs$], $CellContext`pMEMREF[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseApply[
      ToExpression["asmMemRef"], 
      FunctionalParsers`ParseAlternativeComposition[
       FunctionalParsers`ParseApply[ToExpression, 
        FunctionalParsers`ParsePredicate[And[
          StringMatchQ[#, NumberString], 0 <= ToExpression[#] <= 
          65535]& ]], $CellContext`pREG]][
     FunctionalParsers`Private`xs$], $CellContext`pOPCODE2[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseAlternativeComposition[If[
       And[Length[#] > 0, "ADD" === First[#]], {{
         Rest[#], "ADD"}}, {}]& , If[
       And[Length[#] > 0, "SUB" === First[#]], {{
         Rest[#], "SUB"}}, {}]& , If[
       And[Length[#] > 0, "AND" === First[#]], {{
         Rest[#], "AND"}}, {}]& , If[
       And[Length[#] > 0, "ORR" === First[#]], {{
         Rest[#], "ORR"}}, {}]& , If[
       And[Length[#] > 0, "EOR" === First[#]], {{
         Rest[#], "EOR"}}, {}]& , If[
       And[Length[#] > 0, "LSL" === First[#]], {{
         Rest[#], "LSL"}}, {}]& , If[
       And[Length[#] > 0, "LSR" === First[#]], {{
         Rest[#], "LSR"}}, {}]& ][
     FunctionalParsers`Private`xs$], $CellContext`pOPERAND[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseApply[
      ToExpression["asmOperand"], 
      FunctionalParsers`ParseAlternativeComposition[$CellContext`pLITERAL, \
$CellContext`pREG]][FunctionalParsers`Private`xs$], $CellContext`pLITERAL[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseApply[
      ToExpression["asmLiteralProcess"], 
      FunctionalParsers`ParseSequentialCompositionPickRight[If[
        And[Length[#] > 0, "#" === First[#]], {{
          Rest[#], "#"}}, {}]& , 
       FunctionalParsers`ParseApply[ToExpression, 
        FunctionalParsers`ParsePredicate[And[
          StringMatchQ[#, NumberString], -4294967296 <= ToExpression[#] <= 
          4294967295]& ]]]][FunctionalParsers`Private`xs$], 
    FunctionalParsers`ParseSequentialCompositionPickRight[
      Pattern[FunctionalParsers`Private`p1, 
       Blank[]], 
      Pattern[FunctionalParsers`Private`p2, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := FunctionalParsers`ParseApply[Part[#, 2]& , 
      FunctionalParsers`ParseSequentialComposition[
      FunctionalParsers`Private`p1, FunctionalParsers`Private`p2]][
     FunctionalParsers`Private`xs], 
    TagSet[FunctionalParsers`ParseSequentialCompositionPickRight, 
     MessageName[
     FunctionalParsers`ParseSequentialCompositionPickRight, "usage"], 
     "ParseSequentialCompositionPickRight[p1,p2] drops the output of the p1 \
parser."], $CellContext`pOPCODE3[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseAlternativeComposition[If[
       And[Length[#] > 0, "MOV" === First[#]], {{
         Rest[#], "MOV"}}, {}]& , If[
       And[Length[#] > 0, "CMP" === First[#]], {{
         Rest[#], "CMP"}}, {}]& , If[
       And[Length[#] > 0, "MVN" === First[#]], {{
         Rest[#], "MVN"}}, {}]& ][
     FunctionalParsers`Private`xs$], $CellContext`pBCOND[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseAlternativeComposition[If[
       And[Length[#] > 0, "B" === First[#]], {{
         Rest[#], "B"}}, {}]& , If[
       And[Length[#] > 0, "BEQ" === First[#]], {{
         Rest[#], "BEQ"}}, {}]& , If[
       And[Length[#] > 0, "BNE" === First[#]], {{
         Rest[#], "BNE"}}, {}]& , If[
       And[Length[#] > 0, "BGT" === First[#]], {{
         Rest[#], "BGT"}}, {}]& , If[
       And[Length[#] > 0, "BLT" === First[#]], {{
         Rest[#], "BLT"}}, {}]& ][
     FunctionalParsers`Private`xs$], $CellContext`pLABEL[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParsePredicate[StringMatchQ[#, 
       Repeated[
        Alternatives[WordCharacter, "_"]]]& ][FunctionalParsers`Private`xs$], 
    FunctionalParsers`ToTokens[
      Pattern[FunctionalParsers`Private`text, 
       Blank[String]]] := StringSplit[FunctionalParsers`Private`text], 
    FunctionalParsers`ToTokens[
      Pattern[FunctionalParsers`Private`text, 
       Blank[String]], {}] := StringSplit[FunctionalParsers`Private`text], 
    FunctionalParsers`ToTokens[
      Pattern[FunctionalParsers`Private`text, 
       Blank[String]], 
      Pattern[FunctionalParsers`Private`terminals, {
        RepeatedNull[
         Blank[String]]}]] := StringSplit[
      StringReplace[FunctionalParsers`Private`text, 
       Map[# -> StringJoin[" ", #, " "]& , 
        FunctionalParsers`Private`terminals]]], FunctionalParsers`ToTokens[
      Pattern[FunctionalParsers`Private`text, 
       Blank[]], "EBNF"] := 
    FunctionalParsers`ToTokens[
     FunctionalParsers`Private`text, {
      "|", ",", ";", "=", "[", "]", "(", ")", "{", "}"}], 
    TagSet[FunctionalParsers`ToTokens, 
     MessageName[FunctionalParsers`ToTokens, "usage"], 
     "ToTokens[text] breaks down text into tokens. ToTokens[text,terminals] \
breaks down text using specified terminals. ToTokens[text,\"EBNF\"] has a \
special implementation for parsing EBNF code. (This function is becoming \
obsolete, use ParseToTokens.)"], $CellContext`asStep[
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := $CellContext`asExecute[
      $CellContext`asDecodeDebugging[
       $CellContext`asFetch[$CellContext`mem, $CellContext`pc]], \
$CellContext`labels][{$CellContext`mem, $CellContext`pc, $CellContext`status, \
$CellContext`registers}], $CellContext`asExecute[
      Pattern[$CellContext`instr, 
       Blank[]], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := $CellContext`asInterpretStep[$CellContext`instr, \
$CellContext`labels][{$CellContext`mem, $CellContext`pc, $CellContext`status, \
$CellContext`registers}], $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[
           Pattern[$CellContext`m, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {
      ReplacePart[$CellContext`mem, 
       Part[$CellContext`registers, $CellContext`m + 1] -> 
       Part[$CellContext`registers, $CellContext`r + 1]], $CellContext`pc + 
      1, $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          Pattern[$CellContext`m, 
           Blank[]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {
      ReplacePart[$CellContext`mem, $CellContext`m -> 
       Part[$CellContext`registers, $CellContext`r + 1]], $CellContext`pc + 
      1, $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[
           Pattern[$CellContext`m, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 
      1, $CellContext`status, 
      ReplacePart[$CellContext`registers, $CellContext`r + 1 -> 
       Part[$CellContext`mem, 
         Part[$CellContext`registers, $CellContext`m + 
          1]]]}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          Pattern[$CellContext`m, 
           Blank[]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 
      1, $CellContext`status, 
      ReplacePart[$CellContext`registers, $CellContext`r + 1 -> 
       Part[$CellContext`mem, $CellContext`m]]}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{
        Pattern[$CellContext`op, 
         Alternatives["MOV", "MVN"]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[
           Pattern[$CellContext`l, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 
      1, $CellContext`status, 
      ReplacePart[$CellContext`registers, $CellContext`r + 
        1 -> $CellContext`operation[$CellContext`op, \
$CellContext`$wordLength][$CellContext`l]]}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{
        Pattern[$CellContext`op, 
         Alternatives["MOV", "MVN"]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 
      1, $CellContext`status, 
      ReplacePart[$CellContext`registers, $CellContext`r1 + 
        1 -> $CellContext`operation[$CellContext`op, $CellContext`$wordLength][
         Part[$CellContext`registers, $CellContext`r2 + 
          1]]]}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 1, 
      $CellContext`compare[
       Part[$CellContext`registers, $CellContext`r1 + 1], 
       Part[$CellContext`registers, $CellContext`r2 + 
        1]], $CellContext`registers}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[
           Pattern[$CellContext`l, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 1, 
      $CellContext`compare[
       Part[$CellContext`registers, $CellContext`r + 
        1], $CellContext`l], $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{
        Pattern[$CellContext`op, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], {
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[
            Pattern[$CellContext`l, 
             Blank[]]]]}}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := 
    With[{$CellContext`f = $CellContext`operation[$CellContext`op, \
$CellContext`$wordLength]}, {$CellContext`mem, $CellContext`pc + 
       1, $CellContext`status, 
       ReplacePart[$CellContext`registers, $CellContext`r1 + 
         1 -> $CellContext`f[
          
          Part[$CellContext`registers, $CellContext`r2 + 
           1], $CellContext`l]]}], $CellContext`asInterpretStep[
      $CellContext`asmInstr[{
        Pattern[$CellContext`op, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], {
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]], 
          $CellContext`asmOperand[
           $CellContext`asmReg[
            Pattern[$CellContext`r3, 
             Blank[]]]]}}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := 
    With[{$CellContext`f = $CellContext`operation[$CellContext`op, \
$CellContext`$wordLength]}, {$CellContext`mem, $CellContext`pc + 
       1, $CellContext`status, 
       ReplacePart[$CellContext`registers, $CellContext`r1 + 
         1 -> $CellContext`f[
          Part[$CellContext`registers, $CellContext`r2 + 1], 
          
          Part[$CellContext`registers, $CellContext`r3 + 
           1]]]}], $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"B", 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      $CellContext`labels[$CellContext`l], $CellContext`status, \
$CellContext`registers}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"BEQ", 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      If[$CellContext`status == "EQ", 
       $CellContext`labels[$CellContext`l], $CellContext`pc + 
       1], $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"BNE", 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      If[$CellContext`status != "EQ", 
       $CellContext`labels[$CellContext`l], $CellContext`pc + 
       1], $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"BLT", 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      If[$CellContext`status == "LT", 
       $CellContext`labels[$CellContext`l], $CellContext`pc + 
       1], $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"BGT", 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      If[$CellContext`status == "GT", 
       $CellContext`labels[$CellContext`l], $CellContext`pc + 
       1], $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"OUT", 
        Pattern[$CellContext`msg, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := (MessageDialog[
       StringJoin["PC: ", 
        ToString[$CellContext`pc], 
        "\n", $CellContext`msg]]; {$CellContext`mem, $CellContext`pc + 
       1, $CellContext`status, $CellContext`registers}), \
$CellContext`asInterpretStep[
      $CellContext`asmInstr["HALT"], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      0, $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      Pattern[$CellContext`instr, 
       Blank[]], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 
      1, $CellContext`status, $CellContext`registers}, \
$CellContext`asmInstr[] := $CellContext`asmInstr, $CellContext`asmReg[] := \
$CellContext`asmReg, $CellContext`asmMemRef[] := $CellContext`asmMemRef, \
$CellContext`asmOperand[] := $CellContext`asmOperand, \
$CellContext`asmLiteral[] := $CellContext`asmLiteral, $CellContext`operation[
     "ADD", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 
       16]] := $CellContext`binChop[# + #2, $CellContext`wl]& , \
$CellContext`operation["SUB", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 
       16]] := $CellContext`binChop[# + ($CellContext`binNeg[#2, \
$CellContext`wl] + 1), $CellContext`wl]& , $CellContext`operation["AND", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] = BitAnd, $CellContext`operation["ORR", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] = BitOr, $CellContext`operation["EOR", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] = BitXor, $CellContext`operation["LSL", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] := $CellContext`binChop[
      BitShiftLeft[#], $CellContext`wl]& , $CellContext`operation["LSR", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] = BitShiftRight, $CellContext`operation["MOV", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] = Identity, $CellContext`operation["MVN", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 
       16]] := $CellContext`binNeg[#, $CellContext`wl]& , $CellContext`binChop[
      Pattern[$CellContext`x, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`numBits, 
        Blank[]], 16]] := 
    Mod[$CellContext`x, 2^$CellContext`numBits], $CellContext`binNeg[
      Pattern[$CellContext`x, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`numBits, 
        Blank[]], 16]] := 
    BitXor[$CellContext`x, 2^$CellContext`numBits - 
      1], $CellContext`$wordLength = 16, $CellContext`compare[
      Pattern[$CellContext`x, 
       Blank[]], 
      Pattern[$CellContext`y, 
       Blank[]]] := 
    Which[$CellContext`x == $CellContext`y, 
      "EQ", $CellContext`x < $CellContext`y, 
      "LT", $CellContext`x > $CellContext`y, "GT", True, 
      "Fail"], $CellContext`asFetch[
      Pattern[$CellContext`mem, 
       Blank[]], 
      Pattern[$CellContext`pc, 
       Blank[]]] := 
    Part[$CellContext`mem, $CellContext`pc], $CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[
           Pattern[$CellContext`m, 
            Blank[]]]]}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r], " , R", 
      ToString[$CellContext`m]], $CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          Pattern[$CellContext`m, 
           Blank[]]]}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r], " , ", 
      ToString[$CellContext`m]], $CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[
           Pattern[$CellContext`l, 
            Blank[]]]]}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r], " , # ", 
      ToString[
       If[$CellContext`format == "signed", 
        $CellContext`from2sComplement[$CellContext`l], $CellContext`l]]], \
$CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]]]}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r1], " , R", 
      ToString[$CellContext`r2]], $CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], {
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[
            Pattern[$CellContext`l, 
             Blank[]]]]}}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r1], " , R", 
      ToString[$CellContext`r2], " , # ", 
      ToString[
       If[$CellContext`format == "signed", 
        $CellContext`from2sComplement[$CellContext`l], $CellContext`l]]], \
$CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], {
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]], 
          $CellContext`asmOperand[
           $CellContext`asmReg[
            Pattern[$CellContext`r3, 
             Blank[]]]]}}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r1], " , R", 
      ToString[$CellContext`r2], " , R", 
      ToString[$CellContext`r3]], $CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`branchString, 
         Blank[]], 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`branchString, " ", 
      ToString[$CellContext`l]], $CellContext`asPrint[
      $CellContext`asmInstr["HALT"], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := "HALT", $CellContext`asPrint[
      Pattern[$CellContext`instr, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := ToString[$CellContext`instr], 
    Attributes[$CellContext`from2sComplement] = {Listable}, Condition[
      $CellContext`from2sComplement[
       Pattern[$CellContext`n, 
        Blank[Integer]], 
       Optional[
        Pattern[$CellContext`numBits, 
         Blank[Integer]], 16]], 
      $CellContext`fits[$CellContext`n, $CellContext`numBits - 
       1]] := $CellContext`n, Condition[
      $CellContext`from2sComplement[
       Pattern[$CellContext`n, 
        Blank[Integer]], 
       Optional[
        Pattern[$CellContext`numBits, 
         Blank[Integer]], 16]], 
      $CellContext`fits[$CellContext`n, $CellContext`numBits]] := \
-$CellContext`binNeg[$CellContext`n, $CellContext`numBits] - 
     1, $CellContext`from2sComplement[
      Pattern[$CellContext`n, 
       Blank[Integer]], 
      Optional[
       Pattern[$CellContext`numBits, 
        Blank[Integer]], 16]] := $CellContext`from2sComplement[
      $CellContext`binChop[$CellContext`n, $CellContext`numBits], \
$CellContext`numBits], $CellContext`fits[
      Pattern[$CellContext`n, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`numBits, 
        Blank[]], 16]] := 
    BitLength[$CellContext`n] <= $CellContext`numBits, $CellContext`i = 
    1, $CellContext`bf[
      Pattern[$CellContext`x, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`b, 
        Blank[]], 2]] := BaseForm[$CellContext`x, $CellContext`b]}]], "Output",\
ExpressionUUID->"6051a861-d179-4af8-8086-456b99913c61"]
}, {2}]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Code examples (inline)", "Subsection",ExpressionUUID->"8a4951d7-6178-4a67-ad29-a0e36ddfb847"],

Cell[CellGroupData[{

Cell["Example 1", "Subsubsection",ExpressionUUID->"f82d79d7-233c-4d6e-9dd1-d35b62fd6548"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "code1", "=", "\[IndentingNewLine]", 
   "\"\<STR R11, 0010 ;this is a comment and is terminated by: ;\nl2 : LDR \
R1, 0012 ;this is another comment;\nl1 : SUB R10, R8, R3\nl3 : MOV R7, #-0019 \
; not every line needs a comment ;\nl3a : MVN R8, # 4096 ; may create a \
negative number! ;\nl4 : MOV R5, R2 ;;\nl5 : CMP R2, R3 ; empty comments (see \
above) need not have spaces ;\nl6 : BEQ l11\nl7 : ADD R12, R10, #00066\nMOV \
R3, R2\n\nl10 : B l2\nl11 : HALT\>\""}], ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"bec52cec-e00c-49ef-9c63-8c2580ccd921"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Run the code for example 1", "Subsubsection",ExpressionUUID->"8895dc0b-711b-47c2-bebe-a4af259a3fda"],

Cell[TextData[{
 "Use the ",
 StyleBox["runMyProgram", "Input"],
 " function to execute your program and inspect the results. Currently the \
registers hold the value of their own register value as this aids testing. If \
you want to reset all registers to zero, try adding assembler code to do this \
before the main body of your code."
}], "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"runMyProgram", "[", "\[IndentingNewLine]", 
  RowBox[{"code1", ",", " ", 
   RowBox[{"(*", " ", 
    RowBox[{"the", " ", "code", " ", "you", " ", "are", " ", "running"}], " ",
     "*)"}], "\[IndentingNewLine]", "60", "           ", 
   RowBox[{"(*", " ", 
    RowBox[{"memory", " ", "start", " ", "address"}], " ", "*)"}], ",", 
   "\[IndentingNewLine]", "100"}], "         ", 
  RowBox[{"(*", " ", 
   RowBox[{
   "maximum", " ", "number", " ", "of", " ", "program", " ", "steps"}], " ", 
   "*)"}], "]"}]], "Code",ExpressionUUID->"211f22e9-89e7-4236-ae53-\
353f53ba6e8c"],

Cell[BoxData[
 DynamicModuleBox[{$CellContext`tab$$ = 1, $CellContext`code$$ = 
  "STR R11, 0010 ;this is a comment and is terminated by: ;\nl2 : LDR R1, \
0012 ;this is another comment;\nl1 : SUB R10, R8, R3\nl3 : MOV R7, #-0019 ; \
not every line needs a comment ;\nl3a : MVN R8, # 4096 ; may create a \
negative number! ;\nl4 : MOV R5, R2 ;;\nl5 : CMP R2, R3 ; empty comments (see \
above) need not have spaces ;\nl6 : BEQ l11\nl7 : ADD R12, R10, #00066\nMOV \
R3, R2\n\nl10 : B l2\nl11 : HALT", $CellContext`codeRun$$ = {{
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 12}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"]}}], 61, "EQ", {0, 1, 2, 3, 4, 5, 6, 7, 8,
     9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 62, "EQ", {0, 1, 2, 3, 4, 5, 6, 
    7, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 63, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    7, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 64, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    7, 8, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 65, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    65517, 8, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 66, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 67, "EQ", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 68, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 69, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 70, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 71, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 62, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 63, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 64, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 65, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 66, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 67, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 68, "EQ", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 72, "EQ", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 0, "EQ", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}}, $CellContext`memoryRules$$ = {{
   61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> "SUB R10 , R8 , R3", 64 -> 
    "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> "MOV R5 , R2", 67 -> 
    "CMP R2 , R3", 68 -> "BEQ l11", 69 -> "ADD R12 , R10 , # 66", 70 -> 
    "MOV R3 , R2", 71 -> "B l2", 72 -> "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}}, $CellContext`len$$ = 
  20, $CellContext`keys$$ = {{61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
   72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63,
    64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 
   69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10,
    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 
   66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 
   71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62,
    63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 
   68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
   72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63,
    64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 
   69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10,
    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 
   66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 
   71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
   72}}, $CellContext`pcList$$ = {61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
  62, 63, 64, 65, 66, 67, 68, 72, 0}, $CellContext`statusList$$ = {
  "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "LT", "LT", "LT", "LT", "LT", 
   "LT", "LT", "LT", "LT", "LT", "EQ", "EQ", 
   "EQ"}, $CellContext`registerList$$ = CompressedData["
1:eJxTTMoPymNiYGAQAWJeBghgBGKQGDMQswAxKxCzATE7EHMAMScQcwExNxDz
UKCHgUZ6WInU8/Y/eXr+vydOD9MQ1eOOpodpiOr5+35UD6V6AMTGUfc=
  "], $CellContext`memories$$ = {
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 12}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"]}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}]}, $CellContext`showCode$$ = False}, 
  TagBox[GridBox[{
     {
      PanelBox["\<\"Assembler version 2.21\"\>"]},
     {
      PanelBox[
       DynamicBox[ToBoxes[
         If[$CellContext`showCode$$, $CellContext`code$$, "Code hidden"], 
         StandardForm],
        ImageSizeCache->{89., {1., 12.}}]]},
     {
      TagBox[
       StyleBox[
        DynamicModuleBox[{$CellContext`format$$ = "signed", $CellContext`i$$ =
          1, $CellContext`lim$$ = 100, $CellContext`offset$$ = 50, 
         Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
         Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, 
         Typeset`animvar$$ = 1, Typeset`name$$ = "\"untitled\"", 
         Typeset`specs$$ = {{{
            Hold[$CellContext`offset$$], 50, "memory start"}, 0, 512, 1}, {{
            Hold[$CellContext`lim$$], 100, "computation limit"}, 1, 10000, 
           10}, {{
            Hold[$CellContext`i$$], 1, "computation step"}, 1, 
           Dynamic[$CellContext`len$$], 1}, {{
            Hold[$CellContext`format$$], "signed", "format"}, {
           "signed", "2's complement"}}}, Typeset`size$$ = {
         713., {296.7010498046875, 305.2989501953125}}, Typeset`update$$ = 0, 
         Typeset`initDone$$, Typeset`skipInitDone$$ = 
         False, $CellContext`offset$607$$ = 0, $CellContext`lim$612$$ = 
         0, $CellContext`i$613$$ = 0, $CellContext`format$614$$ = False}, 
         DynamicBox[Manipulate`ManipulateBoxes[
          1, StandardForm, 
           "Variables" :> {$CellContext`format$$ = "signed", $CellContext`i$$ = 
             1, $CellContext`lim$$ = 100, $CellContext`offset$$ = 50}, 
           "ControllerVariables" :> {
             Hold[$CellContext`offset$$, $CellContext`offset$607$$, 0], 
             Hold[$CellContext`lim$$, $CellContext`lim$612$$, 0], 
             Hold[$CellContext`i$$, $CellContext`i$613$$, 0], 
             Hold[$CellContext`format$$, $CellContext`format$614$$, False]}, 
           "OtherVariables" :> {
            Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
             Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
             Typeset`specs$$, Typeset`size$$, Typeset`update$$, 
             Typeset`initDone$$, Typeset`skipInitDone$$}, "Body" :> Column[{
              Row[{
                Button[
                "Reload code", $CellContext`code$$ = 
                  "STR R11, 0010 ;this is a comment and is terminated by: ;\n\
l2 : LDR R1, 0012 ;this is another comment;\nl1 : SUB R10, R8, R3\nl3 : MOV \
R7, #-0019 ; not every line needs a comment ;\nl3a : MVN R8, # 4096 ; may \
create a negative number! ;\nl4 : MOV R5, R2 ;;\nl5 : CMP R2, R3 ; empty \
comments (see above) need not have spaces ;\nl6 : BEQ l11\nl7 : ADD R12, R10, \
#00066\nMOV R3, R2\n\nl10 : B l2\nl11 : HALT"; $CellContext`codeRun$$ = \
$CellContext`asLoadRunDecoded[$CellContext`code$$, $CellContext`offset$$, \
$CellContext`lim$$, 16]; $CellContext`len$$ = 
                  Length[$CellContext`codeRun$$]; {$CellContext`memories$$, \
$CellContext`pcList$$, $CellContext`statusList$$, \
$CellContext`registerList$$} = 
                  Transpose[$CellContext`codeRun$$]; \
$CellContext`memoryRules$$ = Map[Drop[
                    
                    ArrayRules[#], -1]& , $CellContext`memories$$]; \
$CellContext`memoryRules$$ = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                    
                    Part[#, 
                    2], $CellContext`format$$]& , #]& , \
$CellContext`memoryRules$$]; $CellContext`memoryRules$$ = 
                  Map[
                   SortBy[#, 
                    Part[#, 
                    1]& ]& , $CellContext`memoryRules$$]; $CellContext`keys$$ = 
                  Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules$$]; 
                 Null], 
                Button["Show code", $CellContext`showCode$$ = True], 
                Button["Hide code", $CellContext`showCode$$ = False]}], 
              TabView[{"base 10" -> Column[{
                   Grid[{{
                    Text["Memory"], 
                    Text["PC"], 
                    Text["Status"], 
                    Text["CIR"]}, {
                    TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys$$, $CellContext`i$$], 
                    Normal[
                    Part[$CellContext`memoryRules$$, $CellContext`i$$]]]], 
                    Part[$CellContext`pcList$$, $CellContext`i$$], 
                    Appearance -> {"Limited", 10}], 
                    Text[
                    Part[$CellContext`pcList$$, $CellContext`i$$]], 
                    Text[
                    Part[$CellContext`statusList$$, $CellContext`i$$]], 
                    Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories$$, $CellContext`i$$], 
                    
                    Part[$CellContext`pcList$$, $CellContext`i$$]], \
$CellContext`format$$]]}}], 
                   Grid[{{
                    Text["Registers"]}, {
                    Text[
                    
                    Grid[{{$CellContext`R0, $CellContext`R1, $CellContext`R2, \
$CellContext`R3, $CellContext`R4, $CellContext`R5, $CellContext`R6, \
$CellContext`R7, $CellContext`R8, $CellContext`R9, $CellContext`R10, \
$CellContext`R11, $CellContext`R12}, 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    
                    Part[$CellContext`registerList$$, \
$CellContext`i$$]]}]]}}]}], "base 2" -> Column[{
                   Grid[{{
                    Text["Memory"], 
                    Text["PC"], 
                    Text["Status"], 
                    Text["CIR"]}, {
                    TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys$$, $CellContext`i$$], 
                    Normal[
                    Part[$CellContext`memoryRules$$, $CellContext`i$$]]]], 
                    Part[$CellContext`pcList$$, $CellContext`i$$], 
                    Appearance -> {"Limited", 10}], 
                    Text[
                    Part[$CellContext`pcList$$, $CellContext`i$$]], 
                    Text[
                    Part[$CellContext`statusList$$, $CellContext`i$$]], 
                    Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories$$, $CellContext`i$$], 
                    
                    Part[$CellContext`pcList$$, $CellContext`i$$]], \
$CellContext`format$$]]}}], 
                   Grid[{{
                    Text["Registers"]}, {
                    Text[
                    Grid[
                    
                    Transpose[{{$CellContext`R0, $CellContext`R1, \
$CellContext`R2, $CellContext`R3, $CellContext`R4, $CellContext`R5, \
$CellContext`R6, $CellContext`R7, $CellContext`R8, $CellContext`R9, \
$CellContext`R10, $CellContext`R11, $CellContext`R12}, 
                    Map[$CellContext`bf, 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList$$, $CellContext`i$$]]], 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList$$, $CellContext`i$$]]}], 
                    Alignment -> Right]]}}]}]}, 
               Dynamic[$CellContext`tab$$]]}], 
           "Specifications" :> {{{$CellContext`offset$$, 50, "memory start"}, 
              0, 512, 1}, {{$CellContext`lim$$, 100, "computation limit"}, 1, 
              10000, 10}, {{$CellContext`i$$, 1, "computation step"}, 1, 
              Dynamic[$CellContext`len$$], 1, Appearance -> 
              "Open"}, {{$CellContext`format$$, "signed", "format"}, {
              "signed", "2's complement"}}}, "Options" :> {}, 
           "DefaultOptions" :> {}],
          ImageSizeCache->{779., {426., 435.}},
          SingleEvaluation->True],
         Deinitialization:>None,
         DynamicModuleValues:>{},
         Initialization:>(($CellContext`code$$ = 
            "STR R11, 0010 ;this is a comment and is terminated by: ;\nl2 : \
LDR R1, 0012 ;this is another comment;\nl1 : SUB R10, R8, R3\nl3 : MOV R7, \
#-0019 ; not every line needs a comment ;\nl3a : MVN R8, # 4096 ; may create \
a negative number! ;\nl4 : MOV R5, R2 ;;\nl5 : CMP R2, R3 ; empty comments \
(see above) need not have spaces ;\nl6 : BEQ l11\nl7 : ADD R12, R10, #00066\n\
MOV R3, R2\n\nl10 : B l2\nl11 : HALT"; $CellContext`codeRun$$ = \
$CellContext`asLoadRunDecoded[$CellContext`code$$, 60, $CellContext`lim$$, 
              16]; $CellContext`len$$ = 
            Length[$CellContext`codeRun$$]; {$CellContext`memories$$, \
$CellContext`pcList$$, $CellContext`statusList$$, \
$CellContext`registerList$$} = 
            Transpose[$CellContext`codeRun$$]; $CellContext`memoryRules$$ = 
            Map[Drop[
               
               ArrayRules[#], -1]& , $CellContext`memories$$]; \
$CellContext`memoryRules$$ = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                 
                 Part[#, 
                  2], $CellContext`format$$]& , #]& , \
$CellContext`memoryRules$$]; $CellContext`memoryRules$$ = 
            Map[SortBy[#, 
               Part[#, 
                1]& ]& , $CellContext`memoryRules$$]; $CellContext`keys$$ = 
            Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules$$]; Null); 
          Typeset`initDone$$ = True),
         SynchronousInitialization->True,
         UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
         UnsavedVariables:>{Typeset`initDone$$},
         UntrackedVariables:>{Typeset`size$$}], "Manipulate",
        Deployed->True,
        StripOnInput->False],
       Manipulate`InterpretManipulate[1]]}
    },
    DefaultBaseStyle->"Column",
    GridBoxAlignment->{"Columns" -> {{Left}}},
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
   "Column"],
  DynamicModuleValues:>{}]], "Output",ExpressionUUID->"635a7837-a719-4551-\
b565-2590564c0996"]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["Example 2", "Subsubsection",ExpressionUUID->"3badbe20-9023-4f33-a521-6f57d0f2b45e"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "code2", "=", "\[IndentingNewLine]", 
   "\"\<MOV R2 , # 10\nMOV R1 , # 0\nMOV R12 , # 999\nloop : ADD R1 , R1 , # \
1\nCMP R1 , R2\nBNE loop\nHALT\>\""}], ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"457f8781-0ac0-4846-8e83-44350f3c3b6a"]
}, Closed]],

Cell[CellGroupData[{

Cell["Example 3: Design of a counter/timer", "Subsubsection",ExpressionUUID->"59d4d8cc-9f5c-461c-91a4-99c43dff0ce2"],

Cell["\<\
Rough notes towards a design: use R1 to hold the incremented value
Use R2 to hold the wait value; assume in memory
(we could have used # immediate addressing)
loop until R1 = R2
; initialization
loop : ADD R1 R1 # 1 ; increment value in R1
CMP R1 R2 ; compare R1 with R2
BEQ finish ;  stop if R1 = R2
B loop ; otherwise go back to loop step
finish : HALT\
\>", "Text"]
}, Closed]],

Cell[CellGroupData[{

Cell["Final version of counter/timer", "Subsubsection",ExpressionUUID->"3dddb27f-d3cf-44c5-b688-b42b1c521de1"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "code4", "=", "\[IndentingNewLine]", 
   "\"\<MOV R2 , # 10\nMOV R1 , # 0\nloop : ADD R1 , R1 , # 1\nCMP R1 , R2\n\
BNE loop\nHALT\>\""}], ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"851ec032-9490-4ab8-b13d-12f65875ddf2"],

Cell[BoxData[
 RowBox[{"runMyProgram", "[", "code4", "]"}]], "Input"],

Cell[TextData[{
 "Test below ",
 StyleBox["fails",
  FontSlant->"Italic"],
 " if more than one step is executed, and breaks if code is stepped through \
to the second \[OpenCurlyDoubleQuote]instruction\[CloseCurlyDoubleQuote].  \
This is basically an \[OpenCurlyDoubleQuote]out of memory\
\[CloseCurlyDoubleQuote] crash, compounded by the fact that there is no ",
 StyleBox["HALT", "Input"],
 " instruction so the machine tries to execute the next instruction when \
there is none.  An object lesson in how (not) to fail gracefully.  The moral \
is: always include a ",
 StyleBox["HALT", "Input"],
 " instruction at the end of your code."
}], "Text"]
}, Closed]],

Cell[CellGroupData[{

Cell["More sophisticated memory addressing", "Subsubsection",ExpressionUUID->"a1d031f5-ec41-495d-b95a-25ae7f2f5173"],

Cell["\<\
The code below accesses memory locations which are stored in registers.  This \
allows us to actually do useful things with memory.\
\>", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "code5", "=", 
   "\"\<STR R11 , 0010 ; this is a comment and is terminated by: ;\nl2 : LDR \
R1 , 0012 ; this is another comment: note spaces! ;\nl2a : MOV R7 , # 10 ; \
prep the 10th memory address in R7 ;\nl2b : LDR R11 , R7 ; read its contents \
into R11 ;\n12c : STR R8 , R7 ; store R8 into the location ;\nl1 : SUB R10 , \
R8 , R3\nl3 : MOV R7 , # -0019 ; not every line needs a comment ;\nl3a : MVN \
R8 , # 4096 ; may create a negative number! ;\nl4 : MOV R5 , R2 ; ;\nl5 : CMP \
R2 , R3 ; empty comments (see above) must also have spaces ;\nl6 : BEQ l10\n\
l7 : ADD R12 , R10 , # 00066\nMOV R3 , R2\nHALT\>\""}], ";"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"1423806e-ed1c-4527-8531-5f2a7dedbaf5"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"runMyProgram", "[", 
  RowBox[{"code5", ",", "60", ",", " ", "100"}], "]"}]], "Input",
 InitializationCell->
  True,ExpressionUUID->"53ba27d2-5fc6-4f03-94fb-10ea35627c4d"],

Cell[BoxData[
 DynamicModuleBox[{$CellContext`tab$$ = 1, $CellContext`code$$ = 
  "STR R11 , 0010 ; this is a comment and is terminated by: ;\nl2 : LDR R1 , \
0012 ; this is another comment: note spaces! ;\nl2a : MOV R7 , # 10 ; prep \
the 10th memory address in R7 ;\nl2b : LDR R11 , R7 ; read its contents into \
R11 ;\n12c : STR R8 , R7 ; store R8 into the location ;\nl1 : SUB R10 , R8 , \
R3\nl3 : MOV R7 , # -0019 ; not every line needs a comment ;\nl3a : MVN R8 , \
# 4096 ; may create a negative number! ;\nl4 : MOV R5 , R2 ; ;\nl5 : CMP R2 , \
R3 ; empty comments (see above) must also have spaces ;\nl6 : BEQ l10\nl7 : \
ADD R12 , R10 , # 00066\nMOV R3 , R2\nHALT", $CellContext`codeRun$$ = {{
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 14}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"]}}], 61, "EQ", {0, 1, 2, 3, 4, 5, 6, 7, 8,
     9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 11}}], 62, "EQ", {0, 1, 2, 3, 4, 5, 6, 
    7, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 11}}], 63, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    7, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 11}}], 64, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    10, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 11}}], 65, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    10, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 66, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    10, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 67, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    10, 8, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 68, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    65517, 8, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 69, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 70, "EQ", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 71, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 72, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 73, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 74, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 0, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}}, $CellContext`memoryRules$$ = {{
   61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> "MOV R7 , # 10", 64 -> 
    "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> "SUB R10 , R8 , R3", 67 -> 
    "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> "MOV R5 , R2", 70 -> 
    "CMP R2 , R3", 71 -> "BEQ l10", 72 -> "ADD R12 , R10 , # 66", 73 -> 
    "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> 
    "HALT"}}, $CellContext`len$$ = 
  15, $CellContext`keys$$ = {{61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 
   73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10,
    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 
   64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 
   67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 
   70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 
   73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10,
    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 
   64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 
   67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 
   70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 
   73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10,
    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 
   74}}, $CellContext`pcList$$ = {61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
  72, 73, 74, 0}, $CellContext`statusList$$ = {
  "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "LT", "LT", 
   "LT", "LT", "LT"}, $CellContext`registerList$$ = CompressedData["
1:eJxTTMoPymNiYGDgB2JeBghgBGKQGDMQswAxKxCzATE7EHMAMScQcwExNxDz
UKCHgYp6uIaIHlYi9bz9T56e/++J08M0RPW4o+lhoqEeALoPI0Q=
  "], $CellContext`memories$$ = {
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 14}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"]}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}]}, $CellContext`showCode$$ = False}, 
  TagBox[GridBox[{
     {
      PanelBox["\<\"Assembler version 2.21\"\>"]},
     {
      PanelBox[
       DynamicBox[ToBoxes[
         If[$CellContext`showCode$$, $CellContext`code$$, "Code hidden"], 
         StandardForm],
        ImageSizeCache->{89., {1., 12.}}]]},
     {
      TagBox[
       StyleBox[
        DynamicModuleBox[{$CellContext`format$$ = "signed", $CellContext`i$$ =
          1, $CellContext`lim$$ = 100, $CellContext`offset$$ = 50, 
         Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
         Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, 
         Typeset`animvar$$ = 1, Typeset`name$$ = "\"untitled\"", 
         Typeset`specs$$ = {{{
            Hold[$CellContext`offset$$], 50, "memory start"}, 0, 512, 1}, {{
            Hold[$CellContext`lim$$], 100, "computation limit"}, 1, 10000, 
           10}, {{
            Hold[$CellContext`i$$], 1, "computation step"}, 1, 
           Dynamic[$CellContext`len$$], 1}, {{
            Hold[$CellContext`format$$], "signed", "format"}, {
           "signed", "2's complement"}}}, Typeset`size$$ = {
         713., {296.7010498046875, 305.2989501953125}}, Typeset`update$$ = 0, 
         Typeset`initDone$$, Typeset`skipInitDone$$ = 
         False, $CellContext`offset$657$$ = 0, $CellContext`lim$658$$ = 
         0, $CellContext`i$659$$ = 0, $CellContext`format$660$$ = False}, 
         DynamicBox[Manipulate`ManipulateBoxes[
          1, StandardForm, 
           "Variables" :> {$CellContext`format$$ = "signed", $CellContext`i$$ = 
             1, $CellContext`lim$$ = 100, $CellContext`offset$$ = 50}, 
           "ControllerVariables" :> {
             Hold[$CellContext`offset$$, $CellContext`offset$657$$, 0], 
             Hold[$CellContext`lim$$, $CellContext`lim$658$$, 0], 
             Hold[$CellContext`i$$, $CellContext`i$659$$, 0], 
             Hold[$CellContext`format$$, $CellContext`format$660$$, False]}, 
           "OtherVariables" :> {
            Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
             Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
             Typeset`specs$$, Typeset`size$$, Typeset`update$$, 
             Typeset`initDone$$, Typeset`skipInitDone$$}, "Body" :> Column[{
              Row[{
                Button[
                "Reload code", $CellContext`code$$ = 
                  "STR R11 , 0010 ; this is a comment and is terminated by: ;\
\nl2 : LDR R1 , 0012 ; this is another comment: note spaces! ;\nl2a : MOV R7 \
, # 10 ; prep the 10th memory address in R7 ;\nl2b : LDR R11 , R7 ; read its \
contents into R11 ;\n12c : STR R8 , R7 ; store R8 into the location ;\nl1 : \
SUB R10 , R8 , R3\nl3 : MOV R7 , # -0019 ; not every line needs a comment ;\n\
l3a : MVN R8 , # 4096 ; may create a negative number! ;\nl4 : MOV R5 , R2 ; ;\
\nl5 : CMP R2 , R3 ; empty comments (see above) must also have spaces ;\nl6 : \
BEQ l10\nl7 : ADD R12 , R10 , # 00066\nMOV R3 , R2\nHALT"; \
$CellContext`codeRun$$ = $CellContext`asLoadRunDecoded[$CellContext`code$$, \
$CellContext`offset$$, $CellContext`lim$$, 16]; $CellContext`len$$ = 
                  Length[$CellContext`codeRun$$]; {$CellContext`memories$$, \
$CellContext`pcList$$, $CellContext`statusList$$, \
$CellContext`registerList$$} = 
                  Transpose[$CellContext`codeRun$$]; \
$CellContext`memoryRules$$ = Map[Drop[
                    
                    ArrayRules[#], -1]& , $CellContext`memories$$]; \
$CellContext`memoryRules$$ = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                    
                    Part[#, 
                    2], $CellContext`format$$]& , #]& , \
$CellContext`memoryRules$$]; $CellContext`memoryRules$$ = 
                  Map[SortBy[#, 
                    Part[#, 
                    1]& ]& , $CellContext`memoryRules$$]; $CellContext`keys$$ = 
                  Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules$$]; 
                 Null], 
                Button["Show code", $CellContext`showCode$$ = True], 
                Button["Hide code", $CellContext`showCode$$ = False]}], 
              TabView[{"base 10" -> Column[{
                   Grid[{{
                    Text["Memory"], 
                    Text["PC"], 
                    Text["Status"], 
                    Text["CIR"]}, {
                    TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys$$, $CellContext`i$$], 
                    Normal[
                    Part[$CellContext`memoryRules$$, $CellContext`i$$]]]], 
                    Part[$CellContext`pcList$$, $CellContext`i$$], 
                    Appearance -> {"Limited", 10}], 
                    Text[
                    Part[$CellContext`pcList$$, $CellContext`i$$]], 
                    Text[
                    Part[$CellContext`statusList$$, $CellContext`i$$]], 
                    Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories$$, $CellContext`i$$], 
                    
                    Part[$CellContext`pcList$$, $CellContext`i$$]], \
$CellContext`format$$]]}}], 
                   Grid[{{
                    Text["Registers"]}, {
                    Text[
                    
                    Grid[{{$CellContext`R0, $CellContext`R1, $CellContext`R2, \
$CellContext`R3, $CellContext`R4, $CellContext`R5, $CellContext`R6, \
$CellContext`R7, $CellContext`R8, $CellContext`R9, $CellContext`R10, \
$CellContext`R11, $CellContext`R12}, 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    
                    Part[$CellContext`registerList$$, \
$CellContext`i$$]]}]]}}]}], "base 2" -> Column[{
                   Grid[{{
                    Text["Memory"], 
                    Text["PC"], 
                    Text["Status"], 
                    Text["CIR"]}, {
                    TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys$$, $CellContext`i$$], 
                    Normal[
                    Part[$CellContext`memoryRules$$, $CellContext`i$$]]]], 
                    Part[$CellContext`pcList$$, $CellContext`i$$], 
                    Appearance -> {"Limited", 10}], 
                    Text[
                    Part[$CellContext`pcList$$, $CellContext`i$$]], 
                    Text[
                    Part[$CellContext`statusList$$, $CellContext`i$$]], 
                    Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories$$, $CellContext`i$$], 
                    
                    Part[$CellContext`pcList$$, $CellContext`i$$]], \
$CellContext`format$$]]}}], 
                   Grid[{{
                    Text["Registers"]}, {
                    Text[
                    Grid[
                    
                    Transpose[{{$CellContext`R0, $CellContext`R1, \
$CellContext`R2, $CellContext`R3, $CellContext`R4, $CellContext`R5, \
$CellContext`R6, $CellContext`R7, $CellContext`R8, $CellContext`R9, \
$CellContext`R10, $CellContext`R11, $CellContext`R12}, 
                    Map[$CellContext`bf, 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList$$, $CellContext`i$$]]], 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList$$, $CellContext`i$$]]}], 
                    Alignment -> Right]]}}]}]}, 
               Dynamic[$CellContext`tab$$]]}], 
           "Specifications" :> {{{$CellContext`offset$$, 50, "memory start"}, 
              0, 512, 1}, {{$CellContext`lim$$, 100, "computation limit"}, 1, 
              10000, 10}, {{$CellContext`i$$, 1, "computation step"}, 1, 
              Dynamic[$CellContext`len$$], 1, Appearance -> 
              "Open"}, {{$CellContext`format$$, "signed", "format"}, {
              "signed", "2's complement"}}}, "Options" :> {}, 
           "DefaultOptions" :> {}],
          ImageSizeCache->{779., {426., 435.}},
          SingleEvaluation->True],
         Deinitialization:>None,
         DynamicModuleValues:>{},
         Initialization:>(($CellContext`code$$ = 
            "STR R11 , 0010 ; this is a comment and is terminated by: ;\nl2 : \
LDR R1 , 0012 ; this is another comment: note spaces! ;\nl2a : MOV R7 , # 10 \
; prep the 10th memory address in R7 ;\nl2b : LDR R11 , R7 ; read its \
contents into R11 ;\n12c : STR R8 , R7 ; store R8 into the location ;\nl1 : \
SUB R10 , R8 , R3\nl3 : MOV R7 , # -0019 ; not every line needs a comment ;\n\
l3a : MVN R8 , # 4096 ; may create a negative number! ;\nl4 : MOV R5 , R2 ; ;\
\nl5 : CMP R2 , R3 ; empty comments (see above) must also have spaces ;\nl6 : \
BEQ l10\nl7 : ADD R12 , R10 , # 00066\nMOV R3 , R2\nHALT"; \
$CellContext`codeRun$$ = $CellContext`asLoadRunDecoded[$CellContext`code$$, 
              60, $CellContext`lim$$, 16]; $CellContext`len$$ = 
            Length[$CellContext`codeRun$$]; {$CellContext`memories$$, \
$CellContext`pcList$$, $CellContext`statusList$$, \
$CellContext`registerList$$} = 
            Transpose[$CellContext`codeRun$$]; $CellContext`memoryRules$$ = 
            Map[Drop[
               
               ArrayRules[#], -1]& , $CellContext`memories$$]; \
$CellContext`memoryRules$$ = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                 
                 Part[#, 
                  2], $CellContext`format$$]& , #]& , \
$CellContext`memoryRules$$]; $CellContext`memoryRules$$ = 
            Map[SortBy[#, 
               Part[#, 
                1]& ]& , $CellContext`memoryRules$$]; $CellContext`keys$$ = 
            Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules$$]; Null); 
          Typeset`initDone$$ = True),
         SynchronousInitialization->True,
         UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
         UnsavedVariables:>{Typeset`initDone$$},
         UntrackedVariables:>{Typeset`size$$}], "Manipulate",
        Deployed->True,
        StripOnInput->False],
       Manipulate`InterpretManipulate[1]]}
    },
    DefaultBaseStyle->"Column",
    GridBoxAlignment->{"Columns" -> {{Left}}},
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
   "Column"],
  DynamicModuleValues:>{}]], "Output",ExpressionUUID->"9182e09a-e0a4-4a21-\
b31d-09ea5301aa55"]
}, Open  ]]
}, Closed]]
}, Closed]]
}, Open  ]],

Cell[CellGroupData[{

Cell["2-s complement arithmetic", "Section",ExpressionUUID->"c807eb4c-828b-4fcc-a228-e5f490e7c292"],

Cell[CellGroupData[{

Cell["Notes", "Subsection",ExpressionUUID->"93683673-b0ac-4811-84e8-a25ef8df27ed"],

Cell["\<\
We want to mimic typical chip design constraints on wordsize and the \
arithmetic issues of over (or under-) flow that arise from this.  Mathematica \
has a large word size (in fact, unlimited!) but we want to explore the issues \
of word size using a much smaller value to make these issues clearer.\
\>", "Text",ExpressionUUID->"3008e7a8-b1cf-46b3-9a04-ab6f6f76dc9a"],

Cell[CellGroupData[{

Cell["Limitations for Mathematica users", "Subsubsection",ExpressionUUID->"2afceeff-64d1-4e26-90f3-bc0d0d226bd6"],

Cell[TextData[{
 "We cannot use ",
 StyleBox["BaseForm", "Input"],
 " output as input"
}], "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ToExpression", "[", 
  SubscriptBox["\"\<1111111111\>\"", "\"\<2\>\""], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"ToExpression", "::", "notstrbox"}], "MessageName"], ":", 
  " ", "\<\"\[NoBreak]\\!\\(\\*SubscriptBox[\\\"\\\\\\\"1111111111\\\\\\\"\\\"\
, \\\"\\\\\\\"2\\\\\\\"\\\"]\\)\[NoBreak] is not a string or a box. \
ToExpression can only interpret strings or boxes as Wolfram Language input. \
\\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", \
ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/ToExpression/notstrbox\\\", ButtonNote -> \
\\\"ToExpression::notstrbox\\\"]\\)\"\>"}]], "Message", "MSG"],

Cell[BoxData["$Failed"], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ToString", "[", 
   RowBox[{"BaseForm", "[", 
    RowBox[{
     RowBox[{
      SuperscriptBox["2", "10"], "-", "1"}], ",", "2"}], "]"}], "]"}], "//", 
  "FullForm"}]], "Input"],

Cell[BoxData[
 TagBox[
  StyleBox["\"\<1111111111\\n          2\>\"",
   ShowSpecialCharacters->False,
   ShowStringCharacters->True,
   NumberMarks->True],
  FullForm]], "Output"]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Open  ]]
},
AutoGeneratedPackage->Automatic,
WindowSize->{1424, 842},
WindowMargins->{{229, Automatic}, {Automatic, 156}},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
TrackCellChangeTimes->False,
Magnification:>1.5 Inherited,
FrontEndVersion->"11.2 for Mac OS X x86 (32-bit, 64-bit Kernel) (September \
26, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 101, 0, 146, "Title",ExpressionUUID->"201660c2-92e8-4e15-8dcb-9b99754ea4dc"],
Cell[CellGroupData[{
Cell[1612, 39, 114, 0, 103, "Chapter",ExpressionUUID->"03a0a00b-b0e6-4866-954c-fcee234b8af7"],
Cell[1729, 41, 160, 3, 53, "Text",ExpressionUUID->"037f078b-a1b4-4ca9-8a50-c87f42294165"],
Cell[CellGroupData[{
Cell[1914, 48, 95, 0, 67, "Subsubsection",ExpressionUUID->"9ce0362a-8db3-453f-93d9-9e426f612ef8"],
Cell[CellGroupData[{
Cell[2034, 52, 149, 3, 97, "Code",ExpressionUUID->"f6f41542-74bb-4f5e-831e-91d06bdd8aa9",
 InitializationCell->True],
Cell[2186, 57, 130, 0, 70, "Output",ExpressionUUID->"4d80afa2-affa-4858-8bac-80e533b0acf1"]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[2365, 63, 90, 0, 53, "Subsubsection",ExpressionUUID->"07fa5cb8-3dae-4efe-a154-3f926c175961"],
Cell[CellGroupData[{
Cell[2480, 67, 118, 1, 78, "Code",ExpressionUUID->"0b8e8ab5-a533-4c7c-a476-3fc45cd4585e"],
Cell[2601, 70, 159, 2, 52, "Output",ExpressionUUID->"5fbd6e65-8cae-4c1b-8b6b-62243820b369"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[2809, 78, 103, 0, 67, "Subsubsection",ExpressionUUID->"008e5049-00ea-4b3a-b738-4ebfbce95d68"],
Cell[2915, 80, 45, 0, 57, "Text",ExpressionUUID->"8bf951ad-f6a0-4c74-ba5c-a4465c385420"],
Cell[2963, 82, 418, 11, 157, "Code",ExpressionUUID->"ce5a2722-8515-42b7-b4f1-d6859bb221df",
 InitializationCell->True],
Cell[3384, 95, 143, 3, 114, "Code",ExpressionUUID->"b971c2a9-9e74-43e6-8c03-8ef3c816f375"],
Cell[3530, 100, 141, 3, 114, "Code",ExpressionUUID->"ec9bfa1c-9544-4242-8c03-36d68617ddc4"]
}, Closed]],
Cell[CellGroupData[{
Cell[3708, 108, 129, 0, 53, "Subsubsection",ExpressionUUID->"d725a7b2-4bff-4efd-a197-f13156733a60"],
Cell[3840, 110, 213, 3, 53, "Text",ExpressionUUID->"6026ea4f-b730-4a95-98e8-ceacfeb007db"],
Cell[CellGroupData[{
Cell[4078, 117, 293, 7, 67, "Input",ExpressionUUID->"1a4d0eef-e65f-4ae1-b058-dd24474facd6"],
Cell[4374, 126, 300, 8, 64, "Output",ExpressionUUID->"cf007ccb-27b7-4dc6-8894-adda7a186e84"]
}, {2}]],
Cell[4686, 137, 189, 5, 53, "Text",ExpressionUUID->"b43b87ab-a5f9-4069-8746-ea0cffad3ed0"],
Cell[4878, 144, 142, 2, 53, "Text",ExpressionUUID->"f15b3aaa-8dd7-4c16-aa34-8f66b2389877"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5057, 151, 86, 0, 101, "Section",ExpressionUUID->"5c2a3222-c7eb-4037-a159-d58f45ca9fe2"],
Cell[5146, 153, 195, 3, 53, "Text",ExpressionUUID->"f9425d64-983c-40a8-b637-930e91a22d96"],
Cell[CellGroupData[{
Cell[5366, 160, 142, 1, 67, "Subsubsection",ExpressionUUID->"58b67e43-0ea4-44aa-a940-88514b524587"],
Cell[5511, 163, 116, 0, 53, "Text",ExpressionUUID->"9f667fe6-1d27-4937-a710-6dca6e566117"],
Cell[5630, 165, 371, 5, 88, "Text",ExpressionUUID->"e6585e7c-a59a-45f1-8aaf-5ceee4645c39"],
Cell[6004, 172, 120, 0, 53, "Text",ExpressionUUID->"92414e67-4d74-47ac-b487-0c25c17573bb"],
Cell[6127, 174, 468, 7, 122, "Text",ExpressionUUID->"40620cb3-f24f-4e2f-b24b-20393e6321fe"]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[6656, 188, 80, 0, 103, "Chapter",ExpressionUUID->"f06d27b6-f0f5-45b4-9236-f76f3670f688"],
Cell[CellGroupData[{
Cell[6761, 192, 52, 1, 124, "Section",ExpressionUUID->"e53c15eb-f9d3-495e-9c0f-0bda330d63c3",
 InitializationGroup->True],
Cell[CellGroupData[{
Cell[6838, 197, 38, 0, 70, "Subsubsection",ExpressionUUID->"3135016c-31e8-4630-a8cb-e4a0bccde484"],
Cell[CellGroupData[{
Cell[6901, 201, 220, 4, 141, "Code",ExpressionUUID->"b3e9acc0-65e2-4ba2-bf22-498801a7a3c4"],
Cell[7124, 207, 104, 0, 70, "Output",ExpressionUUID->"1ad0687e-c460-44e2-abbd-61b982ddcf4b"],
Cell[7231, 209, 130, 0, 70, "Output",ExpressionUUID->"3e70ad2d-2d97-4905-823d-c340c2cc820d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7398, 214, 160, 3, 114, "Code",ExpressionUUID->"50594c05-d359-456a-a3d2-24918dfe60e7"],
Cell[7561, 219, 129, 0, 70, "Output",ExpressionUUID->"ec663dca-b721-4404-847a-c28691eb5870"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7727, 224, 139, 2, 114, "Code",ExpressionUUID->"8832f2b7-a9bd-49e1-8603-945deca17feb"],
Cell[7869, 228, 275, 7, 70, "Message",ExpressionUUID->"2a29adf8-d038-4b54-895f-a1e64b7e75f4"],
Cell[8147, 237, 312, 7, 70, "Message",ExpressionUUID->"d9f4f6dd-c73f-4262-9a08-2c47334a10b5"],
Cell[8462, 246, 89, 0, 70, "Output",ExpressionUUID->"a05af518-9e09-42b7-b0ea-f0ff2c710352"]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[8600, 252, 89, 0, 70, "Subsubsection",ExpressionUUID->"a6798124-2b60-4d44-9bae-76e6227e7053"],
Cell[8692, 254, 306, 9, 87, "Input",ExpressionUUID->"6c8d5fdf-3982-4751-ab7a-eedd70c1ec83",
 InitializationCell->True],
Cell[9001, 265, 342, 9, 103, "Input",ExpressionUUID->"1bb7769d-03af-48b8-8cd8-edabdd4c4ca0",
 InitializationCell->True],
Cell[9346, 276, 1459, 37, 366, "Input",ExpressionUUID->"afcbc36c-2dae-42b4-ba05-64be0f9c928e",
 InitializationCell->True]
}, Closed]]
}, Closed]],
Cell[CellGroupData[{
Cell[10854, 319, 39, 0, 124, "Section",ExpressionUUID->"4db4eff0-e703-49e2-87dd-6e7c8668c199"],
Cell[CellGroupData[{
Cell[10918, 323, 58, 0, 87, "Subsection",ExpressionUUID->"86e0ecf2-7481-48a1-b2bf-653a61f2c1c3"],
Cell[CellGroupData[{
Cell[11001, 327, 53, 0, 87, "Subsubsection",ExpressionUUID->"b2fd5f59-e063-4514-a9a4-b963d3c69591"],
Cell[11057, 329, 733, 13, 238, "Text",ExpressionUUID->"cd1137a9-3905-4ab3-8a9b-a2b1e7fbab54"],
Cell[11793, 344, 152, 3, 67, "Text",ExpressionUUID->"acb5f896-0049-45f2-b788-db9bd306dcb6"],
Cell[11948, 349, 1121, 18, 1062, "Input",ExpressionUUID->"f880d6e2-deeb-430b-a30e-f94e22d1b97b",
 InitializationCell->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[13106, 372, 70, 0, 87, "Subsubsection",ExpressionUUID->"b714955e-7b37-4948-b228-cd74a6f05bca"],
Cell[13179, 374, 1393, 22, 1105, "Input",ExpressionUUID->"f52ee5a9-14d6-4ee7-aa46-c7111ce13280",
 InitializationCell->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[14609, 401, 111, 2, 87, "Subsubsection",ExpressionUUID->"0f3bb856-396a-4324-8446-ec08ebb192b6"],
Cell[CellGroupData[{
Cell[14745, 407, 419, 9, 175, "Input",ExpressionUUID->"611f070b-8ec0-4712-96de-4312f26442e4",
 InitializationCell->True],
Cell[15167, 418, 83, 0, 70, "Output",ExpressionUUID->"eb437226-940e-4b64-beb1-0959e29c98b9"]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[15323, 426, 99, 0, 124, "Section",ExpressionUUID->"5b21c775-e4e8-4cb1-bde0-711ddef16d8d"],
Cell[CellGroupData[{
Cell[15447, 430, 45, 0, 87, "Subsection",ExpressionUUID->"c20be201-e42a-4713-bc0a-6d85c3b848a4"],
Cell[15495, 432, 5277, 153, 1065, "Input",ExpressionUUID->"855effa7-79a1-42df-9d0a-3e61a5aa88dc",
 InitializationCell->True],
Cell[CellGroupData[{
Cell[20797, 589, 312, 6, 243, "Input",ExpressionUUID->"d85d4260-02ea-4cf1-b636-dbc50e73ef07"],
Cell[21112, 597, 154, 6, 67, "Output",ExpressionUUID->"a49d6927-305b-4689-a79b-55f1d8ed7720"],
Cell[21269, 605, 30, 0, 67, "Output",ExpressionUUID->"a9dc621f-f710-4047-9f9b-0e87e1ceddd9"],
Cell[21302, 607, 155, 6, 67, "Output",ExpressionUUID->"314175e2-d531-49fc-9141-7e992f3a7b15"],
Cell[21460, 615, 30, 0, 67, "Output",ExpressionUUID->"811e9c57-f156-4ea2-8f76-4683ecfff2b6"],
Cell[21493, 617, 154, 6, 67, "Output",ExpressionUUID->"4e7b0094-7586-4db0-8883-03d146108cc6"]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[21696, 629, 58, 0, 87, "Subsection",ExpressionUUID->"7ca927fc-f613-4904-87ea-88ffe7ddf9e6"],
Cell[21757, 631, 3445, 97, 567, "Input",ExpressionUUID->"0fb47de0-6a58-489e-a9af-75aad127ffef",
 InitializationCell->True],
Cell[25205, 730, 38448, 1064, 6676, "Input",ExpressionUUID->"0c54ee79-400b-455d-a0b3-eddc0a85c01a",
 InitializationCell->True]
}, Closed]],
Cell[CellGroupData[{
Cell[63690, 1799, 78, 0, 87, "Subsection",ExpressionUUID->"695b8ecc-9303-4ea2-b949-ff56b48c781d"],
Cell[63771, 1801, 17150, 381, 2568, "Input",ExpressionUUID->"59444a36-8be1-47fa-93b9-74f7546e6e1a",
 InitializationCell->True]
}, Closed]]
}, Closed]]
}, Closed]],
Cell[CellGroupData[{
Cell[80982, 2189, 81, 0, 84, "Chapter",ExpressionUUID->"7b8347bf-ba03-4cba-98e9-fda7bd0d6fbf"],
Cell[CellGroupData[{
Cell[81088, 2193, 98, 0, 101, "Section",ExpressionUUID->"bde34c08-97df-454b-b27d-fc11fa523c81"],
Cell[CellGroupData[{
Cell[81211, 2197, 107, 0, 81, "Subsection",ExpressionUUID->"e463df51-951f-42dc-8e4d-f112e55e2c59"],
Cell[81321, 2199, 128, 0, 53, "Text",ExpressionUUID->"6967238a-fe68-4e59-8035-2477d466a4ad"],
Cell[CellGroupData[{
Cell[81474, 2203, 3578, 79, 939, "Input",ExpressionUUID->"9fa01849-53c9-41d7-a5d9-6cb92ca6519d"],
Cell[85055, 2284, 62781, 1448, 1133, "Output",ExpressionUUID->"6051a861-d179-4af8-8086-456b99913c61"]
}, {2}]]
}, Open  ]],
Cell[CellGroupData[{
Cell[147882, 3738, 99, 0, 81, "Subsection",ExpressionUUID->"8a4951d7-6178-4a67-ad29-a0e36ddfb847"],
Cell[CellGroupData[{
Cell[148006, 3742, 89, 0, 67, "Subsubsection",ExpressionUUID->"f82d79d7-233c-4d6e-9dd1-d35b62fd6548"],
Cell[148098, 3744, 606, 11, 474, "Input",ExpressionUUID->"bec52cec-e00c-49ef-9c63-8c2580ccd921",
 InitializationCell->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[148741, 3760, 106, 0, 67, "Subsubsection",ExpressionUUID->"8895dc0b-711b-47c2-bebe-a4af259a3fda"],
Cell[148850, 3762, 349, 7, 142, "Text",ExpressionUUID->"9c52a17f-960d-4cd2-ac3e-bba64dbebc87"],
Cell[CellGroupData[{
Cell[149224, 3773, 598, 13, 241, "Code",ExpressionUUID->"211f22e9-89e7-4236-ae53-353f53ba6e8c"],
Cell[149825, 3788, 85647, 2077, 70, "Output",ExpressionUUID->"635a7837-a719-4551-b565-2590564c0996"]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[235521, 5871, 89, 0, 53, "Subsubsection",ExpressionUUID->"3badbe20-9023-4f33-a521-6f57d0f2b45e"],
Cell[235613, 5873, 294, 7, 393, "Input",ExpressionUUID->"457f8781-0ac0-4846-8e83-44350f3c3b6a",
 InitializationCell->True]
}, Closed]],
Cell[CellGroupData[{
Cell[235944, 5885, 116, 0, 53, "Subsubsection",ExpressionUUID->"59d4d8cc-9f5c-461c-91a4-99c43dff0ce2"],
Cell[236063, 5887, 378, 11, 442, "Text",ExpressionUUID->"95824cc5-38f2-4887-953e-5fe08a9b6ec5"]
}, Closed]],
Cell[CellGroupData[{
Cell[236478, 5903, 110, 0, 53, "Subsubsection",ExpressionUUID->"3dddb27f-d3cf-44c5-b688-b42b1c521de1"],
Cell[236591, 5905, 277, 7, 349, "Input",ExpressionUUID->"851ec032-9490-4ab8-b13d-12f65875ddf2",
 InitializationCell->True],
Cell[236871, 5914, 69, 1, 67, "Input",ExpressionUUID->"392fbe5b-07d2-4359-9a9b-7a8a54071f01"],
Cell[236943, 5917, 650, 14, 196, "Text",ExpressionUUID->"616ab3cf-4742-4136-8a18-11a1837e2f18"]
}, Closed]],
Cell[CellGroupData[{
Cell[237630, 5936, 116, 0, 53, "Subsubsection",ExpressionUUID->"a1d031f5-ec41-495d-b95a-25ae7f2f5173"],
Cell[237749, 5938, 155, 3, 57, "Text",ExpressionUUID->"443710c8-afb7-4a0b-be45-ab66872112df"],
Cell[237907, 5943, 760, 13, 670, "Input",ExpressionUUID->"1423806e-ed1c-4527-8531-5f2a7dedbaf5",
 InitializationCell->True],
Cell[CellGroupData[{
Cell[238692, 5960, 195, 4, 103, "Input",ExpressionUUID->"53ba27d2-5fc6-4f03-94fb-10ea35627c4d",
 InitializationCell->True],
Cell[238890, 5966, 80782, 1951, 70, "Output",ExpressionUUID->"9182e09a-e0a4-4a21-b31d-09ea5301aa55"]
}, Open  ]]
}, Closed]]
}, Closed]]
}, Open  ]],
Cell[CellGroupData[{
Cell[319745, 7925, 99, 0, 101, "Section",ExpressionUUID->"c807eb4c-828b-4fcc-a228-e5f490e7c292"],
Cell[CellGroupData[{
Cell[319869, 7929, 82, 0, 81, "Subsection",ExpressionUUID->"93683673-b0ac-4811-84e8-a25ef8df27ed"],
Cell[319954, 7931, 379, 5, 122, "Text",ExpressionUUID->"3008e7a8-b1cf-46b3-9a04-ab6f6f76dc9a"],
Cell[CellGroupData[{
Cell[320358, 7940, 113, 0, 67, "Subsubsection",ExpressionUUID->"2afceeff-64d1-4e26-90f3-bc0d0d226bd6"],
Cell[320474, 7942, 98, 4, 57, "Text",ExpressionUUID->"7e2e8e68-0f51-4038-8696-a17237e31e68"],
Cell[CellGroupData[{
Cell[320597, 7950, 112, 2, 67, "Input",ExpressionUUID->"24402ed3-abb8-46d0-8a45-4ad228cd7398"],
Cell[320712, 7954, 570, 10, 52, "Message",ExpressionUUID->"e52ba22d-fc2a-4762-b2ac-e2f497efe298"],
Cell[321285, 7966, 34, 0, 67, "Output",ExpressionUUID->"77e7d487-e0e0-40df-9c23-272eda4b1b36"]
}, Open  ]],
Cell[CellGroupData[{
Cell[321356, 7971, 211, 7, 73, "Input",ExpressionUUID->"18773cb7-94de-4c16-bd37-36b563f8d71b"],
Cell[321570, 7980, 180, 6, 67, "Output",ExpressionUUID->"cc06d70b-03f1-4e8c-861a-cf2d3d5f9cd8"]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* NotebookSignature vVOjEZ12Mqxa3UzBaCip9QcI *)
