STR R11 , 0010 ; this is a comment and is terminated by: ;
l2 : LDR R1 , 0012 ; this is another comment: note spaces! ;
l1 : SUB R10 , R8 , R3
l3 : MOV R7 , # -0019 ; not every line needs a comment ;
l3a : MVN R8 , # 4096 ; may create a negative number! ;
l4 : MOV R5 , R2 ; ;
l5 : CMP R2 , R3 ; empty comments (see above) must also have spaces ;
l6 : BEQ l11
l7 : ADD R12 , R10 , # 00066
MOV R3 , R2

l10 : B l2
l11 : HALT
