# AQA Assembly language README

This project contains a computable document which implements an assembler for the assembly language specified for the AQA AS and A level qualifications in Computer Science.  Instructions are also included in a separate file.

In the computable document, an interface is provided to load and run your assembly language programs.  A couple of these are distributed with the first release (aka lovelace) and it is expected that more will be added as time goes on.  The computable document itself can only be viewed using Wolfram's [CDF player](https://www.wolfram.com/cdf-player/) or Mathematica and can only be edited using Mathematica.

The instructions are provided in three formats: pdf, for viewing without the CDF player; cdf, for viewing within the player or Mathematica; nb (notebook) format, for viewing, executing and/or editing with a full version of Mathematica.
