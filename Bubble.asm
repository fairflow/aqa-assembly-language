MOV R0 , # 58 ; data initialisation ;
STR R0 , 1
MOV R0 , # 1
STR R0 , 2
MOV R0 , # 199 ; change data so we need to bubble more than once ;
STR R0 , 3
MOV R0 , # 142
STR R0 , 4
MOV R0 , # 130
STR R0 , 5   ; data now in place ;

MOV R0 , # 0 ; clear temp register R0 ;
MOV R3 , # 0 ; clear register R3 ;
MOV R4 , # 0 ; clear register R4 ;
MOV R5 , # 0 ; clear register R5 ;
MOV R6 , # 0 ; clear temp register R6 ;
MOV R7 , # 0 ; clear 'swapped' register R7 ;

MOV R1 , # 5 ; 5 elements in the array ;
MOV R2 , # 1 ; 1st element is in location 1 ;
MOV R8 , R2  ; now we need to save the initial memory location ;

start : CMP R1 , # 2
BLT cleanUp ; finished if there are fewer than 2 locations to handle ;

loopOuter : MOV R2 , R8 ; start at beginning of loop ;
MOV R7 , # 0 ; this is a repeat loop so clear the swapped flag ;

loopInner : LDR R3 , R2 ; 1st memory value of 2 ;
ADD R2 , R2 , # 1
LDR R4 , R2 ; 2nd memory value ;
CMP R3 , R4
BGT swap ; we will sort in ascending order ;
cond : CMP R1 , R2
BEQ testOuter ; inner loop is finished ;
B loopInner   ; go around the loop1 again ;

swap : MOV R0 , R0 ; label: swap memory locations ;
MOV R7 , # 1 ; we have a swap so update value of R7 to reflect this ;
MOV R6 , R2 ; save current value of R2 ;
STR R3 , R2 ; R2 holds pointer to the 2nd value which should become the 1st ;
SUB R2 , R2 , # 1 ; back up memory pointer ;
STR R4 , R2 ; 1st value now becomes the 2nd ;
MOV R2 , R6 ; restore previous value of R2 ;
B cond ; resume inner loop ;

testOuter : CMP R7 , # 1 ; have elements been swapped? ;
BLT cleanUp ; No: outer loop is finished ;
B loopOuter ; Yes: go around the outer loop again ;

cleanUp : MOV R0 , # 0 ; clean up &c ;

HALT