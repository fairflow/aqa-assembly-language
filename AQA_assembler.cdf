(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 11.0' *)

(*************************************************************************)
(*                                                                       *)
(*                                                                       *)
(*  This file was created under the Wolfram Enterprise licensing terms.  *)
(*                                                                       *)
(*       For additional information concerning CDF licensing see:        *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*                                                                       *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[    311214,       7954]
NotebookOptionsPosition[    305626,       7739]
NotebookOutlinePosition[    306106,       7759]
CellTagsIndexPosition[    306063,       7756]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Assembly language compilation", "Title"],

Cell[CellGroupData[{

Cell["AS Computer Science, South Devon College", "Chapter"],

Cell["\<\
Copyright \[Copyright] Matthew Fairtlough & South Devon College, November, \
2017\
\>", "Text"],

Cell[CellGroupData[{

Cell["This directory:", "Subsubsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NotebookDirectory", "[", "]"}]], "Code",
 InitializationCell->True],

Cell[BoxData["\<\"S:\\\\A Level Academy\\\\Computer Science\\\\CAS\\\\CAS \
Software\\\\\"\>"], "Output"]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["This file:", "Subsubsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NotebookFileName", "[", "]"}]], "Code"],

Cell[BoxData["\<\"S:\\\\A Level Academy\\\\Computer Science\\\\CAS\\\\CAS \
Software\\\\AssemblyASCS_for_CAS.nb\"\>"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Version and word length", "Subsubsection"],

Cell["Version 2.21w1 (word version)", "Text"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"$wordLength", "=", "16"}], ";"}], " "}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "change", " ", "me", " ", "before", " ", "activating", " ", "assembler", 
    " ", "to", " ", "alter", " ", "the", " ", "word", " ", "length"}], " ", 
   "*)"}]}]}], "Code",
 InitializationCell->True],

Cell[BoxData[
 RowBox[{
  RowBox[{"$version", "=", "\"\<2.21w1\>\""}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{"$assemblyVersion", "=", "2.21"}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell["Activate assembler (for full Mathematica version)", "Subsubsection"],

Cell["\<\
Click the button below to evaluate initialization cells to add the code for \
assembly language execution to this and other notebooks:\
\>", "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Button", "[", 
  RowBox[{"\"\<Activate assembler\>\"", ",", 
   RowBox[{"FrontEndExecute", "[", 
    RowBox[{"FrontEndToken", "[", 
     RowBox[{
      RowBox[{"InputNotebook", "[", "]"}], ",", 
      "\"\<EvaluateInitialization\>\""}], "]"}], "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 ButtonBox["\<\"Activate assembler\"\>",
  Appearance->Automatic,
  ButtonFunction:>FrontEndExecute[
    FrontEndToken[
     InputNotebook[], "EvaluateInitialization"]],
  Evaluator->Automatic,
  Method->"Preemptive"]], "Output"]
}, {2}]],

Cell[TextData[{
 "Note that this should be done ",
 StyleBox["before",
  FontSlant->"Italic"],
 " loading any code below."
}], "Text"],

Cell["\<\
Now continue to explore the assembler or return to your code...\
\>", "Text"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Introduction", "Section"],

Cell["\<\
This notebook implements an assembler for the assembly language specified for \
the AQA A level in Computer Science.\
\>", "Text"],

Cell[CellGroupData[{

Cell["Limitations and variations from the published specifications", \
"Subsubsection"],

Cell["You can now include blank lines in your code.", "Text"],

Cell["\<\
Comments in code are now supported; they must begin and end with \
\[OpenCurlyQuote];\[CloseCurlyQuote].  The colon after an instruction label \
must have at least one space before and after.  I recommend exactly one space \
in each case for maximum compatibility with the published syntax.  \
\>", "Text"],

Cell["See the code examples below for what is required.", "Text"],

Cell["\<\
Support for binary or hexadecimal notation for numerical literals is not yet \
provided; all values are in decimal. This accords with the format of exam \
questions, by the way.  Bit shifts and logical operations work by first \
translating to binary, then carrying out the operation and finally \
translating back to decimal.  Arithmetic overflow may result in negative \
decimal numbers.\
\>", "Text"]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Coding", "Chapter"],

Cell[CellGroupData[{

Cell["Setup", "Section",
 InitializationGroup->True],

Cell[CellGroupData[{

Cell["Load and save", "Subsubsection"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"originalDirectory", "=", 
  RowBox[{"Directory", "[", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"NotebookDirectory", "[", "]"}]}], "Code"],

Cell[BoxData["\<\"N:\\\\\"\>"], "Output"],

Cell[BoxData["\<\"S:\\\\A Level Academy\\\\Computer Science\\\\CAS\\\\CAS \
Software\\\\\"\>"], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SetDirectory", "[", 
  RowBox[{"NotebookDirectory", "[", "]"}], "]"}]], "Code"],

Cell[BoxData["\<\"S:\\\\A Level Academy\\\\Computer Science\\\\CAS\\\\CAS \
Software\"\>"], "Output"]
}, Open  ]],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<FunctionalParsers`\>\"", "]"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell["Utilities", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"mkConst", "[", "f_", "]"}], ":=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"ClearAll", "[", "f", "]"}], ";", 
    RowBox[{
     RowBox[{"f", "[", "]"}], ":=", "f"}]}], ")"}]}]], "Input",
 InitializationCell->True],

Cell[BoxData[
 RowBox[{
  RowBox[{"mkConst", "/@", 
   RowBox[{"{", 
    RowBox[{
    "asmBcond", ",", "asmMemRef", ",", "asmInstr", ",", "asmReg", ",", 
     "asmLabel", ",", "asmOperand", ",", "asmLiteral", ",", "asmLine"}], 
    "}"}]}], ";"}]], "Input",
 InitializationCell->True],

Cell[BoxData[{
 RowBox[{
  RowBox[{"Clear", "[", 
   RowBox[{"asmBcond", ",", "asmRegProcess", ",", "asmLiteralProcess"}], 
   "]"}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "it", " ", "is", " ", "possible", " ", "just", " ", "to", " ", "set", " ", 
    "up", " ", "the", " ", "computations", " ", "that", " ", "the", " ", 
    "functions", " ", "do", " ", "in", " ", "advance", " ", 
    RowBox[{"e", ".", "g", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"asmBcond", "[", "\"\<BEQ\>\"", "]"}], ":=", "asmBranchOnEq"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"asmBcond", "[", "p_", "]"}], ":=", "p"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"asmRegProcess", "=", 
   RowBox[{
    RowBox[{"asmReg", "[", 
     RowBox[{"ToExpression", "[", 
      RowBox[{"StringDrop", "[", 
       RowBox[{"#", ",", "1"}], "]"}], "]"}], "]"}], "&"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"asmLiteralProcess", "[", "n_", "]"}], ":=", 
  RowBox[{"asmLiteral", "[", 
   RowBox[{"to2sComplement", "[", 
    RowBox[{"n", ",", "$wordLength"}], "]"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "this", " ", "is", " ", "not", " ", "perfect", " ", "but", " ", 
    "represents", " ", "a", " ", "compromise", " ", "solution"}], " ", 
   "*)"}]}]}], "Input",
 InitializationCell->True]
}, Closed]]
}, Closed]],

Cell[CellGroupData[{

Cell["The parser generator", "Section"],

Cell[CellGroupData[{

Cell["The grammar of the assembly language", "Subsection"],

Cell[CellGroupData[{

Cell["Grammar for recognition only", "Subsubsection"],

Cell[TextData[{
 "This grammar is in EBNF (Extended Backus-Naur Form), which is a common \
format for specifying and implementing grammars.  BNF is not an AS-level \
topic but is useful background information and will be examined in the A \
level (but not ",
 StyleBox["Extended",
  FontSlant->"Italic"],
 " BNF).  It formally specifies the grammar of the assembly language \
implemented in this notebook.  The grammar below does not specify a result \
for the parser; it just parses it, essentially returning a yes/no answer, \
where \[OpenCurlyQuote]yes\[CloseCurlyQuote] means: the program to be parsed \
is syntactically valid and \[OpenCurlyQuote]no\[CloseCurlyQuote] means: the \
program to be parsed is not valid."
}], "Text"],

Cell["\<\
Further development could allow you to check your assembly language program \
for syntactic correctness before trying to run it.\
\>", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "assemblyGrammar", "=", 
   "\"\<\n<assembly> = <line> | (<line> , <assembly>) ;\n<line> = \
[<labelling>] , <instruction> , [(';' , [<comments>], ';')] ;\n<comments> = \
<comment> | (<comment> , <comments>) ;\n<comment> = '_WordString' ;\n\
<labelling> = <label> , ':' ;\n<label> = '_WordString' ;\n<instruction> = \n\
(<opcode1> , (<reg> , ',') , <mem-ref>) | \n(<opcode2> , (<reg> , ',') , \
((<reg> , ',') , <operand>)) | \n(<opcode3> , (<reg> , ',') , <operand>) | \n\
('CMP' , (<reg> , ',') , <operand>) | \n(<bcond> , <label>) | 'HALT' ;\n\
<opcode1> = 'LDR' | 'STR' ;\n<opcode2> = 'ADD' | 'SUB' | 'AND' | 'ORR' | \
'EOR' | 'LSL' | 'LSR' ;\n<opcode3> = 'MOV' | 'CMP' | 'MVN' ;\n<bcond> = 'B' | \
'BEQ' | 'BNE' | 'BGT' | 'BLT' ;\n<reg> = ('R0' | 'R1' | 'R2' | 'R3' | 'R4' | \
'R5' | 'R6' | 'R7' | 'R8' | 'R9' | 'R10' | 'R11' | 'R12') ;\n<mem-ref> = \
'Range[0,65535]' ;\n<literal> = ('#' , 'Range[-4294967296,4294967295]') ;\n\
<operand> = (<literal> | <reg>) ;\n\>\""}], " ", ";"}]], "Input",
 InitializationCell->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["Grammar to compute elements of the parse tree", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "operationalAssemblyGrammar", "=", 
   "\"\<\n<assembly> = <line> | (<line> , <assembly>) ;\n<line> = \
([<labelling>] , [<instruction>]) \[LeftTriangle] [(';' , [<comments>], ';')] \
<@ asmLine ;\n<comments> = <comment> | (<comment> , <comments>) ;\n<comment> \
= '_WordString' ;\n<labelling> = <label> \[LeftTriangle] ':' <@ asmLabel ;\n\
<label> = '_WordString' ;\n<instruction> = \n('OUT' , '_WordString') |\n\
(<opcode1> , (<reg> \[LeftTriangle] ',') , <mem-ref>) | \n(<opcode2> , (<reg> \
\[LeftTriangle] ',') , ((<reg> \[LeftTriangle] ',') , <operand>)) | \n\
(<opcode3> , (<reg> \[LeftTriangle] ',') , <operand>) | \n('CMP' , (<reg> \
\[LeftTriangle] ',') , <operand>) | \n(<bcond> , <label>) | 'HALT' <@ \
asmInstr ;\n<opcode1> = 'LDR' | 'STR' ;\n<opcode2> = 'ADD' | 'SUB' | 'AND' | \
'ORR' | 'EOR' | 'LSL' | 'LSR' ;\n<opcode3> = 'MOV' | 'CMP' | 'MVN' ;\n<bcond> \
= 'B' | 'BEQ' | 'BNE' | 'BGT' | 'BLT' ;\n<reg> = ('R0' | 'R1' | 'R2' | 'R3' | \
'R4' | 'R5' | 'R6' | 'R7' | 'R8' | 'R9' | 'R10' | 'R11' | 'R12') <@ \
asmRegProcess ;\n<mem-ref> = 'Range[0,65535]' | <reg> <@ asmMemRef ;\n\
<literal> = ('#' \[RightTriangle] 'Range[-4294967296,4294967295]') <@ \
asmLiteralProcess ;\n<operand> = (<literal> | <reg>) <@ asmOperand ;\n\>\""}],
   " ", ";"}]], "Input",
 InitializationCell->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
This next short \[Section] generates the definitions for the parser e.g. pLINE\
\>", "Subsubsection"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"Clear", "[", "\"\<p*\>\"", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"currentParser", "=", 
   RowBox[{"GenerateParsersFromEBNF", "@", 
    RowBox[{"ParseToEBNFTokens", "@", "operationalAssemblyGrammar"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"LeafCount", "@", "currentParser"}]}], "Input",
 InitializationCell->True],

Cell[BoxData["1111"], "Output"]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["The operational assembler", "Section"],

Cell[CellGroupData[{

Cell["Code for 2-s complement", "Subsection"],

Cell[BoxData[{
 RowBox[{"Clear", "[", 
  RowBox[{
  "binChop", ",", "bf", ",", "fits", ",", "binNeg", ",", "to2sComplement", 
   ",", "from2sComplement", ",", "overflow"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"bf", "[", 
   RowBox[{"x_", ",", 
    RowBox[{"b_:", "2"}]}], "]"}], ":=", 
  RowBox[{"BaseForm", "[", 
   RowBox[{"x", ",", "b"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"binChop", "[", 
   RowBox[{"x_", ",", 
    RowBox[{"numBits_:", "16"}]}], "]"}], ":=", 
  RowBox[{"Mod", "[", 
   RowBox[{"x", ",", 
    SuperscriptBox["2", "numBits"]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"binNeg", "[", 
    RowBox[{"x_", ",", 
     RowBox[{"numBits_:", "16"}]}], "]"}], ":=", 
   RowBox[{"BitXor", "[", 
    RowBox[{"x", ",", 
     RowBox[{
      SuperscriptBox["2", "numBits"], "-", "1"}]}], "]"}]}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"note", " ", "this", " ", 
    StyleBox["preserves",
     FontSlant->"Italic"], " ", "any", " ", "bits", " ", "of", " ", "x", " ", 
    "of", " ", "significance", " ", "of", " ", "16", " ", "or", " ", 
    "above"}], " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"fits", "[", 
    RowBox[{"n_", ",", 
     RowBox[{"numBits_:", "16"}]}], "]"}], ":=", 
   RowBox[{
    RowBox[{"BitLength", "[", "n", "]"}], "\[LessEqual]", "numBits"}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{"SetAttributes", "[", 
  RowBox[{"to2sComplement", ",", "Listable"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"to2sComplement", "[", 
    RowBox[{"n_Integer", ",", 
     RowBox[{"numBits_Integer:", "16"}]}], "]"}], "/;", 
   RowBox[{"n", "\[Equal]", 
    RowBox[{"-", 
     SuperscriptBox["2", 
      RowBox[{"numBits", "-", "1"}]]}]}]}], 
  RowBox[{"(*", " ", 
   RowBox[{"special", " ", "case"}], " ", "*)"}], ":=", "\[IndentingNewLine]", 
  SuperscriptBox["2", 
   RowBox[{"numBits", "-", "1"}]]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"to2sComplement", "[", 
   RowBox[{
    RowBox[{"n_Integer", "/;", 
     RowBox[{"n", "<", "0"}]}], ",", 
    RowBox[{"numBits_Integer:", "16"}]}], "]"}], 
  RowBox[{"(*", " ", 
   RowBox[{"other", " ", "negative", " ", "numbers"}], " ", "*)"}], ":=", 
  "\[IndentingNewLine]", 
  RowBox[{"With", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"bitForm", "=", 
      RowBox[{"binChop", "[", 
       RowBox[{
        RowBox[{"-", "n"}], ",", 
        RowBox[{"numBits", "-", "1"}]}], "]"}]}], "}"}], ",", 
    RowBox[{
     RowBox[{"binNeg", "[", 
      RowBox[{"bitForm", ",", "numBits"}], "]"}], "+", "1"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{"binChop", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"binNeg", "[", 
        RowBox[{"bitForm", ",", "numBits"}], "]"}], "+", "1"}], ",", 
      "numBits"}], "]"}], "*)"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"to2sComplement", "[", 
    RowBox[{
     RowBox[{"n_Integer", "/;", 
      RowBox[{"n", "\[GreaterEqual]", "0"}]}], ",", 
     RowBox[{"numBits_Integer:", "16"}]}], "]"}], 
   RowBox[{"(*", " ", 
    RowBox[{"positive", " ", "numbers"}], " ", "*)"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"binChop", "[", 
    RowBox[{"n", ",", 
     RowBox[{"numBits", "-", "1"}]}], "]"}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{"SetAttributes", "[", 
  RowBox[{"from2sComplement", ",", "Listable"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"from2sComplement", "[", 
    RowBox[{"n_Integer", ",", 
     RowBox[{"numBits_Integer:", "16"}]}], "]"}], "/;", 
   RowBox[{"fits", "[", 
    RowBox[{"n", ",", 
     RowBox[{"numBits", "-", "1"}]}], "]"}]}], ":=", 
  "n"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"from2sComplement", "[", 
    RowBox[{"n_Integer", ",", 
     RowBox[{"numBits_Integer:", "16"}]}], "]"}], "/;", 
   RowBox[{"fits", "[", 
    RowBox[{"n", ",", "numBits"}], "]"}]}], ":=", 
  RowBox[{
   RowBox[{"-", 
    RowBox[{"binNeg", "[", 
     RowBox[{"n", ",", "numBits"}], "]"}]}], "-", 
   "1"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"from2sComplement", "[", 
   RowBox[{"n_Integer", ",", 
    RowBox[{"numBits_Integer:", "16"}]}], "]"}], ":=", 
  RowBox[{"(*", " ", 
   RowBox[{"not", " ", "really", " ", "allowed"}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"from2sComplement", "[", 
   RowBox[{
    RowBox[{"binChop", "[", 
     RowBox[{"n", ",", "numBits"}], "]"}], ",", "numBits"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"overflow", "[", 
   RowBox[{"n_", ",", 
    RowBox[{"numBits_:", "16"}]}], "]"}], ":=", 
  RowBox[{"Not", "[", 
   RowBox[{"fits", "[", 
    RowBox[{"n", ",", 
     RowBox[{"numBits", "-", "1"}]}], "]"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "see", " ", "if", " ", "a", " ", "number", " ", "cannot", " ", "be", " ", 
     "represented", " ", "in", " ", "a", " ", "number", " ", 
     RowBox[{"(", 
      RowBox[{"default", ":", "16"}], ")"}], " ", "of", " ", "bits", " ", 
     "using", " ", "2"}], "-", 
    RowBox[{"s", " ", "complement"}]}], " ", "*)"}]}]}], "Input",
 InitializationCell->True],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"254", "//", "bf"}], "\[IndentingNewLine]", 
 RowBox[{"BitShiftLeft", "[", "254", "]"}], "\[IndentingNewLine]", 
 RowBox[{"%", "//", "bf"}], "\[IndentingNewLine]", 
 RowBox[{"binChop", "[", 
  RowBox[{"%%", ",", "8"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"%", "//", "bf"}]}], "Input"],

Cell[BoxData[
 TagBox[
  InterpretationBox[
   SubscriptBox["\<\"11111110\"\>", "\<\"2\"\>"],
   254,
   Editable->False],
  BaseForm[#, 2]& ]], "Output"],

Cell[BoxData["508"], "Output"],

Cell[BoxData[
 TagBox[
  InterpretationBox[
   SubscriptBox["\<\"111111100\"\>", "\<\"2\"\>"],
   508,
   Editable->False],
  BaseForm[#, 2]& ]], "Output"],

Cell[BoxData["252"], "Output"],

Cell[BoxData[
 TagBox[
  InterpretationBox[
   SubscriptBox["\<\"11111100\"\>", "\<\"2\"\>"],
   252,
   Editable->False],
  BaseForm[#, 2]& ]], "Output"]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["The code to load, decode and execute", "Subsection"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"Clear", "[", 
   RowBox[{"asGrab", ",", "asLocations", ",", "asLoad"}], "]"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "grab", " ", "code", " ", "in", " ", "a", " ", "string", " ", "and", " ", 
    "split", " ", "off", " ", "any", " ", "labels"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asGrab", "[", "code_String", "]"}], ":=", 
   RowBox[{"(*", 
    RowBox[{"Association", "@"}], "*)"}], 
   RowBox[{"Map", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"Switch", "[", 
       RowBox[{"#", ",", 
        RowBox[{"{", "_", "}"}], ",", 
        RowBox[{"\"\<\>\"", "\[Rule]", 
         RowBox[{"#", "[", 
          RowBox[{"[", "1", "]"}], "]"}]}], ",", 
        RowBox[{"{", 
         RowBox[{"_", ",", "_"}], "}"}], ",", 
        RowBox[{
         RowBox[{"#", "[", 
          RowBox[{"[", "1", "]"}], "]"}], "\[Rule]", 
         RowBox[{"#", "[", 
          RowBox[{"[", "2", "]"}], "]"}]}], ",", 
        RowBox[{"{", "}"}], ",", 
        RowBox[{
         RowBox[{"##", "&"}], "[", "]"}]}], "\[IndentingNewLine]", 
       RowBox[{"(*", " ", 
        RowBox[{"ignore", " ", "any", " ", "blank", " ", "lines"}], " ", 
        "*)"}], "]"}], "&"}], ",", 
     RowBox[{"Map", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"StringSplit", "[", 
         RowBox[{"#", ",", "\"\< : \>\""}], "]"}], "&"}], ",", 
       RowBox[{"StringSplit", "[", 
        RowBox[{"code", ",", "\"\<\\n\>\""}], "]"}]}], "]"}]}], "]"}]}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "StringSplit", " ", "threaded", " ", "over", " ", "lists", " ", "so", " ", 
    "the", " ", "Map", " ", "may", " ", "be", " ", "unnecessary"}], " ", 
   "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "use", " ", "memory", " ", "offset", " ", "to", " ", "store", " ", 
    "locations", " ", "for", " ", "labelled", " ", "instructions"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asLocations", "[", 
    RowBox[{"p_", ",", 
     RowBox[{"offset_:", "0"}]}], "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{"Association", "@", 
    RowBox[{"MapIndexed", "[", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"First", "[", "#1", "]"}], "\[Rule]", 
        RowBox[{
         RowBox[{"First", "[", "#2", "]"}], "+", "offset"}]}], "&"}], ",", 
      RowBox[{"Normal", "@", "p"}]}], "]"}]}]}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"load", " ", "code", " ", "into", " ", "\"\<memory\>\""}], ":"}], 
   " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"asLoad", "[", 
   RowBox[{"p_", ",", 
    RowBox[{"offset_:", "0"}], ",", 
    RowBox[{"wl_:", "16"}]}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{"not", " ", "needed", " ", "here"}], " ", "*)"}], "]"}], ":=", 
  "\[IndentingNewLine]", 
  RowBox[{"With", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"len", "=", 
      RowBox[{"Length", "[", "p", "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"SparseArray", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"Range", "[", "len", "]"}], "+", "offset"}], "\[Rule]", 
      RowBox[{"Map", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"#", "[", 
          RowBox[{"[", "2", "]"}], "]"}], "&"}], ",", "p"}], "]"}]}], "]"}]}],
    "]"}]}]}], "Input",
 InitializationCell->True],

Cell[BoxData[{
 RowBox[{
  RowBox[{"Clear", "[", 
   RowBox[{
   "asInterpretStep", ",", "operation", ",", " ", "compare", ",", "asFetch", 
    ",", "asRun", ",", " ", "asDecodeDebugging", ",", "asExecute", ",", 
    "asRunDecoded", ",", "asRun", ",", "asLoadRun", ",", "asLoadRunDecoded", 
    ",", " ", "asPrint"}], "]"}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"binary", " ", "operations"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<ADD\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], ":=", 
   RowBox[{
    RowBox[{"binChop", "[", 
     RowBox[{
      RowBox[{"Plus", "[", 
       RowBox[{"#1", ",", "#2"}], "]"}], ",", "wl"}], "]"}], "&"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<SUB\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], ":=", 
   RowBox[{
    RowBox[{"binChop", "[", 
     RowBox[{
      RowBox[{"Plus", "[", 
       RowBox[{"#1", ",", 
        RowBox[{
         RowBox[{"binNeg", "[", 
          RowBox[{"#2", ",", "wl"}], "]"}], "+", "1"}]}], "]"}], ",", "wl"}], 
     "]"}], "&"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<AND\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], "=", "BitAnd"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<ORR\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], "=", "BitOr"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<EOR\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], "=", "BitXor"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"operation", "[", 
    RowBox[{"\"\<LSL\>\"", ",", 
     RowBox[{"wl_:", "16"}]}], "]"}], ":=", 
   RowBox[{
    RowBox[{"binChop", "[", 
     RowBox[{
      RowBox[{"BitShiftLeft", "[", "#", "]"}], ",", "wl"}], "]"}], "&"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"operation", "[", 
     RowBox[{"\"\<LSR\>\"", ",", 
      RowBox[{"wl_:", "16"}]}], "]"}], "=", "BitShiftRight"}], ";"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"unary", " ", "operations"}], " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"operation", "[", 
     RowBox[{"\"\<MOV\>\"", ",", 
      RowBox[{"wl_:", "16"}]}], "]"}], "=", "Identity"}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"no", " ", "need", " ", "to", " ", "truncate"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"operation", "[", 
     RowBox[{"\"\<MVN\>\"", ",", 
      RowBox[{"wl_:", "16"}]}], "]"}], ":=", 
    RowBox[{
     RowBox[{"binNeg", "[", 
      RowBox[{"#", ",", "wl"}], "]"}], "&"}]}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"no", " ", "need", " ", "to", " ", 
    RowBox[{"truncate", "?"}]}], " ", "*)"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{"Clear", "[", "asFetch", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asFetch", "[", 
    RowBox[{"mem_", ",", "pc_"}], "]"}], ":=", 
   RowBox[{"mem", "[", 
    RowBox[{"[", "pc", "]"}], "]"}]}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{
   "now", " ", "fetches", " ", "directly", " ", "from", " ", "memory"}], " ", 
   "*)"}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asDecodeDebugging", "[", "p_String", "]"}], ":=", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"ParseShortest", "[", "pINSTRUCTION", "]"}], "[", 
      RowBox[{"ToTokens", "[", 
       RowBox[{"p", ",", 
        RowBox[{"{", 
         RowBox[{
         "\"\<;\>\"", ",", "\"\<#\>\"", ",", "\"\<:\>\"", ",", "\"\<,\>\""}], 
         "}"}]}], "]"}], "]"}], ")"}], "\[LeftDoubleBracket]", 
    RowBox[{"1", ",", "2"}], "\[RightDoubleBracket]"}]}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"asDecodeDebugging", "[", "p_String", "]"}], ":=", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"ParseShortest", "[", "pINSTRUCTION", "]"}], "[", 
       RowBox[{"ToTokens", "[", 
        RowBox[{"p", ",", 
         RowBox[{"{", "}"}]}], "]"}], "]"}], ")"}], "\[LeftDoubleBracket]", 
     RowBox[{"1", ",", "2"}], "\[RightDoubleBracket]"}]}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asDecodeDebugging", "[", "v_", "]"}], ":=", "v"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"compare", "[", 
    RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
   RowBox[{"Which", "[", 
    RowBox[{
     RowBox[{"x", "\[Equal]", "y"}], ",", "\"\<EQ\>\"", ",", 
     RowBox[{"x", "<", "y"}], ",", "\"\<LT\>\"", ",", 
     RowBox[{"x", ">", "y"}], ",", "\"\<GT\>\"", ",", "True", ",", 
     "\"\<Fail\>\""}], "]"}]}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "needs", " ", "extensive", " ", "testing", " ", "to", " ", "ensure", " ", 
    "processor", " ", "operates", " ", "correctly", " ", "with", " ", 
    "finite", " ", "word", " ", "length"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<STR\>\"", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", 
           RowBox[{"(*", " ", 
            RowBox[{"counting", " ", "from", " ", "0"}], " ", "*)"}], "]"}], 
          ",", 
          RowBox[{"asmMemRef", "[", "m_", "]"}]}], "}"}]}], "}"}], "]"}], ",",
      "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"ReplacePart", "[", 
     RowBox[{"mem", ",", 
      RowBox[{"m", "\[Rule]", 
       RowBox[{"registers", "[", 
        RowBox[{"[", 
         RowBox[{"r", "+", "1"}], "]"}], "]"}]}]}], "]"}], ",", 
    RowBox[{"pc", "+", "1"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<LDR\>\"", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", "]"}], ",", 
          RowBox[{"asmMemRef", "[", "m_", "]"}]}], "}"}]}], "}"}], "]"}], ",",
      "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"pc", "+", "1"}], ",", "status", ",", 
    RowBox[{"ReplacePart", "[", 
     RowBox[{"registers", ",", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"r", "+", "1"}], ")"}], "\[Rule]", 
       RowBox[{"mem", "[", 
        RowBox[{"[", "m", "]"}], "]"}]}]}], "]"}]}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<STR\>\"", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", 
           RowBox[{"(*", " ", 
            RowBox[{"counting", " ", "from", " ", "0"}], " ", "*)"}], "]"}], 
          ",", 
          RowBox[{"asmMemRef", "[", 
           RowBox[{"asmReg", "[", "m_", "]"}], "]"}]}], "}"}]}], "}"}], "]"}],
      ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"ReplacePart", "[", 
     RowBox[{"mem", ",", 
      RowBox[{
       RowBox[{"registers", "[", 
        RowBox[{"[", 
         RowBox[{"m", "+", "1"}], "]"}], "]"}], "\[Rule]", 
       RowBox[{"registers", "[", 
        RowBox[{"[", 
         RowBox[{"r", "+", "1"}], "]"}], "]"}]}]}], "]"}], ",", 
    RowBox[{"pc", "+", "1"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asInterpretStep", "[", 
     RowBox[{
      RowBox[{"asmInstr", "[", 
       RowBox[{"{", 
        RowBox[{"\"\<LDR\>\"", ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"asmReg", "[", "r_", "]"}], ",", 
           RowBox[{"asmMemRef", "[", 
            RowBox[{"asmReg", "[", "m_", "]"}], "]"}]}], "}"}]}], "}"}], 
       "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
     "}"}], "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"mem", ",", 
     RowBox[{"pc", "+", "1"}], ",", "status", ",", 
     RowBox[{"ReplacePart", "[", 
      RowBox[{"registers", ",", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"r", "+", "1"}], ")"}], "\[Rule]", 
        RowBox[{"mem", "[", 
         RowBox[{"[", 
          RowBox[{"registers", "[", 
           RowBox[{"[", 
            RowBox[{"m", "+", "1"}], "]"}], "]"}], "]"}], "]"}]}]}], "]"}]}], 
    "}"}]}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "Actually", " ", "MOV", " ", "and", " ", "MVN", " ", "are", " ", "both", 
    " ", "unary", " ", "operations", " ", "with", " ", "Identity", " ", "for",
     " ", "MOV"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
     RowBox[{"asInterpretStep", "[", 
      RowBox[{
       RowBox[{"asmInstr", "[", 
        RowBox[{"{", 
         RowBox[{"\"\<MOV\>\"", ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"asmReg", "[", "r_", "]"}], ",", 
            RowBox[{"asmOperand", "[", 
             RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], "}"}], 
        "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
     RowBox[{"{", 
      RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
      "}"}], "]"}], ":=", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{
      RowBox[{"{", 
       RowBox[{"mem", ",", 
        RowBox[{"pc", "+", "1"}], ",", "status", ",", 
        RowBox[{"ReplacePart", "[", 
         RowBox[{"registers", ",", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{"r", "+", "1"}], ")"}], "\[Rule]", 
           RowBox[{
            RowBox[{"operation", "[", "\"\<MOV\>\"", "]"}], "[", "l", 
            "]"}]}]}], "]"}]}], "}"}], "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"asInterpretStep", "[", 
        RowBox[{
         RowBox[{"asmInstr", "[", 
          RowBox[{"{", 
           RowBox[{"\"\<MOV\>\"", ",", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
              RowBox[{"asmOperand", "[", 
               RowBox[{"asmReg", "[", "r2_", "]"}], "]"}]}], "}"}]}], "}"}], 
          "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
       RowBox[{"{", 
        RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
        "}"}], "]"}]}], ":=", "\[IndentingNewLine]", 
     RowBox[{"{", 
      RowBox[{"mem", ",", 
       RowBox[{"pc", "+", "1"}], ",", "status", ",", 
       RowBox[{"ReplacePart", "[", 
        RowBox[{"registers", ",", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"r1", "+", "1"}], ")"}], "\[Rule]", 
          RowBox[{
           RowBox[{"operation", "[", "\"\<MOV\>\"", "]"}], "[", 
           RowBox[{"registers", "[", 
            RowBox[{"[", 
             RowBox[{"r2", "+", "1"}], "]"}], "]"}], "]"}]}]}], "]"}]}], 
      "}"}]}]}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"op", ":", 
         RowBox[{"\"\<MOV\>\"", "|", "\"\<MVN\>\""}]}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"pc", "+", "1"}], ",", "status", ",", 
    RowBox[{"ReplacePart", "[", 
     RowBox[{"registers", ",", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"r", "+", "1"}], ")"}], "\[Rule]", 
       RowBox[{
        RowBox[{"operation", "[", 
         RowBox[{"op", ",", "$wordLength"}], "]"}], "[", "l", "]"}]}]}], 
     "]"}]}], "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"op", ":", 
         RowBox[{"\"\<MOV\>\"", "|", "\"\<MVN\>\""}]}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmReg", "[", "r2_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"pc", "+", "1"}], ",", "status", ",", 
    RowBox[{"ReplacePart", "[", 
     RowBox[{"registers", ",", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"r1", "+", "1"}], ")"}], "\[Rule]", 
       RowBox[{
        RowBox[{"operation", "[", 
         RowBox[{"op", ",", "$wordLength"}], "]"}], "[", 
        RowBox[{"registers", "[", 
         RowBox[{"[", 
          RowBox[{"r2", "+", "1"}], "]"}], "]"}], "]"}]}]}], "]"}]}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<CMP\>\"", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmReg", "[", "r2_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", " ", 
    RowBox[{"pc", "+", "1"}], ",", 
    RowBox[{"compare", "[", 
     RowBox[{
      RowBox[{"registers", "[", 
       RowBox[{"[", 
        RowBox[{"r1", "+", "1"}], "]"}], "]"}], ",", 
      RowBox[{"registers", "[", 
       RowBox[{"[", 
        RowBox[{"r2", "+", "1"}], "]"}], "]"}]}], "]"}], ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<CMP\>\"", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", " ", 
    RowBox[{"pc", "+", "1"}], ",", 
    RowBox[{"compare", "[", 
     RowBox[{
      RowBox[{"registers", "[", 
       RowBox[{"[", 
        RowBox[{"r", "+", "1"}], "]"}], "]"}], ",", "l"}], "]"}], ",", 
    "registers"}], "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"op_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"asmReg", "[", "r2_", "]"}], ",", 
            RowBox[{"asmOperand", "[", 
             RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], 
         "}"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"With", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"f", "=", 
      RowBox[{"operation", "[", 
       RowBox[{"op", ",", "$wordLength"}], "]"}]}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"{", 
     RowBox[{"mem", ",", 
      RowBox[{"pc", "+", "1"}], ",", "status", ",", 
      RowBox[{"ReplacePart", "[", 
       RowBox[{"registers", ",", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"r1", "+", "1"}], ")"}], "\[Rule]", 
         RowBox[{"f", "[", 
          RowBox[{
           RowBox[{"registers", "[", 
            RowBox[{"[", 
             RowBox[{"r2", "+", "1"}], "]"}], "]"}], ",", "l"}], "]"}]}]}], 
       "]"}]}], "}"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"op_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"asmReg", "[", "r2_", "]"}], ",", 
            RowBox[{"asmOperand", "[", 
             RowBox[{"asmReg", "[", "r3_", "]"}], "]"}]}], "}"}]}], "}"}]}], 
       "}"}], "]"}], ",", "\[IndentingNewLine]", "labels_"}], "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"With", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"f", "=", 
      RowBox[{"operation", "[", 
       RowBox[{"op", ",", "$wordLength"}], "]"}]}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"{", 
     RowBox[{"mem", ",", 
      RowBox[{"pc", "+", "1"}], ",", "status", ",", 
      RowBox[{"ReplacePart", "[", 
       RowBox[{"registers", ",", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"r1", "+", "1"}], ")"}], "\[Rule]", 
         RowBox[{"f", "[", 
          RowBox[{
           RowBox[{"registers", "[", 
            RowBox[{"[", 
             RowBox[{"r2", "+", "1"}], "]"}], "]"}], ",", 
           RowBox[{"registers", "[", 
            RowBox[{"[", 
             RowBox[{"r3", "+", "1"}], "]"}], "]"}]}], "]"}]}]}], "]"}]}], 
     "}"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<B\>\"", ",", "l_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"labels", "[", "l", "]"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<BEQ\>\"", ",", "l_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{"status", "\[Equal]", "\"\<EQ\>\""}], ",", 
      RowBox[{"labels", "[", "l", "]"}], ",", 
      RowBox[{"pc", "+", "1"}]}], "]"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<BNE\>\"", ",", "l_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{"status", "\[NotEqual]", "\"\<EQ\>\""}], ",", 
      RowBox[{"labels", "[", "l", "]"}], ",", 
      RowBox[{"pc", "+", "1"}]}], "]"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<BLT\>\"", ",", "l_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{"status", "\[Equal]", "\"\<LT\>\""}], ",", 
      RowBox[{"labels", "[", "l", "]"}], ",", 
      RowBox[{"pc", "+", "1"}]}], "]"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<BGT\>\"", ",", "l_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
   "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{"mem", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{"status", "\[Equal]", "\"\<GT\>\""}], ",", 
      RowBox[{"labels", "[", "l", "]"}], ",", 
      RowBox[{"pc", "+", "1"}]}], "]"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"\"\<OUT\>\"", ",", "msg_"}], "}"}], "]"}], ",", "labels_"}], 
    "]"}], "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"MessageDialog", "[", 
     RowBox[{"\"\<PC: \>\"", "<>", 
      RowBox[{"ToString", "[", "pc", "]"}], "<>", "\"\<\\n\>\"", "<>", 
      "msg"}], "]"}], ";", 
    RowBox[{"{", 
     RowBox[{"mem", ",", 
      RowBox[{"pc", " ", "+", "1"}], ",", "status", ",", "registers"}], 
     "}"}]}], ")"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asInterpretStep", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", "\"\<HALT\>\"", "]"}], ",", "labels_"}], "]"}], 
   "[", 
   RowBox[{"{", 
    RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
    "}"}], "]"}], ":=", 
  RowBox[{"{", 
   RowBox[{"mem", ",", "0", " ", 
    RowBox[{"(*", " ", 
     RowBox[{
     "special", " ", "value", " ", "not", " ", "otherwise", " ", "used"}], 
     " ", "*)"}], ",", "status", ",", "registers"}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asInterpretStep", "[", 
     RowBox[{"instr_", ",", "labels_"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
     "}"}], "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{"(*", "  ", 
    RowBox[{
     RowBox[{"preconditions", ":", " ", "\[IndentingNewLine]", "   ", 
      RowBox[{
      "the", " ", "instructions", " ", "are", " ", "loaded", " ", "into", " ",
        "memory", " ", "already", " ", "and", " ", "we", " ", "know", " ", 
       "their", " ", 
       StyleBox["locations",
        FontSlant->"Italic"]}]}], ";", "\[IndentingNewLine]", "\t", 
     RowBox[{
     "we", " ", "have", " ", "stored", " ", "all", " ", "previous", " ", 
      RowBox[{"labels", " ", "'"}], 
      RowBox[{"appropriately", "'"}], " ", "in", " ", "labels"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"(", 
      RowBox[{
      "actually", " ", "labels", " ", "is", " ", "an", " ", "association", 
       " ", "between", " ", "labels", " ", "and", " ", "instructions", " ", 
       "now"}], ")"}], ";", "\[IndentingNewLine]", "\t", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
       "note", " ", "this", " ", "means", " ", "that", " ", "we", " ", "need",
         " ", "a", " ", "first", " ", "pass", " ", "to", " ", "do", " ", 
        "this", " ", "step"}], ",", "\[IndentingNewLine]", "\t", 
       RowBox[{
        RowBox[{
        "and", " ", "further", " ", "that", " ", "this", " ", "will", " ", 
         "mean", " ", "a", " ", "re"}], "-", 
        RowBox[{"implementation", " ", "of", " ", "map"}]}]}], ")"}], ";", 
     "\[IndentingNewLine]", "\t", 
     RowBox[{
     "the", " ", "registers", " ", "are", " ", "all", " ", "correctly", " ", 
      "filled"}], ";", "\[IndentingNewLine]", "\t", 
     RowBox[{
     "the", " ", "current", " ", "instruction", " ", "does", " ", "not", " ", 
      "reference", " ", "a", " ", "memory", " ", "location", " ", "outside", 
      " ", "of", " ", "mem"}], ";", "\[IndentingNewLine]", "\t", 
     RowBox[{
     "the", " ", "current", " ", "instruction", " ", "does", " ", "not", " ", 
      "reference", " ", "a", " ", "label", " ", "outside", " ", "of", " ", 
      "labels"}], ";", "\[IndentingNewLine]", "\t", 
     RowBox[{
      RowBox[{"we", " ", "are", " ", "not", " ", "writing", " ", "self"}], 
      "-", 
      RowBox[{
      "modifying", " ", "code", " ", "so", " ", "we", " ", "cannot", " ", 
       "branch", " ", "to", " ", "an", " ", "arbitrary", " ", 
       "\[IndentingNewLine]", "  ", "point", " ", "in", " ", "memory"}]}], 
     ";"}], "\[IndentingNewLine]", "*)"}], "\[IndentingNewLine]", 
   RowBox[{"(*", "instr", 
    RowBox[{"(*", "UNDEFINED", "*)"}], "*)"}], "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"mem", ",", 
     RowBox[{"pc", " ", "+", "1"}], ",", "status", ",", "registers"}], 
    "}"}]}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", 
           RowBox[{"(*", " ", 
            RowBox[{"counting", " ", "from", " ", "0"}], " ", "*)"}], "]"}], 
          ",", 
          RowBox[{"asmMemRef", "[", "m_", "]"}]}], "}"}]}], "}"}], "]"}], ",",
      "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r", "]"}], "<>", "\"\< , \>\"", "<>", 
    RowBox[{"ToString", "[", "m", "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", 
           RowBox[{"(*", " ", 
            RowBox[{"counting", " ", "from", " ", "0"}], " ", "*)"}], "]"}], 
          ",", 
          RowBox[{"asmMemRef", "[", 
           RowBox[{"asmReg", "[", "m_", "]"}], "]"}]}], "}"}]}], "}"}], "]"}],
      ",", "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r", "]"}], "<>", "\"\< , R\>\"", "<>", 
    RowBox[{"ToString", "[", "m", "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r", "]"}], "<>", "\"\< , # \>\"", "<>", 
    RowBox[{"ToString", "[", 
     RowBox[{"If", "[", 
      RowBox[{
       RowBox[{"format", "\[Equal]", "\"\<signed\>\""}], ",", 
       RowBox[{"from2sComplement", "@", "l"}], ",", "l"}], "]"}], "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"asmOperand", "[", 
           RowBox[{"asmReg", "[", "r2_", "]"}], "]"}]}], "}"}]}], "}"}], 
      "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r1", "]"}], "<>", "\"\< , R\>\"", "<>", 
    RowBox[{"ToString", "[", "r2", "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"asmReg", "[", "r2_", "]"}], ",", 
            RowBox[{"asmOperand", "[", 
             RowBox[{"asmLiteral", "[", "l_", "]"}], "]"}]}], "}"}]}], 
         "}"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r1", "]"}], "<>", "\"\< , R\>\"", "<>", 
    RowBox[{"ToString", "[", "r2", "]"}], "<>", "\"\< , # \>\"", "<>", 
    RowBox[{"ToString", "[", 
     RowBox[{"If", "[", 
      RowBox[{
       RowBox[{"format", "\[Equal]", "\"\<signed\>\""}], ",", 
       RowBox[{"from2sComplement", "@", "l"}], ",", "l"}], "]"}], "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"opString_", ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"asmReg", "[", "r1_", "]"}], ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"asmReg", "[", "r2_", "]"}], ",", 
            RowBox[{"asmOperand", "[", 
             RowBox[{"asmReg", "[", "r3_", "]"}], "]"}]}], "}"}]}], "}"}]}], 
       "}"}], "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   RowBox[{"opString", "<>", "\"\< R\>\"", "<>", 
    RowBox[{"ToString", "[", "r1", "]"}], "<>", "\"\< , R\>\"", "<>", 
    RowBox[{"ToString", "[", "r2", "]"}], "<>", "\"\< , R\>\"", "<>", 
    RowBox[{"ToString", "[", "r3", "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", 
      RowBox[{"{", 
       RowBox[{"branchString_", ",", "l_"}], "}"}], "]"}], ",", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   RowBox[{"branchString", "<>", "\"\< \>\"", "<>", 
    RowBox[{"ToString", "[", "l", "]"}]}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{
     RowBox[{"asmInstr", "[", "\"\<HALT\>\"", "]"}], ",", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", "\"\<HALT\>\""}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asPrint", "[", 
    RowBox[{"instr_", ",", 
     RowBox[{"format_:", "\"\<signed\>\""}]}], "]"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"ToString", "[", "instr", "]"}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asExecute", "[", 
     RowBox[{"instr_", ",", "labels_"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
    "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"asInterpretStep", "[", 
     RowBox[{"instr", ",", "labels"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem", ",", "pc", ",", "status", ",", "registers"}], "}"}], 
    "]"}]}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "seems", " ", "to", " ", "work", " ", "out", " ", "of", " ", "the", " ", 
    "box", " ", "now"}], " ", "*)"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asStep", "[", "labels_", "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", "status_", ",", "registers_"}], "}"}], 
    "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"asExecute", "[", 
     RowBox[{
      RowBox[{"asDecodeDebugging", "[", 
       RowBox[{"asFetch", "[", 
        RowBox[{"mem", ",", "pc"}], "]"}], "]"}], ",", "labels"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem", ",", "pc", ",", "status", ",", "registers"}], "}"}], 
    "]"}]}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asRunDecoded", "[", 
     RowBox[{"program_List", ",", "labels_"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
     "}"}], "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{"FoldList", "[", 
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"asExecute", "[", 
        RowBox[{"#2", ",", "labels"}], "]"}], "[", "#1", "]"}], "&"}], ",", 
     RowBox[{"{", 
      RowBox[{"mem", ",", "pc", ",", "status", ",", "registers"}], "}"}], ",",
      "program"}], "]"}]}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "ok", " ", "that", " ", "does", " ", "not", " ", "really", " ", "work", 
    " ", "since", " ", "the", " ", "function", " ", "needs", " ", "to", " ", 
    "access", " ", "the", " ", "pc", " ", "first"}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "but", " ", "it", " ", "can", " ", "be", " ", "used", " ", "to", " ", 
    "work", " ", "with", " ", "a", " ", "limited", " ", "number", " ", "of", 
    " ", "instructions", " ", "that", " ", "do", " ", "not", " ", 
    StyleBox["read",
     FontSlant->"Italic"], 
    StyleBox[" ",
     FontSlant->"Plain"], 
    StyleBox["the",
     FontSlant->"Plain"], 
    StyleBox[" ",
     FontSlant->"Plain"], 
    StyleBox["pc",
     FontSlant->"Plain"]}], 
   StyleBox[" ",
    FontSlant->"Plain"], 
   StyleBox["*)",
    FontSlant->"Plain"]}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"asRun", "[", 
     RowBox[{"code_List", ",", "labels_"}], "]"}], "[", 
    RowBox[{"{", 
     RowBox[{"mem_", ",", "pc_", ",", " ", "status_", ",", "registers_"}], 
     "}"}], "]"}], ":=", "\[IndentingNewLine]", "l"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"not", " ", "so", " ", "far", " ", "needed"}], " ", "*)"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"asLoadRun", "[", 
    RowBox[{"code_String", ",", 
     RowBox[{"offset_:", "0"}], ",", 
     RowBox[{"maxSteps_:", "100"}]}], "]"}], ":=", "\[IndentingNewLine]", 
   RowBox[{"With", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"iniPc", "=", 
        RowBox[{"1", "+", "offset"}]}], ",", 
       RowBox[{"iniStatus", "=", "\"\<EQ\>\""}], ",", 
       RowBox[{"iniRegisters", "=", 
        RowBox[{"Range", "[", 
         RowBox[{"0", ",", "12"}], "]"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"codeArray", "=", 
        RowBox[{"asGrab", "[", "code", "]"}]}]}], "}"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"With", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"iniMem", "=", 
          RowBox[{"asLoad", "[", 
           RowBox[{"codeArray", ",", "offset"}], "]"}]}], ",", 
         RowBox[{"locs", "=", 
          RowBox[{"asLocations", "[", 
           RowBox[{"codeArray", ",", "offset"}], "]"}]}]}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"NestWhileList", "[", 
        RowBox[{
         RowBox[{"asStep", "[", "locs", "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
          "iniMem", ",", "iniPc", ",", "iniStatus", ",", "iniRegisters"}], 
          "}"}], ",", 
         RowBox[{
          RowBox[{
           RowBox[{"#", "[", 
            RowBox[{"[", "2", "]"}], "]"}], ">", "0"}], "&"}], ",", "1", ",", 
         "maxSteps"}], "]"}]}], "]"}]}], "]"}]}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"asLoadRunDecoded", "[", 
   RowBox[{"code_String", ",", 
    RowBox[{"offset_:", "0"}], ",", 
    RowBox[{"maxSteps_:", "100"}], ",", 
    RowBox[{"wl_Integer:", "16"}]}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"With", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"iniPc", "=", 
       RowBox[{"1", "+", "offset"}]}], ",", 
      RowBox[{"iniStatus", "=", "\"\<EQ\>\""}], ",", 
      RowBox[{"iniRegisters", "=", 
       RowBox[{"Range", "[", 
        RowBox[{"0", ",", "12"}], "]"}]}], ",", "\[IndentingNewLine]", 
      RowBox[{"codeArray", "=", 
       RowBox[{"asGrab", "[", "code", "]"}]}]}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"With", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"loadedCode", "=", 
         RowBox[{"asLoad", "[", 
          RowBox[{"codeArray", ",", "offset", ",", "wl"}], "]"}]}], ",", 
        RowBox[{"locs", "=", 
         RowBox[{"asLocations", "[", 
          RowBox[{"codeArray", ",", "offset"}], "]"}]}]}], "}"}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"With", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"iniMem", "=", 
          RowBox[{"Map", "[", 
           RowBox[{"asDecodeDebugging", ",", "loadedCode"}], "]"}]}], "}"}], 
        ",", "\[IndentingNewLine]", 
        RowBox[{"NestWhileList", "[", 
         RowBox[{
          RowBox[{"asStep", "[", "locs", "]"}], ",", 
          RowBox[{"{", 
           RowBox[{
           "iniMem", ",", "iniPc", ",", "iniStatus", ",", "iniRegisters"}], 
           "}"}], ",", 
          RowBox[{
           RowBox[{
            RowBox[{"#", "[", 
             RowBox[{"[", "2", "]"}], "]"}], ">", "0"}], "&"}], ",", "1", ",",
           "maxSteps"}], "]"}]}], "]"}]}], "]"}]}], "]"}]}]}], "Input",
 InitializationCell->True]
}, Closed]],

Cell[CellGroupData[{

Cell["The interface to inspect the results of running the code", "Subsection"],

Cell[BoxData[{
 RowBox[{"Clear", "[", "runMyProgram", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"runMyProgram", "[", 
   RowBox[{"program_", ",", 
    RowBox[{"start_Integer:", "50"}], ",", " ", 
    RowBox[{"limit_Integer:", "100"}], ",", 
    RowBox[{"wl_:", "16"}]}], "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"DynamicModule", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
     "tab", ",", "code", ",", "codeRun", ",", "memoryRules", ",", "len", ",", 
      "keys", ",", "pcList", ",", "statusList", ",", "registerList", ",", 
      "memories", ",", 
      RowBox[{"showCode", "=", "False"}]}], "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"Column", "[", 
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"Panel", "[", 
        RowBox[{"\"\<Assembler version \>\"", "<>", 
         RowBox[{"ToString", "@", "$assemblyVersion"}]}], "]"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"Panel", "[", 
        RowBox[{"Dynamic", "@", 
         RowBox[{"If", "[", 
          RowBox[{"showCode", ",", 
           RowBox[{"(*", 
            RowBox[{"Dynamic", "@"}], "*)"}], "code", ",", 
           "\"\<Code hidden\>\""}], "]"}]}], "]"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"Manipulate", "[", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"Column", "[", 
          RowBox[{"{", "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"Row", "@", 
             RowBox[{"{", "\[IndentingNewLine]", 
              RowBox[{
               RowBox[{"Button", "[", 
                RowBox[{"\"\<Reload code\>\"", ",", 
                 RowBox[{"(", 
                  RowBox[{
                   RowBox[{"code", "=", "program"}], ";", 
                   RowBox[{"codeRun", "=", 
                    RowBox[{"asLoadRunDecoded", "[", 
                    RowBox[{"code", ",", "offset", ",", "lim", ",", "wl"}], 
                    "]"}]}], ";", 
                   RowBox[{"len", "=", 
                    RowBox[{"Length", "[", "codeRun", "]"}]}], ";", 
                   RowBox[{
                    RowBox[{"{", 
                    RowBox[{
                    "memories", ",", "pcList", ",", "statusList", ",", 
                    "registerList"}], "}"}], "=", 
                    RowBox[{"Transpose", "[", "codeRun", "]"}]}], ";", 
                   "\[IndentingNewLine]", 
                   RowBox[{"memoryRules", "=", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"Drop", "[", 
                    RowBox[{
                    RowBox[{"ArrayRules", "[", "#", "]"}], ",", 
                    RowBox[{"-", "1"}]}], "]"}], "&"}], "/@", "memories"}]}], 
                   " ", ";", "\[IndentingNewLine]", 
                   RowBox[{"memoryRules", "=", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"Map", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{
                    RowBox[{"#", "[", 
                    RowBox[{"[", 
                    RowBox[{"1", ",", "1"}], "]"}], "]"}], "\[Rule]", 
                    RowBox[{"asPrint", "[", 
                    RowBox[{
                    RowBox[{"#1", "[", 
                    RowBox[{"[", "2", "]"}], "]"}], ",", "format"}], "]"}]}], 
                    "&"}], ",", "#"}], "]"}], "&"}], "/@", "memoryRules"}]}], 
                   ";", "\[IndentingNewLine]", 
                   RowBox[{"memoryRules", "=", 
                    RowBox[{"Map", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"SortBy", "[", 
                    RowBox[{"#", ",", 
                    RowBox[{
                    RowBox[{"#", "[", 
                    RowBox[{"[", "1", "]"}], "]"}], "&"}]}], "]"}], "&"}], 
                    ",", "memoryRules"}], "]"}]}], ";", "\[IndentingNewLine]", 
                   RowBox[{"keys", "=", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"Map", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"#", "[", 
                    RowBox[{"[", "1", "]"}], "]"}], "&"}], ",", "#"}], "]"}], 
                    "&"}], "/@", "memoryRules"}]}], ";"}], ")"}]}], "]"}], 
               ",", "\[IndentingNewLine]", 
               RowBox[{"Button", "[", 
                RowBox[{"\"\<Show code\>\"", ",", 
                 RowBox[{"(", 
                  RowBox[{"showCode", "=", "True"}], ")"}]}], "]"}], ",", 
               "\[IndentingNewLine]", 
               RowBox[{"Button", "[", 
                RowBox[{"\"\<Hide code\>\"", ",", 
                 RowBox[{"(", 
                  RowBox[{"showCode", "=", "False"}], ")"}]}], "]"}]}], 
              "}"}]}], ",", "\[IndentingNewLine]", 
            RowBox[{"TabView", "[", "\[IndentingNewLine]", 
             RowBox[{
              RowBox[{"{", 
               RowBox[{
                RowBox[{"\"\<base 10\>\"", "\[Rule]", "\[IndentingNewLine]", 
                 RowBox[{"Column", "[", 
                  RowBox[{"{", "\[IndentingNewLine]", 
                   RowBox[{
                    RowBox[{"Grid", "[", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"Text", "@", "\"\<Memory\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<PC\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<Status\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<CIR\>\""}]}], "}"}], ",", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"TabView", "[", " ", 
                    RowBox[{
                    RowBox[{"Thread", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{"#1", ",", "#2"}], "}"}], "&"}], "[", 
                    RowBox[{
                    RowBox[{"keys", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ",", 
                    RowBox[{"Normal", "[", 
                    RowBox[{"memoryRules", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "]"}]}], "]"}], "]"}], 
                    ",", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ",", 
                    RowBox[{"Appearance", "\[Rule]", 
                    RowBox[{"{", 
                    RowBox[{"\"\<Limited\>\"", ",", "10"}], "}"}]}]}], "]"}], 
                    ",", "\[IndentingNewLine]", 
                    RowBox[{"Text", "@", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                    RowBox[{"Text", "@", 
                    RowBox[{"statusList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                    RowBox[{"Text", "@", 
                    RowBox[{"asPrint", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"memories", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "[", 
                    RowBox[{"[", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "]"}], "]"}], ",", 
                    "format"}], "]"}]}]}], "}"}]}], "}"}], "]"}], ",", 
                    "\[IndentingNewLine]", 
                    RowBox[{"Grid", "[", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{"Text", "@", "\"\<Registers\>\""}], "}"}], ",", 
                    RowBox[{"{", 
                    RowBox[{"Text", "@", 
                    RowBox[{"Grid", "[", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{
                    "R0", ",", "R1", ",", "R2", ",", "R3", ",", "R4", ",", 
                    "R5", ",", "R6", ",", "R7", ",", "R8", ",", "R9", ",", 
                    "R10", ",", "R11", ",", "R12"}], "}"}], ",", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"If", "[", 
                    RowBox[{
                    RowBox[{"format", "\[Equal]", "\"\<signed\>\""}], ",", 
                    RowBox[{"from2sComplement", "[", 
                    RowBox[{"#", ",", "$wordLength"}], "]"}], ",", "#"}], 
                    "]"}], "&"}], "/@", 
                    RowBox[{"registerList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}]}], "}"}], "]"}]}], 
                    "}"}]}], "}"}], "]"}]}], "\[IndentingNewLine]", "}"}], 
                  "]"}]}], ",", "\[IndentingNewLine]", 
                RowBox[{"\"\<base 2\>\"", "\[Rule]", "\[IndentingNewLine]", 
                 RowBox[{"Column", "[", 
                  RowBox[{"{", "\[IndentingNewLine]", 
                   RowBox[{
                    RowBox[{"Grid", "[", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"Text", "@", "\"\<Memory\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<PC\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<Status\>\""}], ",", 
                    RowBox[{"Text", "@", "\"\<CIR\>\""}]}], "}"}], ",", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"TabView", "[", " ", 
                    RowBox[{
                    RowBox[{"Thread", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{"#1", ",", "#2"}], "}"}], "&"}], "[", 
                    RowBox[{
                    RowBox[{"keys", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ",", 
                    RowBox[{"Normal", "[", 
                    RowBox[{"memoryRules", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "]"}]}], "]"}], "]"}], 
                    ",", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ",", 
                    RowBox[{"Appearance", "\[Rule]", 
                    RowBox[{"{", 
                    RowBox[{"\"\<Limited\>\"", ",", "10"}], "}"}]}]}], "]"}], 
                    ",", "\[IndentingNewLine]", 
                    RowBox[{"Text", "@", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                    RowBox[{"Text", "@", 
                    RowBox[{"statusList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                    "\[IndentingNewLine]", 
                    RowBox[{"Text", "@", 
                    RowBox[{"asPrint", "[", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"memories", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "[", 
                    RowBox[{"[", 
                    RowBox[{"pcList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "]"}], "]"}], ",", 
                    "format"}], "]"}]}]}], "}"}]}], "}"}], "]"}], ",", 
                    "\[IndentingNewLine]", 
                    RowBox[{"Grid", "[", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{"Text", "@", "\"\<Registers\>\""}], "}"}], ",", 
                    RowBox[{"{", "\[IndentingNewLine]", 
                    RowBox[{"Text", "@", "\[IndentingNewLine]", 
                    RowBox[{"Grid", "[", 
                    RowBox[{
                    RowBox[{"Transpose", "@", 
                    RowBox[{"{", 
                    RowBox[{
                    RowBox[{"{", 
                    RowBox[{
                    "R0", ",", "R1", ",", "R2", ",", "R3", ",", "R4", ",", 
                    "R5", ",", "R6", ",", "R7", ",", "R8", ",", "R9", ",", 
                    "R10", ",", "R11", ",", "R12"}], "}"}], ",", 
                    "\[IndentingNewLine]", 
                    RowBox[{"bf", "/@", 
                    RowBox[{"(", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"If", "[", 
                    RowBox[{
                    RowBox[{"format", "\[Equal]", "\"\<signed\>\""}], ",", 
                    RowBox[{"from2sComplement", "[", 
                    RowBox[{"#", ",", "$wordLength"}], "]"}], ",", "#"}], 
                    "]"}], "&"}], "/@", 
                    RowBox[{"registerList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ")"}]}], ",", 
                    "\[IndentingNewLine]", 
                    RowBox[{
                    RowBox[{
                    RowBox[{"If", "[", 
                    RowBox[{
                    RowBox[{"format", "\[Equal]", "\"\<signed\>\""}], ",", 
                    RowBox[{"from2sComplement", "[", 
                    RowBox[{"#", ",", "$wordLength"}], "]"}], ",", "#"}], 
                    "]"}], "&"}], "/@", 
                    RowBox[{"registerList", "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}]}], "}"}]}], ",", 
                    RowBox[{"Alignment", "\[Rule]", "Right"}]}], "]"}]}], 
                    "}"}]}], "}"}], "]"}]}], "\[IndentingNewLine]", "}"}], 
                  "]"}]}]}], "}"}], ",", 
              RowBox[{"Dynamic", "[", "tab", "]"}]}], "\[IndentingNewLine]", 
             "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"offset", ",", "50", ",", "\"\<memory start\>\""}], "}"}],
            ",", "0", ",", "512", ",", "1"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"lim", ",", "limit", ",", "\"\<computation limit\>\""}], 
            "}"}], ",", "1", ",", "10000", ",", "10"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"i", ",", "1", ",", "\"\<computation step\>\""}], "}"}], 
           ",", "1", ",", 
           RowBox[{"Dynamic", "@", "len"}], ",", "1", ",", 
           RowBox[{"Appearance", "\[Rule]", "\"\<Open\>\""}]}], "}"}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"format", ",", "\"\<signed\>\"", ",", "\"\<format\>\""}], 
            "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"\"\<signed\>\"", ",", "\"\<2's complement\>\""}], 
            "}"}]}], "}"}], ",", "\[IndentingNewLine]", 
         RowBox[{"Initialization", "\[RuleDelayed]", "\[IndentingNewLine]", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"code", "=", "program"}], ";", 
            RowBox[{"codeRun", "=", 
             RowBox[{"asLoadRunDecoded", "[", 
              RowBox[{"code", ",", "start", ",", "lim", ",", "wl"}], "]"}]}], 
            ";", 
            RowBox[{"len", "=", 
             RowBox[{"Length", "[", "codeRun", "]"}]}], ";", 
            RowBox[{
             RowBox[{"{", 
              RowBox[{
              "memories", ",", "pcList", ",", "statusList", ",", 
               "registerList"}], "}"}], "=", 
             RowBox[{"Transpose", "[", "codeRun", "]"}]}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"memoryRules", "=", 
             RowBox[{
              RowBox[{
               RowBox[{"Drop", "[", 
                RowBox[{
                 RowBox[{"ArrayRules", "[", "#", "]"}], ",", 
                 RowBox[{"-", "1"}]}], "]"}], "&"}], "/@", "memories"}]}], 
            " ", ";", "\[IndentingNewLine]", 
            RowBox[{"memoryRules", "=", 
             RowBox[{
              RowBox[{
               RowBox[{"Map", "[", 
                RowBox[{
                 RowBox[{
                  RowBox[{
                   RowBox[{"#", "[", 
                    RowBox[{"[", 
                    RowBox[{"1", ",", "1"}], "]"}], "]"}], "\[Rule]", 
                   RowBox[{"asPrint", "[", 
                    RowBox[{
                    RowBox[{"#1", "[", 
                    RowBox[{"[", "2", "]"}], "]"}], ",", "format"}], "]"}]}], 
                  "&"}], ",", "#"}], "]"}], "&"}], "/@", "memoryRules"}]}], 
            ";", "\[IndentingNewLine]", 
            RowBox[{"memoryRules", "=", 
             RowBox[{"Map", "[", 
              RowBox[{
               RowBox[{
                RowBox[{"SortBy", "[", 
                 RowBox[{"#", ",", 
                  RowBox[{
                   RowBox[{"#", "[", 
                    RowBox[{"[", "1", "]"}], "]"}], "&"}]}], "]"}], "&"}], 
               ",", "memoryRules"}], "]"}]}], ";", "\[IndentingNewLine]", 
            RowBox[{"keys", "=", 
             RowBox[{
              RowBox[{
               RowBox[{"Map", "[", 
                RowBox[{
                 RowBox[{
                  RowBox[{"#", "[", 
                   RowBox[{"[", "1", "]"}], "]"}], "&"}], ",", "#"}], "]"}], 
               "&"}], "/@", "memoryRules"}]}], ";"}], ")"}]}]}], "]"}]}], 
      "}"}], "]"}]}], "]"}]}]}], "Input",
 InitializationCell->True]
}, Closed]]
}, Closed]]
}, Closed]],

Cell[CellGroupData[{

Cell["Running", "Chapter"],

Cell[CellGroupData[{

Cell["Input and output of code", "Section"],

Cell[CellGroupData[{

Cell["Interface to loading from file", "Subsection"],

Cell["Use the interface below to load your code and assemble it", "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"DynamicModule", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"file", "=", "\"\<File not yet chosen\>\""}], ",", "code", ",", 
     RowBox[{"codeLoaded", "=", "False"}], ",", "interface", ",", 
     RowBox[{"interfaceLoaded", "=", "False"}]}], "}"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"Column", "[", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Row", "@", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"Item", "[", 
          RowBox[{"FileNameSetter", "[", 
           RowBox[{"Dynamic", "[", "file", "]"}], "]"}], "]"}], ",", 
         RowBox[{"Spacer", "@", "10"}], ",", 
         RowBox[{"Panel", "[", 
          RowBox[{"Dynamic", "[", "file", "]"}], "]"}]}], "}"}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"Row", "@", 
       RowBox[{"{", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"Button", "[", 
          RowBox[{
          "\"\<Import assembly language program\>\"", ",", 
           "\[IndentingNewLine]", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"code", "=", 
              RowBox[{"Import", "[", 
               RowBox[{"file", ",", "\"\<Text\>\""}], "]"}]}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"codeLoaded", "=", "True"}], ";", 
             RowBox[{"interfaceLoaded", "=", "False"}]}], ")"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"Method", "\[Rule]", "\"\<Queued\>\""}]}], "]"}], ",", 
         RowBox[{"Spacer", "@", "10"}], ",", "\[IndentingNewLine]", 
         RowBox[{"Button", "[", 
          RowBox[{"\"\<Run program\>\"", ",", "\[IndentingNewLine]", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"If", "[", 
              RowBox[{"codeLoaded", ",", "None", ",", 
               RowBox[{
                RowBox[{"code", "=", 
                 RowBox[{"Import", "[", 
                  RowBox[{"file", ",", "\"\<Text\>\""}], "]"}]}], ";", 
                "\[IndentingNewLine]", 
                RowBox[{"codeLoaded", "=", "True"}]}]}], "]"}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"interface", "=", 
              RowBox[{"runMyProgram", "[", "\[IndentingNewLine]", 
               RowBox[{"code", ",", "  ", 
                RowBox[{"(*", " ", 
                 RowBox[{
                 "the", " ", "code", " ", "you", " ", "are", " ", "running"}],
                  " ", "*)"}], "\[IndentingNewLine]", "60", "           ", 
                RowBox[{"(*", " ", 
                 RowBox[{"memory", " ", "start", " ", "address"}], " ", 
                 "*)"}], ",", "\[IndentingNewLine]", "100"}], "         ", 
               RowBox[{"(*", " ", 
                RowBox[{
                "maximum", " ", "number", " ", "of", " ", "program", " ", 
                 "steps"}], " ", "*)"}], "]"}]}], ";", "\[IndentingNewLine]", 
             RowBox[{"interfaceLoaded", "=", "True"}]}], ")"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"Method", "\[Rule]", "\"\<Queued\>\""}]}], "]"}]}], 
        "}"}]}], ",", "\[IndentingNewLine]", 
      RowBox[{"Dynamic", "@", "\[IndentingNewLine]", 
       RowBox[{"If", "[", 
        RowBox[{
        "interfaceLoaded", ",", "interface", ",", "\[IndentingNewLine]", 
         RowBox[{"If", "[", 
          RowBox[{"codeLoaded", ",", "code", ",", 
           RowBox[{"Panel", "@", "\"\<Waiting for instructions...\>\""}]}], 
          "]"}]}], "]"}]}]}], "\[IndentingNewLine]", "}"}], "]"}], ",", 
   RowBox[{"SaveDefinitions", "\[Rule]", "True"}]}], "]"}]], "Input"],

Cell[BoxData[
 DynamicModuleBox[{$CellContext`file$$ = 
  "File not yet chosen", $CellContext`code$$, $CellContext`codeLoaded$$ = 
  False, $CellContext`interface$$, $CellContext`interfaceLoaded$$ = False}, 
  TagBox[GridBox[{
     {
      TemplateBox[{ItemBox[
         TemplateBox[{
           Dynamic[$CellContext`file$$], "Open", All}, "FileNameSetterBoxes"],
          StripOnInput -> False],InterpretationBox[
         StyleBox[
          GraphicsBox[{}, ImageSize -> {10, 0}, BaselinePosition -> Baseline],
           "CacheGraphics" -> False], 
         Spacer[10]],PanelBox[
         DynamicBox[
          ToBoxes[$CellContext`file$$, StandardForm]]]},
       "RowDefault"]},
     {
      TemplateBox[{
       ButtonBox[
        "\"Import assembly language program\"", 
         ButtonFunction :> ($CellContext`code$$ = 
           Import[$CellContext`file$$, "Text"]; $CellContext`codeLoaded$$ = 
           True; $CellContext`interfaceLoaded$$ = False), Method -> "Queued", 
         Appearance -> Automatic, Evaluator -> Automatic],InterpretationBox[
         StyleBox[
          GraphicsBox[{}, ImageSize -> {10, 0}, BaselinePosition -> Baseline],
           "CacheGraphics" -> False], 
         Spacer[10]],ButtonBox[
        "\"Run program\"", 
         ButtonFunction :> (
          If[$CellContext`codeLoaded$$, 
            None, $CellContext`code$$ = 
             Import[$CellContext`file$$, "Text"]; $CellContext`codeLoaded$$ = 
             True]; $CellContext`interface$$ = \
$CellContext`runMyProgram[$CellContext`code$$, 60, 
             100]; $CellContext`interfaceLoaded$$ = True), Method -> "Queued",
          Appearance -> Automatic, Evaluator -> Automatic]},
       "RowDefault"]},
     {
      DynamicBox[ToBoxes[
        If[$CellContext`interfaceLoaded$$, $CellContext`interface$$, 
         If[$CellContext`codeLoaded$$, $CellContext`code$$, 
          Panel["Waiting for instructions..."]]], StandardForm],
       ImageSizeCache->{221., {23., 33.}}]}
    },
    DefaultBaseStyle->"Column",
    GridBoxAlignment->{"Columns" -> {{Left}}},
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
   "Column"],
  DynamicModuleValues:>{},
  Initialization:>{$CellContext`runMyProgram[
      Pattern[$CellContext`program$, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`start$, 
        Blank[Integer]], 50], 
      Optional[
       Pattern[$CellContext`limit$, 
        Blank[Integer]], 100], 
      Optional[
       Pattern[$CellContext`wl$, 
        Blank[]], 16]] := 
    DynamicModule[{$CellContext`tab, 
       FE`code$$37, $CellContext`codeRun, $CellContext`memoryRules, \
$CellContext`len, $CellContext`keys, $CellContext`pcList, \
$CellContext`statusList, $CellContext`registerList, $CellContext`memories, \
$CellContext`showCode = False}, 
      Column[{
        Panel[
         StringJoin["Assembler version ", 
          ToString[$CellContext`$assemblyVersion]]], 
        Panel[
         Dynamic[
          If[$CellContext`showCode, FE`code$$37, "Code hidden"]]], 
        Manipulate[
         Column[{
           Row[{
             Button[
             "Reload code", 
              FE`code$$37 = $CellContext`program$; $CellContext`codeRun = \
$CellContext`asLoadRunDecoded[
                FE`code$$37, $CellContext`offset, $CellContext`lim, \
$CellContext`wl$]; $CellContext`len = 
               Length[$CellContext`codeRun]; {$CellContext`memories, \
$CellContext`pcList, $CellContext`statusList, $CellContext`registerList} = 
               Transpose[$CellContext`codeRun]; $CellContext`memoryRules = 
               Map[Drop[
                  
                  ArrayRules[#], -1]& , $CellContext`memories]; \
$CellContext`memoryRules = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                    
                    Part[#, 
                    2], $CellContext`format]& , #]& , \
$CellContext`memoryRules]; $CellContext`memoryRules = 
               Map[SortBy[#, 
                  Part[#, 
                   1]& ]& , $CellContext`memoryRules]; $CellContext`keys = 
               Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules]; Null], 
             Button["Show code", $CellContext`showCode = True], 
             Button["Hide code", $CellContext`showCode = False]}], 
           TabView[{"base 10" -> Column[{
                Grid[{{
                   Text["Memory"], 
                   Text["PC"], 
                   Text["Status"], 
                   Text["CIR"]}, {
                   TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys, $CellContext`i], 
                    Normal[
                    Part[$CellContext`memoryRules, $CellContext`i]]]], 
                    Part[$CellContext`pcList, $CellContext`i], 
                    Appearance -> {"Limited", 10}], 
                   Text[
                    Part[$CellContext`pcList, $CellContext`i]], 
                   Text[
                    Part[$CellContext`statusList, $CellContext`i]], 
                   Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories, $CellContext`i], 
                    
                    Part[$CellContext`pcList, $CellContext`i]], \
$CellContext`format]]}}], 
                Grid[{{
                   Text["Registers"]}, {
                   Text[
                    
                    Grid[{{$CellContext`R0, $CellContext`R1, $CellContext`R2, \
$CellContext`R3, $CellContext`R4, $CellContext`R5, $CellContext`R6, \
$CellContext`R7, $CellContext`R8, $CellContext`R9, $CellContext`R10, \
$CellContext`R11, $CellContext`R12}, 
                    Map[If[$CellContext`format == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList, $CellContext`i]]}]]}}]}], 
             "base 2" -> Column[{
                Grid[{{
                   Text["Memory"], 
                   Text["PC"], 
                   Text["Status"], 
                   Text["CIR"]}, {
                   TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys, $CellContext`i], 
                    Normal[
                    Part[$CellContext`memoryRules, $CellContext`i]]]], 
                    Part[$CellContext`pcList, $CellContext`i], 
                    Appearance -> {"Limited", 10}], 
                   Text[
                    Part[$CellContext`pcList, $CellContext`i]], 
                   Text[
                    Part[$CellContext`statusList, $CellContext`i]], 
                   Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories, $CellContext`i], 
                    
                    Part[$CellContext`pcList, $CellContext`i]], \
$CellContext`format]]}}], 
                Grid[{{
                   Text["Registers"]}, {
                   Text[
                    Grid[
                    
                    Transpose[{{$CellContext`R0, $CellContext`R1, \
$CellContext`R2, $CellContext`R3, $CellContext`R4, $CellContext`R5, \
$CellContext`R6, $CellContext`R7, $CellContext`R8, $CellContext`R9, \
$CellContext`R10, $CellContext`R11, $CellContext`R12}, 
                    Map[$CellContext`bf, 
                    Map[If[$CellContext`format == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList, $CellContext`i]]], 
                    Map[If[$CellContext`format == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList, $CellContext`i]]}], 
                    Alignment -> Right]]}}]}]}, 
            Dynamic[$CellContext`tab]]}], {{$CellContext`offset, 50, 
           "memory start"}, 0, 512, 
          1}, {{$CellContext`lim, $CellContext`limit$, "computation limit"}, 
          1, 10000, 10}, {{$CellContext`i, 1, "computation step"}, 1, 
          Dynamic[$CellContext`len], 1, Appearance -> 
          "Open"}, {{$CellContext`format, "signed", "format"}, {
          "signed", "2's complement"}}, 
         Initialization :> (
          FE`code$$37 = $CellContext`program$; $CellContext`codeRun = \
$CellContext`asLoadRunDecoded[
            FE`code$$37, $CellContext`start$, $CellContext`lim, \
$CellContext`wl$]; $CellContext`len = 
           Length[$CellContext`codeRun]; {$CellContext`memories, \
$CellContext`pcList, $CellContext`statusList, $CellContext`registerList} = 
           Transpose[$CellContext`codeRun]; $CellContext`memoryRules = 
           Map[Drop[
              
              ArrayRules[#], -1]& , $CellContext`memories]; \
$CellContext`memoryRules = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                
                Part[#, 
                 2], $CellContext`format]& , #]& , $CellContext`memoryRules]; \
$CellContext`memoryRules = 
           Map[SortBy[#, 
              Part[#, 1]& ]& , $CellContext`memoryRules]; $CellContext`keys = 
           Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules]; Null)]}]], 
    Attributes[$CellContext`start$] = {Temporary}, 
    Attributes[$CellContext`wl$] = {Temporary}, FE`code$$37 = 
    "STR R11 , 0010 ; this is a comment and is terminated by: ;\nl2 : LDR R1 \
, 0012 ; this is another comment: note spaces! ;\nl1 : SUB R10 , R8 , R3\nl3 \
: MOV R7 , # -0019 ; not every line needs a comment ;\nl3a : MVN R8 , # 4096 \
; may create a negative number! ;\nl4 : MOV R5 , R2 ; ;\nl5 : CMP R2 , R3 ; \
empty comments (see above) must also have spaces ;\nl6 : BEQ l11\nl7 : ADD \
R12 , R10 , # 00066\nMOV R3 , R2\n\nl10 : B l2\nl11 : HALT", \
$CellContext`$assemblyVersion = 2.2, $CellContext`asLoadRunDecoded[
      Pattern[$CellContext`code, 
       Blank[String]], 
      Optional[
       Pattern[$CellContext`offset, 
        Blank[]], 0], 
      Optional[
       Pattern[$CellContext`maxSteps, 
        Blank[]], 100], 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[Integer]], 16]] := 
    With[{$CellContext`iniPc = 
       1 + $CellContext`offset, $CellContext`iniStatus = 
       "EQ", $CellContext`iniRegisters = 
       Range[0, 
         12], $CellContext`codeArray = \
$CellContext`asGrab[$CellContext`code]}, 
      With[{$CellContext`loadedCode = \
$CellContext`asLoad[$CellContext`codeArray, $CellContext`offset, \
$CellContext`wl], $CellContext`locs = \
$CellContext`asLocations[$CellContext`codeArray, $CellContext`offset]}, 
       With[{$CellContext`iniMem = 
         Map[$CellContext`asDecodeDebugging, $CellContext`loadedCode]}, 
        NestWhileList[
         $CellContext`asStep[$CellContext`locs], {$CellContext`iniMem, \
$CellContext`iniPc, $CellContext`iniStatus, $CellContext`iniRegisters}, 
         Part[#, 2] > 0& , 
         1, $CellContext`maxSteps]]]], $CellContext`codeArray = {
     "" -> "STR R11,0010;this is a comment and is terminated by;", "l2" -> 
      "LDR R1,0012 ;this is another comment:note lack of spaces!;", "l1" -> 
      "SUB R10, R8, R3", "l3" -> 
      "MOV R7,#-0019 ; not every line needs a comment ;", "l3a" -> 
      "MVN R8, #4096 ; may create a negative number! ;", "l4" -> 
      "MOV R5, R2;;", "l5" -> 
      "CMP R2, R3 ; empty comments (see above) need no longer have spaces ;", 
      "l6" -> "BEQ l11", "l7" -> "ADD R12, R10, #00066", "" -> "MOV R3, R2", 
      "l10" -> "B l2", "l11" -> "HALT"}, $CellContext`asGrab[
      Pattern[$CellContext`code, 
       Blank[String]]] := Map[Switch[#, {
        Blank[]}, "" -> Part[#, 1], {
        Blank[], 
        Blank[]}, Part[#, 1] -> Part[#, 2], {}, 
       (SlotSequence[1]& )[]]& , 
      Map[StringSplit[#, " : "]& , 
       StringSplit[$CellContext`code, "\n"]]], $CellContext`loadedCode = 
    SparseArray[
     Automatic, {2567}, 0, {
      1, {{0, 12}, {{2556}, {2557}, {2558}, {2559}, {2560}, {2561}, {2562}, {
        2563}, {2564}, {2565}, {2566}, {2567}}}, {
       "STR R11,0010;this is a comment and is terminated by;", 
        "LDR R1,0012 ;this is another comment:note lack of spaces!;", 
        "SUB R10, R8, R3", "MOV R7,#-0019 ; not every line needs a comment ;",
         "MVN R8, #4096 ; may create a negative number! ;", "MOV R5, R2;;", 
        "CMP R2, R3 ; empty comments (see above) need no longer have spaces \
;", "BEQ l11", "ADD R12, R10, #00066", "MOV R3, R2", "B l2", 
        "HALT"}}], $CellContext`asLoad[
      Pattern[$CellContext`p, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`offset, 
        Blank[]], 0], 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] := With[{$CellContext`len = Length[$CellContext`p]}, 
      SparseArray[
      Range[$CellContext`len] + $CellContext`offset -> 
       Map[Part[#, 2]& , $CellContext`p]]], $CellContext`locs = 
    Association[
     "" -> 2565, "l2" -> 2557, "l1" -> 2558, "l3" -> 2559, "l3a" -> 2560, 
      "l4" -> 2561, "l5" -> 2562, "l6" -> 2563, "l7" -> 2564, "l10" -> 2566, 
      "l11" -> 2567], $CellContext`asLocations[
      Pattern[$CellContext`p, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`offset, 
        Blank[]], 0]] := Association[
      MapIndexed[First[#] -> First[#2] + $CellContext`offset& , 
       Normal[$CellContext`p]]], $CellContext`iniMem = 
    "MOV R0 , # 91 ; data initialisation ;\nSTR R0 , 2\nMOV R0 , # 101\nSTR \
R0 , 3\nMOV R0 , # 23\nSTR R0 , 4\nMOV R0 , # 0\nMOV R1 , # 3 ; 3 elements in \
the array ;\nMOV R2 , # 2 ; 1st element is in location 2 ;\n", \
$CellContext`asDecodeDebugging[
      Pattern[$CellContext`p, 
       Blank[String]]] := Part[
      FunctionalParsers`ParseShortest[$CellContext`pINSTRUCTION][
       FunctionalParsers`ToTokens[$CellContext`p, {";", "#", ":", ","}]], 1, 
      2], $CellContext`asDecodeDebugging[
      Pattern[$CellContext`v, 
       Blank[]]] := $CellContext`v, FunctionalParsers`ParseShortest[
      Pattern[FunctionalParsers`Private`p, 
       Blank[]]] := 
    With[{FunctionalParsers`Private`parsed = FunctionalParsers`Private`p[#]}, 
      If[FunctionalParsers`Private`parsed === {}, 
       FunctionalParsers`Private`parsed, {
        First[
         SortBy[FunctionalParsers`Private`parsed, Length[
           Part[#, 1]]& ]]}]]& , 
    TagSet[FunctionalParsers`ParseShortest, 
     MessageName[FunctionalParsers`ParseShortest, "usage"], 
     "ParseShortest[p] takes the output with the shortests rest string."], \
$CellContext`pINSTRUCTION[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseApply[
      ToExpression["asmInstr"], 
      FunctionalParsers`ParseAlternativeComposition[
       FunctionalParsers`ParseSequentialComposition[If[
         And[Length[#] > 0, "OUT" === First[#]], {{
           Rest[#], "OUT"}}, {}]& , 
        FunctionalParsers`ParsePredicate[StringMatchQ[#, 
          Repeated[
           Alternatives[WordCharacter, "_"]]]& ]], 
       FunctionalParsers`ParseSequentialComposition[$CellContext`pOPCODE1, 
        FunctionalParsers`ParseSequentialComposition[
         FunctionalParsers`ParseSequentialCompositionPickLeft[$CellContext`\
pREG, If[
           And[Length[#] > 0, "," === First[#]], {{
             Rest[#], ","}}, {}]& ], $CellContext`pMEMREF]], 
       FunctionalParsers`ParseSequentialComposition[$CellContext`pOPCODE2, 
        FunctionalParsers`ParseSequentialComposition[
         FunctionalParsers`ParseSequentialCompositionPickLeft[$CellContext`\
pREG, If[
           And[Length[#] > 0, "," === First[#]], {{
             Rest[#], ","}}, {}]& ], 
         FunctionalParsers`ParseSequentialComposition[
          
          FunctionalParsers`ParseSequentialCompositionPickLeft[$CellContext`\
pREG, If[
            And[Length[#] > 0, "," === First[#]], {{
              Rest[#], ","}}, {}]& ], $CellContext`pOPERAND]]], 
       FunctionalParsers`ParseSequentialComposition[$CellContext`pOPCODE3, 
        FunctionalParsers`ParseSequentialComposition[
         FunctionalParsers`ParseSequentialCompositionPickLeft[$CellContext`\
pREG, If[
           And[Length[#] > 0, "," === First[#]], {{
             Rest[#], ","}}, {}]& ], $CellContext`pOPERAND]], 
       FunctionalParsers`ParseSequentialComposition[If[
         And[Length[#] > 0, "CMP" === First[#]], {{
           Rest[#], "CMP"}}, {}]& , 
        FunctionalParsers`ParseSequentialComposition[
         FunctionalParsers`ParseSequentialCompositionPickLeft[$CellContext`\
pREG, If[
           And[Length[#] > 0, "," === First[#]], {{
             Rest[#], ","}}, {}]& ], $CellContext`pOPERAND]], 
       FunctionalParsers`ParseSequentialComposition[$CellContext`pBCOND, \
$CellContext`pLABEL], If[
        And[Length[#] > 0, "HALT" === First[#]], {{
          Rest[#], "HALT"}}, {}]& ]][FunctionalParsers`Private`xs$], 
    Attributes[FunctionalParsers`Private`xs$] = {Temporary}, 
    FunctionalParsers`ParseApply[
      Pattern[FunctionalParsers`Private`f, 
       Blank[]], 
      Pattern[FunctionalParsers`Private`p, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := Map[{
       Part[#, 1], 
       FunctionalParsers`Private`f[
        Part[#, 2]]}& , 
      FunctionalParsers`Private`p[FunctionalParsers`Private`xs]], 
    FunctionalParsers`ParseApply[{
       Pattern[FunctionalParsers`Private`fNo, 
        Blank[]], 
       Pattern[FunctionalParsers`Private`fYes, 
        Blank[]]}, 
      Pattern[FunctionalParsers`Private`p, 
       Blank[]]] := 
    With[{FunctionalParsers`Private`res = FunctionalParsers`Private`p[#]}, 
      Map[{
        Part[#, 1], 
        If[Part[#, 2] === {}, FunctionalParsers`Private`fNo, 
         FunctionalParsers`Private`fYes[
          Part[#, 2]]]}& , FunctionalParsers`Private`res]]& , 
    TagSet[FunctionalParsers`ParseApply, 
     MessageName[FunctionalParsers`ParseApply, "usage"], 
     "ParseApply[f,p] applies the function f to the output of p. \
ParseApply[{fNo, fYes}, p] applies the function fNo not unsuccessful parsing \
and the function fYes the output of successful parsing using p."], 
    FunctionalParsers`ParseAlternativeComposition[
      Pattern[FunctionalParsers`Private`args, 
       BlankSequence[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := Apply[Join, 
      Map[#[FunctionalParsers`Private`xs]& , {
       FunctionalParsers`Private`args}]], 
    TagSet[FunctionalParsers`ParseAlternativeComposition, 
     MessageName[FunctionalParsers`ParseAlternativeComposition, "usage"], 
     "ParseAlternativeComposition parses a composition of two or more \
alternative parsers."], FunctionalParsers`ParseSequentialComposition[
      Pattern[FunctionalParsers`Private`p1, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := 
    FunctionalParsers`Private`p1[FunctionalParsers`Private`xs], 
    FunctionalParsers`ParseSequentialComposition[
      Pattern[FunctionalParsers`Private`args, 
       BlankSequence[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := Condition[
      With[{
       FunctionalParsers`Private`parsers = {FunctionalParsers`Private`args}}, 
       Fold[FunctionalParsers`Private`ParseComposeWithResults[#2][#]& , 
        First[FunctionalParsers`Private`parsers][
        FunctionalParsers`Private`xs], 
        Rest[FunctionalParsers`Private`parsers]]], 
      Length[{FunctionalParsers`Private`args}] > 1], 
    TagSet[FunctionalParsers`ParseSequentialComposition, 
     MessageName[FunctionalParsers`ParseSequentialComposition, "usage"], 
     "ParseSequentialComposition parses a sequential composition of two or \
more parsers."], FunctionalParsers`Private`ParseComposeWithResults[
      Pattern[FunctionalParsers`Private`p, 
       Blank[]]][{}] := {}, FunctionalParsers`Private`ParseComposeWithResults[
      Pattern[FunctionalParsers`Private`p, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`res, {
        Repeated[{
          Blank[], 
          Blank[]}]}]] := Block[{FunctionalParsers`Private`t}, 
      (Flatten[#, 1]& )[
       Map[
        Function[{FunctionalParsers`Private`r}, 
         If[
         FunctionalParsers`Private`r === {}, {}, 
          FunctionalParsers`Private`t = FunctionalParsers`Private`p[
             Part[FunctionalParsers`Private`r, 1]]; 
          If[FunctionalParsers`Private`t === {}, {}, 
            Map[{
              Part[#, 1], {
               Part[FunctionalParsers`Private`r, 2], 
               Part[#, 2]}}& , FunctionalParsers`Private`t]]]], 
        FunctionalParsers`Private`res]]], FunctionalParsers`ParsePredicate[
      Pattern[FunctionalParsers`Private`pred, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := If[
      TrueQ[
       And[Length[FunctionalParsers`Private`xs] > 0, 
        FunctionalParsers`Private`pred[
         First[FunctionalParsers`Private`xs]]]], {{
        Rest[FunctionalParsers`Private`xs], 
        First[FunctionalParsers`Private`xs]}}, {}], 
    TagSet[FunctionalParsers`ParsePredicate, 
     MessageName[FunctionalParsers`ParsePredicate, "usage"], 
     "ParsePredicate[p] parses strings that give True for the predicate p."], \
$CellContext`pOPCODE1[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseAlternativeComposition[If[
       And[Length[#] > 0, "LDR" === First[#]], {{
         Rest[#], "LDR"}}, {}]& , If[
       And[Length[#] > 0, "STR" === First[#]], {{
         Rest[#], "STR"}}, {}]& ][FunctionalParsers`Private`xs$], 
    FunctionalParsers`ParseSequentialCompositionPickLeft[
      Pattern[FunctionalParsers`Private`p1, 
       Blank[]], 
      Pattern[FunctionalParsers`Private`p2, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := FunctionalParsers`ParseApply[Part[#, 1]& , 
      FunctionalParsers`ParseSequentialComposition[
      FunctionalParsers`Private`p1, FunctionalParsers`Private`p2]][
     FunctionalParsers`Private`xs], 
    TagSet[FunctionalParsers`ParseSequentialCompositionPickLeft, 
     MessageName[
     FunctionalParsers`ParseSequentialCompositionPickLeft, "usage"], 
     "ParseSequentialCompositionPickLeft[p1,p2] drops the output of the p2 \
parser."], $CellContext`pREG[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseApply[
      ToExpression["asmRegProcess"], 
      FunctionalParsers`ParseAlternativeComposition[If[
        And[Length[#] > 0, "R0" === First[#]], {{
          Rest[#], "R0"}}, {}]& , If[
        And[Length[#] > 0, "R1" === First[#]], {{
          Rest[#], "R1"}}, {}]& , If[
        And[Length[#] > 0, "R2" === First[#]], {{
          Rest[#], "R2"}}, {}]& , If[
        And[Length[#] > 0, "R3" === First[#]], {{
          Rest[#], "R3"}}, {}]& , If[
        And[Length[#] > 0, "R4" === First[#]], {{
          Rest[#], "R4"}}, {}]& , If[
        And[Length[#] > 0, "R5" === First[#]], {{
          Rest[#], "R5"}}, {}]& , If[
        And[Length[#] > 0, "R6" === First[#]], {{
          Rest[#], "R6"}}, {}]& , If[
        And[Length[#] > 0, "R7" === First[#]], {{
          Rest[#], "R7"}}, {}]& , If[
        And[Length[#] > 0, "R8" === First[#]], {{
          Rest[#], "R8"}}, {}]& , If[
        And[Length[#] > 0, "R9" === First[#]], {{
          Rest[#], "R9"}}, {}]& , If[
        And[Length[#] > 0, "R10" === First[#]], {{
          Rest[#], "R10"}}, {}]& , If[
        And[Length[#] > 0, "R11" === First[#]], {{
          Rest[#], "R11"}}, {}]& , If[
        And[Length[#] > 0, "R12" === First[#]], {{
          Rest[#], "R12"}}, {}]& ]][
     FunctionalParsers`Private`xs$], $CellContext`pMEMREF[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseApply[
      ToExpression["asmMemRef"], 
      FunctionalParsers`ParseAlternativeComposition[
       FunctionalParsers`ParseApply[ToExpression, 
        FunctionalParsers`ParsePredicate[And[
          StringMatchQ[#, NumberString], 0 <= ToExpression[#] <= 
          65535]& ]], $CellContext`pREG]][
     FunctionalParsers`Private`xs$], $CellContext`pOPCODE2[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseAlternativeComposition[If[
       And[Length[#] > 0, "ADD" === First[#]], {{
         Rest[#], "ADD"}}, {}]& , If[
       And[Length[#] > 0, "SUB" === First[#]], {{
         Rest[#], "SUB"}}, {}]& , If[
       And[Length[#] > 0, "AND" === First[#]], {{
         Rest[#], "AND"}}, {}]& , If[
       And[Length[#] > 0, "ORR" === First[#]], {{
         Rest[#], "ORR"}}, {}]& , If[
       And[Length[#] > 0, "EOR" === First[#]], {{
         Rest[#], "EOR"}}, {}]& , If[
       And[Length[#] > 0, "LSL" === First[#]], {{
         Rest[#], "LSL"}}, {}]& , If[
       And[Length[#] > 0, "LSR" === First[#]], {{
         Rest[#], "LSR"}}, {}]& ][
     FunctionalParsers`Private`xs$], $CellContext`pOPERAND[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseApply[
      ToExpression["asmOperand"], 
      FunctionalParsers`ParseAlternativeComposition[$CellContext`pLITERAL, \
$CellContext`pREG]][FunctionalParsers`Private`xs$], $CellContext`pLITERAL[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseApply[
      ToExpression["asmLiteralProcess"], 
      FunctionalParsers`ParseSequentialCompositionPickRight[If[
        And[Length[#] > 0, "#" === First[#]], {{
          Rest[#], "#"}}, {}]& , 
       FunctionalParsers`ParseApply[ToExpression, 
        FunctionalParsers`ParsePredicate[And[
          StringMatchQ[#, NumberString], -4294967296 <= ToExpression[#] <= 
          4294967295]& ]]]][FunctionalParsers`Private`xs$], 
    FunctionalParsers`ParseSequentialCompositionPickRight[
      Pattern[FunctionalParsers`Private`p1, 
       Blank[]], 
      Pattern[FunctionalParsers`Private`p2, 
       Blank[]]][
      Pattern[FunctionalParsers`Private`xs, 
       Blank[]]] := FunctionalParsers`ParseApply[Part[#, 2]& , 
      FunctionalParsers`ParseSequentialComposition[
      FunctionalParsers`Private`p1, FunctionalParsers`Private`p2]][
     FunctionalParsers`Private`xs], 
    TagSet[FunctionalParsers`ParseSequentialCompositionPickRight, 
     MessageName[
     FunctionalParsers`ParseSequentialCompositionPickRight, "usage"], 
     "ParseSequentialCompositionPickRight[p1,p2] drops the output of the p1 \
parser."], $CellContext`pOPCODE3[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseAlternativeComposition[If[
       And[Length[#] > 0, "MOV" === First[#]], {{
         Rest[#], "MOV"}}, {}]& , If[
       And[Length[#] > 0, "CMP" === First[#]], {{
         Rest[#], "CMP"}}, {}]& , If[
       And[Length[#] > 0, "MVN" === First[#]], {{
         Rest[#], "MVN"}}, {}]& ][
     FunctionalParsers`Private`xs$], $CellContext`pBCOND[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParseAlternativeComposition[If[
       And[Length[#] > 0, "B" === First[#]], {{
         Rest[#], "B"}}, {}]& , If[
       And[Length[#] > 0, "BEQ" === First[#]], {{
         Rest[#], "BEQ"}}, {}]& , If[
       And[Length[#] > 0, "BNE" === First[#]], {{
         Rest[#], "BNE"}}, {}]& , If[
       And[Length[#] > 0, "BGT" === First[#]], {{
         Rest[#], "BGT"}}, {}]& , If[
       And[Length[#] > 0, "BLT" === First[#]], {{
         Rest[#], "BLT"}}, {}]& ][
     FunctionalParsers`Private`xs$], $CellContext`pLABEL[
      Pattern[FunctionalParsers`Private`xs$, 
       Blank[]]] := FunctionalParsers`ParsePredicate[StringMatchQ[#, 
       Repeated[
        Alternatives[WordCharacter, "_"]]]& ][FunctionalParsers`Private`xs$], 
    FunctionalParsers`ToTokens[
      Pattern[FunctionalParsers`Private`text, 
       Blank[String]]] := StringSplit[FunctionalParsers`Private`text], 
    FunctionalParsers`ToTokens[
      Pattern[FunctionalParsers`Private`text, 
       Blank[String]], {}] := StringSplit[FunctionalParsers`Private`text], 
    FunctionalParsers`ToTokens[
      Pattern[FunctionalParsers`Private`text, 
       Blank[String]], 
      Pattern[FunctionalParsers`Private`terminals, {
        RepeatedNull[
         Blank[String]]}]] := StringSplit[
      StringReplace[FunctionalParsers`Private`text, 
       Map[# -> StringJoin[" ", #, " "]& , 
        FunctionalParsers`Private`terminals]]], FunctionalParsers`ToTokens[
      Pattern[FunctionalParsers`Private`text, 
       Blank[]], "EBNF"] := 
    FunctionalParsers`ToTokens[
     FunctionalParsers`Private`text, {
      "|", ",", ";", "=", "[", "]", "(", ")", "{", "}"}], 
    TagSet[FunctionalParsers`ToTokens, 
     MessageName[FunctionalParsers`ToTokens, "usage"], 
     "ToTokens[text] breaks down text into tokens. ToTokens[text,terminals] \
breaks down text using specified terminals. ToTokens[text,\"EBNF\"] has a \
special implementation for parsing EBNF code. (This function is becoming \
obsolete, use ParseToTokens.)"], $CellContext`asStep[
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := $CellContext`asExecute[
      $CellContext`asDecodeDebugging[
       $CellContext`asFetch[$CellContext`mem, $CellContext`pc]], \
$CellContext`labels][{$CellContext`mem, $CellContext`pc, $CellContext`status, \
$CellContext`registers}], $CellContext`asExecute[
      Pattern[$CellContext`instr, 
       Blank[]], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := $CellContext`asInterpretStep[$CellContext`instr, \
$CellContext`labels][{$CellContext`mem, $CellContext`pc, $CellContext`status, \
$CellContext`registers}], $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[
           Pattern[$CellContext`m, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {
      ReplacePart[$CellContext`mem, 
       Part[$CellContext`registers, $CellContext`m + 1] -> 
       Part[$CellContext`registers, $CellContext`r + 1]], $CellContext`pc + 
      1, $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          Pattern[$CellContext`m, 
           Blank[]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {
      ReplacePart[$CellContext`mem, $CellContext`m -> 
       Part[$CellContext`registers, $CellContext`r + 1]], $CellContext`pc + 
      1, $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[
           Pattern[$CellContext`m, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 
      1, $CellContext`status, 
      ReplacePart[$CellContext`registers, $CellContext`r + 1 -> 
       Part[$CellContext`mem, 
         Part[$CellContext`registers, $CellContext`m + 
          1]]]}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          Pattern[$CellContext`m, 
           Blank[]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 
      1, $CellContext`status, 
      ReplacePart[$CellContext`registers, $CellContext`r + 1 -> 
       Part[$CellContext`mem, $CellContext`m]]}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{
        Pattern[$CellContext`op, 
         Alternatives["MOV", "MVN"]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[
           Pattern[$CellContext`l, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 
      1, $CellContext`status, 
      ReplacePart[$CellContext`registers, $CellContext`r + 
        1 -> $CellContext`operation[$CellContext`op, \
$CellContext`$wordLength][$CellContext`l]]}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{
        Pattern[$CellContext`op, 
         Alternatives["MOV", "MVN"]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 
      1, $CellContext`status, 
      ReplacePart[$CellContext`registers, $CellContext`r1 + 
        1 -> $CellContext`operation[$CellContext`op, $CellContext`$wordLength][
         Part[$CellContext`registers, $CellContext`r2 + 
          1]]]}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 1, 
      $CellContext`compare[
       Part[$CellContext`registers, $CellContext`r1 + 1], 
       Part[$CellContext`registers, $CellContext`r2 + 
        1]], $CellContext`registers}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[
           Pattern[$CellContext`l, 
            Blank[]]]]}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 1, 
      $CellContext`compare[
       Part[$CellContext`registers, $CellContext`r + 
        1], $CellContext`l], $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{
        Pattern[$CellContext`op, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], {
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[
            Pattern[$CellContext`l, 
             Blank[]]]]}}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := 
    With[{$CellContext`f = $CellContext`operation[$CellContext`op, \
$CellContext`$wordLength]}, {$CellContext`mem, $CellContext`pc + 
       1, $CellContext`status, 
       ReplacePart[$CellContext`registers, $CellContext`r1 + 
         1 -> $CellContext`f[
          
          Part[$CellContext`registers, $CellContext`r2 + 
           1], $CellContext`l]]}], $CellContext`asInterpretStep[
      $CellContext`asmInstr[{
        Pattern[$CellContext`op, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], {
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]], 
          $CellContext`asmOperand[
           $CellContext`asmReg[
            Pattern[$CellContext`r3, 
             Blank[]]]]}}}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := 
    With[{$CellContext`f = $CellContext`operation[$CellContext`op, \
$CellContext`$wordLength]}, {$CellContext`mem, $CellContext`pc + 
       1, $CellContext`status, 
       ReplacePart[$CellContext`registers, $CellContext`r1 + 
         1 -> $CellContext`f[
          Part[$CellContext`registers, $CellContext`r2 + 1], 
          
          Part[$CellContext`registers, $CellContext`r3 + 
           1]]]}], $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"B", 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      $CellContext`labels[$CellContext`l], $CellContext`status, \
$CellContext`registers}, $CellContext`asInterpretStep[
      $CellContext`asmInstr[{"BEQ", 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      If[$CellContext`status == "EQ", 
       $CellContext`labels[$CellContext`l], $CellContext`pc + 
       1], $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"BNE", 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      If[$CellContext`status != "EQ", 
       $CellContext`labels[$CellContext`l], $CellContext`pc + 
       1], $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"BLT", 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      If[$CellContext`status == "LT", 
       $CellContext`labels[$CellContext`l], $CellContext`pc + 
       1], $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"BGT", 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      If[$CellContext`status == "GT", 
       $CellContext`labels[$CellContext`l], $CellContext`pc + 
       1], $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      $CellContext`asmInstr[{"OUT", 
        Pattern[$CellContext`msg, 
         Blank[]]}], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := (MessageDialog[
       StringJoin["PC: ", 
        ToString[$CellContext`pc], 
        "\n", $CellContext`msg]]; {$CellContext`mem, $CellContext`pc + 
       1, $CellContext`status, $CellContext`registers}), \
$CellContext`asInterpretStep[
      $CellContext`asmInstr["HALT"], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, 
      0, $CellContext`status, $CellContext`registers}, \
$CellContext`asInterpretStep[
      Pattern[$CellContext`instr, 
       Blank[]], 
      Pattern[$CellContext`labels, 
       Blank[]]][{
       Pattern[$CellContext`mem, 
        Blank[]], 
       Pattern[$CellContext`pc, 
        Blank[]], 
       Pattern[$CellContext`status, 
        Blank[]], 
       Pattern[$CellContext`registers, 
        Blank[]]}] := {$CellContext`mem, $CellContext`pc + 
      1, $CellContext`status, $CellContext`registers}, \
$CellContext`asmInstr[] := $CellContext`asmInstr, $CellContext`asmReg[] := \
$CellContext`asmReg, $CellContext`asmMemRef[] := $CellContext`asmMemRef, \
$CellContext`asmOperand[] := $CellContext`asmOperand, \
$CellContext`asmLiteral[] := $CellContext`asmLiteral, $CellContext`operation[
     "ADD", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 
       16]] := $CellContext`binChop[# + #2, $CellContext`wl]& , \
$CellContext`operation["SUB", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 
       16]] := $CellContext`binChop[# + ($CellContext`binNeg[#2, \
$CellContext`wl] + 1), $CellContext`wl]& , $CellContext`operation["AND", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] = BitAnd, $CellContext`operation["ORR", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] = BitOr, $CellContext`operation["EOR", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] = BitXor, $CellContext`operation["LSL", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] := $CellContext`binChop[
      BitShiftLeft[#], $CellContext`wl]& , $CellContext`operation["LSR", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] = BitShiftRight, $CellContext`operation["MOV", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 16]] = Identity, $CellContext`operation["MVN", 
      Optional[
       Pattern[$CellContext`wl, 
        Blank[]], 
       16]] := $CellContext`binNeg[#, $CellContext`wl]& , $CellContext`binChop[
      Pattern[$CellContext`x, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`numBits, 
        Blank[]], 16]] := 
    Mod[$CellContext`x, 2^$CellContext`numBits], $CellContext`binNeg[
      Pattern[$CellContext`x, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`numBits, 
        Blank[]], 16]] := 
    BitXor[$CellContext`x, 2^$CellContext`numBits - 
      1], $CellContext`$wordLength = 16, $CellContext`compare[
      Pattern[$CellContext`x, 
       Blank[]], 
      Pattern[$CellContext`y, 
       Blank[]]] := 
    Which[$CellContext`x == $CellContext`y, 
      "EQ", $CellContext`x < $CellContext`y, 
      "LT", $CellContext`x > $CellContext`y, "GT", True, 
      "Fail"], $CellContext`asFetch[
      Pattern[$CellContext`mem, 
       Blank[]], 
      Pattern[$CellContext`pc, 
       Blank[]]] := 
    Part[$CellContext`mem, $CellContext`pc], $CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[
           Pattern[$CellContext`m, 
            Blank[]]]]}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r], " , R", 
      ToString[$CellContext`m]], $CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmMemRef[
          Pattern[$CellContext`m, 
           Blank[]]]}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r], " , ", 
      ToString[$CellContext`m]], $CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[
           Pattern[$CellContext`l, 
            Blank[]]]]}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r], " , # ", 
      ToString[
       If[$CellContext`format == "signed", 
        $CellContext`from2sComplement[$CellContext`l], $CellContext`l]]], \
$CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], 
         $CellContext`asmOperand[
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]]]}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r1], " , R", 
      ToString[$CellContext`r2]], $CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], {
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[
            Pattern[$CellContext`l, 
             Blank[]]]]}}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r1], " , R", 
      ToString[$CellContext`r2], " , # ", 
      ToString[
       If[$CellContext`format == "signed", 
        $CellContext`from2sComplement[$CellContext`l], $CellContext`l]]], \
$CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`opString, 
         Blank[]], {
         $CellContext`asmReg[
          Pattern[$CellContext`r1, 
           Blank[]]], {
          $CellContext`asmReg[
           Pattern[$CellContext`r2, 
            Blank[]]], 
          $CellContext`asmOperand[
           $CellContext`asmReg[
            Pattern[$CellContext`r3, 
             Blank[]]]]}}}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`opString, " R", 
      ToString[$CellContext`r1], " , R", 
      ToString[$CellContext`r2], " , R", 
      ToString[$CellContext`r3]], $CellContext`asPrint[
      $CellContext`asmInstr[{
        Pattern[$CellContext`branchString, 
         Blank[]], 
        Pattern[$CellContext`l, 
         Blank[]]}], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := StringJoin[$CellContext`branchString, " ", 
      ToString[$CellContext`l]], $CellContext`asPrint[
      $CellContext`asmInstr["HALT"], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := "HALT", $CellContext`asPrint[
      Pattern[$CellContext`instr, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`format, 
        Blank[]], "signed"]] := ToString[$CellContext`instr], 
    Attributes[$CellContext`from2sComplement] = {Listable}, Condition[
      $CellContext`from2sComplement[
       Pattern[$CellContext`n, 
        Blank[Integer]], 
       Optional[
        Pattern[$CellContext`numBits, 
         Blank[Integer]], 16]], 
      $CellContext`fits[$CellContext`n, $CellContext`numBits - 
       1]] := $CellContext`n, Condition[
      $CellContext`from2sComplement[
       Pattern[$CellContext`n, 
        Blank[Integer]], 
       Optional[
        Pattern[$CellContext`numBits, 
         Blank[Integer]], 16]], 
      $CellContext`fits[$CellContext`n, $CellContext`numBits]] := \
-$CellContext`binNeg[$CellContext`n, $CellContext`numBits] - 
     1, $CellContext`from2sComplement[
      Pattern[$CellContext`n, 
       Blank[Integer]], 
      Optional[
       Pattern[$CellContext`numBits, 
        Blank[Integer]], 16]] := $CellContext`from2sComplement[
      $CellContext`binChop[$CellContext`n, $CellContext`numBits], \
$CellContext`numBits], $CellContext`fits[
      Pattern[$CellContext`n, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`numBits, 
        Blank[]], 16]] := 
    BitLength[$CellContext`n] <= $CellContext`numBits, $CellContext`i = 
    1, $CellContext`bf[
      Pattern[$CellContext`x, 
       Blank[]], 
      Optional[
       Pattern[$CellContext`b, 
        Blank[]], 2]] := BaseForm[$CellContext`x, $CellContext`b]}]], "Output"]
}, {2}]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Code examples (inline)", "Subsection"],

Cell[CellGroupData[{

Cell["Example 1", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "code1", "=", "\[IndentingNewLine]", 
   "\"\<STR R11, 0010 ;this is a comment and is terminated by: ;\nl2 : LDR \
R1, 0012 ;this is another comment;\nl1 : SUB R10, R8, R3\nl3 : MOV R7, #-0019 \
; not every line needs a comment ;\nl3a : MVN R8, # 4096 ; may create a \
negative number! ;\nl4 : MOV R5, R2 ;;\nl5 : CMP R2, R3 ; empty comments (see \
above) need not have spaces ;\nl6 : BEQ l11\nl7 : ADD R12, R10, #00066\nMOV \
R3, R2\n\nl10 : B l2\nl11 : HALT\>\""}], ";"}]], "Input",
 InitializationCell->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["Run the code for example 1", "Subsubsection"],

Cell[TextData[{
 "Use the ",
 StyleBox["runMyProgram", "Input"],
 " function to execute your program and inspect the results. Currently the \
registers hold the value of their own register value as this aids testing. If \
you want to reset all registers to zero, try adding assembler code to do this \
before the main body of your code."
}], "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"runMyProgram", "[", "\[IndentingNewLine]", 
  RowBox[{"code1", ",", " ", 
   RowBox[{"(*", " ", 
    RowBox[{"the", " ", "code", " ", "you", " ", "are", " ", "running"}], " ",
     "*)"}], "\[IndentingNewLine]", "60", "           ", 
   RowBox[{"(*", " ", 
    RowBox[{"memory", " ", "start", " ", "address"}], " ", "*)"}], ",", 
   "\[IndentingNewLine]", "100"}], "         ", 
  RowBox[{"(*", " ", 
   RowBox[{
   "maximum", " ", "number", " ", "of", " ", "program", " ", "steps"}], " ", 
   "*)"}], "]"}]], "Code"],

Cell[BoxData[
 DynamicModuleBox[{$CellContext`tab$$ = 1, $CellContext`code$$ = 
  "STR R11, 0010 ;this is a comment and is terminated by: ;\nl2 : LDR R1, \
0012 ;this is another comment;\nl1 : SUB R10, R8, R3\nl3 : MOV R7, #-0019 ; \
not every line needs a comment ;\nl3a : MVN R8, # 4096 ; may create a \
negative number! ;\nl4 : MOV R5, R2 ;;\nl5 : CMP R2, R3 ; empty comments (see \
above) need not have spaces ;\nl6 : BEQ l11\nl7 : ADD R12, R10, #00066\nMOV \
R3, R2\n\nl10 : B l2\nl11 : HALT", $CellContext`codeRun$$ = {{
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 12}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"]}}], 61, "EQ", {0, 1, 2, 3, 4, 5, 6, 7, 8,
     9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 62, "EQ", {0, 1, 2, 3, 4, 5, 6, 
    7, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 63, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    7, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 64, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    7, 8, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 65, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    65517, 8, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 66, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 67, "EQ", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 68, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 69, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 70, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 71, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 62, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 63, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 64, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 65, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 66, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 67, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 68, "EQ", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 72, "EQ", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}, {
    SparseArray[
    Automatic, {72}, 0, {
     1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l11"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"B", "l2"}], 
       $CellContext`asmInstr["HALT"], 11}}], 0, "EQ", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 61437, 11, 71}}}, $CellContext`memoryRules$$ = {{
   61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> "SUB R10 , R8 , R3", 64 -> 
    "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> "MOV R5 , R2", 67 -> 
    "CMP R2 , R3", 68 -> "BEQ l11", 69 -> "ADD R12 , R10 , # 66", 70 -> 
    "MOV R3 , R2", 71 -> "B l2", 72 -> "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "SUB R10 , R8 , R3", 64 -> "MOV R7 , # -19", 65 -> "MVN R8 , # 4096", 66 -> 
    "MOV R5 , R2", 67 -> "CMP R2 , R3", 68 -> "BEQ l11", 69 -> 
    "ADD R12 , R10 , # 66", 70 -> "MOV R3 , R2", 71 -> "B l2", 72 -> 
    "HALT"}}, $CellContext`len$$ = 
  20, $CellContext`keys$$ = {{61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
   72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63,
    64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 
   69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10,
    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 
   66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 
   71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62,
    63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 
   68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
   72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63,
    64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 
   69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10,
    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 
   66, 67, 68, 69, 70, 71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 
   71, 72}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
   72}}, $CellContext`pcList$$ = {61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
  62, 63, 64, 65, 66, 67, 68, 72, 0}, $CellContext`statusList$$ = {
  "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "LT", "LT", "LT", "LT", "LT", 
   "LT", "LT", "LT", "LT", "LT", "EQ", "EQ", 
   "EQ"}, $CellContext`registerList$$ = CompressedData["
1:eJxTTMoPymNiYGAQAWJeBghgBGKQGDMQswAxKxCzATE7EHMAMScQcwExNxDz
UKCHgUZ6WInU8/Y/eXr+vydOD9MQ1eOOpodpiOr5+35UD6V6AMTGUfc=
  "], $CellContext`memories$$ = {
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 12}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"]}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {72}, 0, {
    1, {{0, 13}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l11"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"B", "l2"}], 
      $CellContext`asmInstr["HALT"], 11}}]}, $CellContext`showCode$$ = False}, 
  TagBox[GridBox[{
     {
      PanelBox["\<\"Assembler version 2.21\"\>"]},
     {
      PanelBox[
       DynamicBox[ToBoxes[
         If[$CellContext`showCode$$, $CellContext`code$$, "Code hidden"], 
         StandardForm],
        ImageSizeCache->{102., {1., 14.}}]]},
     {
      TagBox[
       StyleBox[
        DynamicModuleBox[{$CellContext`format$$ = "signed", $CellContext`i$$ =
          1, $CellContext`lim$$ = 100, $CellContext`offset$$ = 50, 
         Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
         Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, 
         Typeset`animvar$$ = 1, Typeset`name$$ = "\"untitled\"", 
         Typeset`specs$$ = {{{
            Hold[$CellContext`offset$$], 50, "memory start"}, 0, 512, 1}, {{
            Hold[$CellContext`lim$$], 100, "computation limit"}, 1, 10000, 
           10}, {{
            Hold[$CellContext`i$$], 1, "computation step"}, 1, 
           Dynamic[$CellContext`len$$], 1}, {{
            Hold[$CellContext`format$$], "signed", "format"}, {
           "signed", "2's complement"}}}, Typeset`size$$ = {
         720., {297., 306.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
         Typeset`skipInitDone$$ = False, $CellContext`offset$2732$$ = 
         0, $CellContext`lim$2737$$ = 0, $CellContext`i$2738$$ = 
         0, $CellContext`format$2739$$ = False}, 
         DynamicBox[Manipulate`ManipulateBoxes[
          1, StandardForm, 
           "Variables" :> {$CellContext`format$$ = "signed", $CellContext`i$$ = 
             1, $CellContext`lim$$ = 100, $CellContext`offset$$ = 50}, 
           "ControllerVariables" :> {
             Hold[$CellContext`offset$$, $CellContext`offset$2732$$, 0], 
             Hold[$CellContext`lim$$, $CellContext`lim$2737$$, 0], 
             Hold[$CellContext`i$$, $CellContext`i$2738$$, 0], 
             Hold[$CellContext`format$$, $CellContext`format$2739$$, False]}, 
           "OtherVariables" :> {
            Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
             Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
             Typeset`specs$$, Typeset`size$$, Typeset`update$$, 
             Typeset`initDone$$, Typeset`skipInitDone$$}, "Body" :> Column[{
              Row[{
                Button[
                "Reload code", $CellContext`code$$ = 
                  "STR R11, 0010 ;this is a comment and is terminated by: ;\n\
l2 : LDR R1, 0012 ;this is another comment;\nl1 : SUB R10, R8, R3\nl3 : MOV \
R7, #-0019 ; not every line needs a comment ;\nl3a : MVN R8, # 4096 ; may \
create a negative number! ;\nl4 : MOV R5, R2 ;;\nl5 : CMP R2, R3 ; empty \
comments (see above) need not have spaces ;\nl6 : BEQ l11\nl7 : ADD R12, R10, \
#00066\nMOV R3, R2\n\nl10 : B l2\nl11 : HALT"; $CellContext`codeRun$$ = \
$CellContext`asLoadRunDecoded[$CellContext`code$$, $CellContext`offset$$, \
$CellContext`lim$$, 16]; $CellContext`len$$ = 
                  Length[$CellContext`codeRun$$]; {$CellContext`memories$$, \
$CellContext`pcList$$, $CellContext`statusList$$, \
$CellContext`registerList$$} = 
                  Transpose[$CellContext`codeRun$$]; \
$CellContext`memoryRules$$ = Map[Drop[
                    
                    ArrayRules[#], -1]& , $CellContext`memories$$]; \
$CellContext`memoryRules$$ = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                    
                    Part[#, 
                    2], $CellContext`format$$]& , #]& , \
$CellContext`memoryRules$$]; $CellContext`memoryRules$$ = 
                  Map[
                   SortBy[#, 
                    Part[#, 
                    1]& ]& , $CellContext`memoryRules$$]; $CellContext`keys$$ = 
                  Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules$$]; 
                 Null], 
                Button["Show code", $CellContext`showCode$$ = True], 
                Button["Hide code", $CellContext`showCode$$ = False]}], 
              TabView[{"base 10" -> Column[{
                   Grid[{{
                    Text["Memory"], 
                    Text["PC"], 
                    Text["Status"], 
                    Text["CIR"]}, {
                    TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys$$, $CellContext`i$$], 
                    Normal[
                    Part[$CellContext`memoryRules$$, $CellContext`i$$]]]], 
                    Part[$CellContext`pcList$$, $CellContext`i$$], 
                    Appearance -> {"Limited", 10}], 
                    Text[
                    Part[$CellContext`pcList$$, $CellContext`i$$]], 
                    Text[
                    Part[$CellContext`statusList$$, $CellContext`i$$]], 
                    Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories$$, $CellContext`i$$], 
                    
                    Part[$CellContext`pcList$$, $CellContext`i$$]], \
$CellContext`format$$]]}}], 
                   Grid[{{
                    Text["Registers"]}, {
                    Text[
                    
                    Grid[{{$CellContext`R0, $CellContext`R1, $CellContext`R2, \
$CellContext`R3, $CellContext`R4, $CellContext`R5, $CellContext`R6, \
$CellContext`R7, $CellContext`R8, $CellContext`R9, $CellContext`R10, \
$CellContext`R11, $CellContext`R12}, 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    
                    Part[$CellContext`registerList$$, \
$CellContext`i$$]]}]]}}]}], "base 2" -> Column[{
                   Grid[{{
                    Text["Memory"], 
                    Text["PC"], 
                    Text["Status"], 
                    Text["CIR"]}, {
                    TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys$$, $CellContext`i$$], 
                    Normal[
                    Part[$CellContext`memoryRules$$, $CellContext`i$$]]]], 
                    Part[$CellContext`pcList$$, $CellContext`i$$], 
                    Appearance -> {"Limited", 10}], 
                    Text[
                    Part[$CellContext`pcList$$, $CellContext`i$$]], 
                    Text[
                    Part[$CellContext`statusList$$, $CellContext`i$$]], 
                    Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories$$, $CellContext`i$$], 
                    
                    Part[$CellContext`pcList$$, $CellContext`i$$]], \
$CellContext`format$$]]}}], 
                   Grid[{{
                    Text["Registers"]}, {
                    Text[
                    Grid[
                    
                    Transpose[{{$CellContext`R0, $CellContext`R1, \
$CellContext`R2, $CellContext`R3, $CellContext`R4, $CellContext`R5, \
$CellContext`R6, $CellContext`R7, $CellContext`R8, $CellContext`R9, \
$CellContext`R10, $CellContext`R11, $CellContext`R12}, 
                    Map[$CellContext`bf, 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList$$, $CellContext`i$$]]], 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList$$, $CellContext`i$$]]}], 
                    Alignment -> Right]]}}]}]}, 
               Dynamic[$CellContext`tab$$]]}], 
           "Specifications" :> {{{$CellContext`offset$$, 50, "memory start"}, 
              0, 512, 1}, {{$CellContext`lim$$, 100, "computation limit"}, 1, 
              10000, 10}, {{$CellContext`i$$, 1, "computation step"}, 1, 
              Dynamic[$CellContext`len$$], 1, Appearance -> 
              "Open"}, {{$CellContext`format$$, "signed", "format"}, {
              "signed", "2's complement"}}}, "Options" :> {}, 
           "DefaultOptions" :> {}],
          ImageSizeCache->{792., {432., 441.}},
          SingleEvaluation->True],
         Deinitialization:>None,
         DynamicModuleValues:>{},
         Initialization:>(($CellContext`code$$ = 
            "STR R11, 0010 ;this is a comment and is terminated by: ;\nl2 : \
LDR R1, 0012 ;this is another comment;\nl1 : SUB R10, R8, R3\nl3 : MOV R7, \
#-0019 ; not every line needs a comment ;\nl3a : MVN R8, # 4096 ; may create \
a negative number! ;\nl4 : MOV R5, R2 ;;\nl5 : CMP R2, R3 ; empty comments \
(see above) need not have spaces ;\nl6 : BEQ l11\nl7 : ADD R12, R10, #00066\n\
MOV R3, R2\n\nl10 : B l2\nl11 : HALT"; $CellContext`codeRun$$ = \
$CellContext`asLoadRunDecoded[$CellContext`code$$, 60, $CellContext`lim$$, 
              16]; $CellContext`len$$ = 
            Length[$CellContext`codeRun$$]; {$CellContext`memories$$, \
$CellContext`pcList$$, $CellContext`statusList$$, \
$CellContext`registerList$$} = 
            Transpose[$CellContext`codeRun$$]; $CellContext`memoryRules$$ = 
            Map[Drop[
               
               ArrayRules[#], -1]& , $CellContext`memories$$]; \
$CellContext`memoryRules$$ = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                 
                 Part[#, 
                  2], $CellContext`format$$]& , #]& , \
$CellContext`memoryRules$$]; $CellContext`memoryRules$$ = 
            Map[SortBy[#, 
               Part[#, 
                1]& ]& , $CellContext`memoryRules$$]; $CellContext`keys$$ = 
            Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules$$]; Null); 
          Typeset`initDone$$ = True),
         SynchronousInitialization->True,
         UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
         UnsavedVariables:>{Typeset`initDone$$},
         UntrackedVariables:>{Typeset`size$$}], "Manipulate",
        Deployed->True,
        StripOnInput->False],
       Manipulate`InterpretManipulate[1]]}
    },
    DefaultBaseStyle->"Column",
    GridBoxAlignment->{"Columns" -> {{Left}}},
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
   "Column"],
  DynamicModuleValues:>{}]], "Output"]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["Example 2", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "code2", "=", "\[IndentingNewLine]", 
   "\"\<MOV R2 , # 10\nMOV R1 , # 0\nMOV R12 , # 999\nloop : ADD R1 , R1 , # \
1\nCMP R1 , R2\nBNE loop\nHALT\>\""}], ";"}]], "Input",
 InitializationCell->True]
}, Closed]],

Cell[CellGroupData[{

Cell["Example 3: Design of a counter/timer", "Subsubsection"],

Cell["\<\
Rough notes towards a design: use R1 to hold the incremented value
Use R2 to hold the wait value; assume in memory
(we could have used # immediate addressing)
loop until R1 = R2
; initialization
loop : ADD R1 R1 # 1 ; increment value in R1
CMP R1 R2 ; compare R1 with R2
BEQ finish ;  stop if R1 = R2
B loop ; otherwise go back to loop step
finish : HALT\
\>", "Text"]
}, Closed]],

Cell[CellGroupData[{

Cell["Final version of counter/timer", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "code4", "=", "\[IndentingNewLine]", 
   "\"\<MOV R2 , # 10\nMOV R1 , # 0\nloop : ADD R1 , R1 , # 1\nCMP R1 , R2\n\
BNE loop\nHALT\>\""}], ";"}]], "Input",
 InitializationCell->True],

Cell[BoxData[
 RowBox[{"runMyProgram", "[", "code4", "]"}]], "Input"],

Cell[TextData[{
 "Test below ",
 StyleBox["fails",
  FontSlant->"Italic"],
 " if more than one step is executed, and breaks if code is stepped through \
to the second \[OpenCurlyDoubleQuote]instruction\[CloseCurlyDoubleQuote].  \
This is basically an \[OpenCurlyDoubleQuote]out of memory\
\[CloseCurlyDoubleQuote] crash, compounded by the fact that there is no ",
 StyleBox["HALT", "Input"],
 " instruction so the machine tries to execute the next instruction when \
there is none.  An object lesson in how (not) to fail gracefully.  The moral \
is: always include a ",
 StyleBox["HALT", "Input"],
 " instruction at the end of your code."
}], "Text"]
}, Closed]],

Cell[CellGroupData[{

Cell["More sophisticated memory addressing", "Subsubsection"],

Cell["\<\
The code below accesses memory locations which are stored in registers.  This \
allows us to actually do useful things with memory.\
\>", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "code5", "=", 
   "\"\<STR R11 , 0010 ; this is a comment and is terminated by: ;\nl2 : LDR \
R1 , 0012 ; this is another comment: note spaces! ;\nl2a : MOV R7 , # 10 ; \
prep the 10th memory address in R7 ;\nl2b : LDR R11 , R7 ; read its contents \
into R11 ;\n12c : STR R8 , R7 ; store R8 into the location ;\nl1 : SUB R10 , \
R8 , R3\nl3 : MOV R7 , # -0019 ; not every line needs a comment ;\nl3a : MVN \
R8 , # 4096 ; may create a negative number! ;\nl4 : MOV R5 , R2 ; ;\nl5 : CMP \
R2 , R3 ; empty comments (see above) must also have spaces ;\nl6 : BEQ l10\n\
l7 : ADD R12 , R10 , # 00066\nMOV R3 , R2\nHALT\>\""}], ";"}]], "Input",
 InitializationCell->True],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"runMyProgram", "[", 
  RowBox[{"code5", ",", "60", ",", " ", "100"}], "]"}]], "Input",
 InitializationCell->True],

Cell[BoxData[
 DynamicModuleBox[{$CellContext`tab$$ = 1, $CellContext`code$$ = 
  "STR R11 , 0010 ; this is a comment and is terminated by: ;\nl2 : LDR R1 , \
0012 ; this is another comment: note spaces! ;\nl2a : MOV R7 , # 10 ; prep \
the 10th memory address in R7 ;\nl2b : LDR R11 , R7 ; read its contents into \
R11 ;\n12c : STR R8 , R7 ; store R8 into the location ;\nl1 : SUB R10 , R8 , \
R3\nl3 : MOV R7 , # -0019 ; not every line needs a comment ;\nl3a : MVN R8 , \
# 4096 ; may create a negative number! ;\nl4 : MOV R5 , R2 ; ;\nl5 : CMP R2 , \
R3 ; empty comments (see above) must also have spaces ;\nl6 : BEQ l10\nl7 : \
ADD R12 , R10 , # 00066\nMOV R3 , R2\nHALT", $CellContext`codeRun$$ = {{
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 14}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"]}}], 61, "EQ", {0, 1, 2, 3, 4, 5, 6, 7, 8,
     9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 11}}], 62, "EQ", {0, 1, 2, 3, 4, 5, 6, 
    7, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 11}}], 63, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    7, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 11}}], 64, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    10, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 11}}], 65, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    10, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 66, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    10, 8, 9, 10, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 67, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    10, 8, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 68, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    65517, 8, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 69, "EQ", {0, 0, 2, 3, 4, 5, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 70, "EQ", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 71, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 72, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 12}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 73, "LT", {0, 0, 2, 3, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 74, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}, {
    SparseArray[
    Automatic, {74}, 0, {
     1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
       70}, {71}, {72}, {73}, {74}, {10}}}, {
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[10]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[1], 
          $CellContext`asmMemRef[12]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[10]]}}], 
       $CellContext`asmInstr[{"LDR", {
          $CellContext`asmReg[11], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"STR", {
          $CellContext`asmReg[8], 
          $CellContext`asmMemRef[
           $CellContext`asmReg[7]]}}], 
       $CellContext`asmInstr[{"SUB", {
          $CellContext`asmReg[10], {
           $CellContext`asmReg[8], 
           $CellContext`asmOperand[
            $CellContext`asmReg[3]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[7], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[65517]]}}], 
       $CellContext`asmInstr[{"MVN", {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[4096]]}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[5], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr[{"CMP", {
          $CellContext`asmReg[2], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}], 
       $CellContext`asmInstr[{"BEQ", "l10"}], 
       $CellContext`asmInstr[{"ADD", {
          $CellContext`asmReg[12], {
           $CellContext`asmReg[10], 
           $CellContext`asmOperand[
            $CellContext`asmLiteral[66]]}}}], 
       $CellContext`asmInstr[{"MOV", {
          $CellContext`asmReg[3], 
          $CellContext`asmOperand[
           $CellContext`asmReg[2]]}}], 
       $CellContext`asmInstr["HALT"], 8}}], 0, "LT", {0, 0, 2, 2, 4, 2, 6, 
    65517, 61439, 9, 5, 11, 71}}}, $CellContext`memoryRules$$ = {{
   61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> "MOV R7 , # 10", 64 -> 
    "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> "SUB R10 , R8 , R3", 67 -> 
    "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> "MOV R5 , R2", 70 -> 
    "CMP R2 , R3", 71 -> "BEQ l10", 72 -> "ADD R12 , R10 , # 66", 73 -> 
    "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "11", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> "HALT"}, {
   10 -> "8", 61 -> "STR R11 , 10", 62 -> "LDR R1 , 12", 63 -> 
    "MOV R7 , # 10", 64 -> "LDR R11 , R7", 65 -> "STR R8 , R7", 66 -> 
    "SUB R10 , R8 , R3", 67 -> "MOV R7 , # -19", 68 -> "MVN R8 , # 4096", 69 -> 
    "MOV R5 , R2", 70 -> "CMP R2 , R3", 71 -> "BEQ l10", 72 -> 
    "ADD R12 , R10 , # 66", 73 -> "MOV R3 , R2", 74 -> 
    "HALT"}}, $CellContext`len$$ = 
  15, $CellContext`keys$$ = {{61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 
   73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10,
    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 
   64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 
   67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 
   70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 
   73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10,
    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 
   64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 
   67, 68, 69, 70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 
   70, 71, 72, 73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 
   73, 74}, {10, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74}, {10,
    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 
   74}}, $CellContext`pcList$$ = {61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
  72, 73, 74, 0}, $CellContext`statusList$$ = {
  "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "EQ", "LT", "LT", 
   "LT", "LT", "LT"}, $CellContext`registerList$$ = CompressedData["
1:eJxTTMoPymNiYGDgB2JeBghgBGKQGDMQswAxKxCzATE7EHMAMScQcwExNxDz
UKCHgYp6uIaIHlYi9bz9T56e/++J08M0RPW4o+lhoqEeALoPI0Q=
  "], $CellContext`memories$$ = {
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 14}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"]}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 11}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}], 
   SparseArray[
   Automatic, {74}, 0, {
    1, {{0, 15}, {{61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {
      70}, {71}, {72}, {73}, {74}, {10}}}, {
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[10]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[1], 
         $CellContext`asmMemRef[12]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[10]]}}], 
      $CellContext`asmInstr[{"LDR", {
         $CellContext`asmReg[11], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"STR", {
         $CellContext`asmReg[8], 
         $CellContext`asmMemRef[
          $CellContext`asmReg[7]]}}], 
      $CellContext`asmInstr[{"SUB", {
         $CellContext`asmReg[10], {
          $CellContext`asmReg[8], 
          $CellContext`asmOperand[
           $CellContext`asmReg[3]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[7], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[65517]]}}], 
      $CellContext`asmInstr[{"MVN", {
         $CellContext`asmReg[8], 
         $CellContext`asmOperand[
          $CellContext`asmLiteral[4096]]}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[5], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr[{"CMP", {
         $CellContext`asmReg[2], 
         $CellContext`asmOperand[
          $CellContext`asmReg[3]]}}], 
      $CellContext`asmInstr[{"BEQ", "l10"}], 
      $CellContext`asmInstr[{"ADD", {
         $CellContext`asmReg[12], {
          $CellContext`asmReg[10], 
          $CellContext`asmOperand[
           $CellContext`asmLiteral[66]]}}}], 
      $CellContext`asmInstr[{"MOV", {
         $CellContext`asmReg[3], 
         $CellContext`asmOperand[
          $CellContext`asmReg[2]]}}], 
      $CellContext`asmInstr["HALT"], 8}}]}, $CellContext`showCode$$ = False}, 
  TagBox[GridBox[{
     {
      PanelBox["\<\"Assembler version 2.21\"\>"]},
     {
      PanelBox[
       DynamicBox[ToBoxes[
         If[$CellContext`showCode$$, $CellContext`code$$, "Code hidden"], 
         StandardForm],
        ImageSizeCache->{102., {1., 14.}}]]},
     {
      TagBox[
       StyleBox[
        DynamicModuleBox[{$CellContext`format$$ = "signed", $CellContext`i$$ =
          1, $CellContext`lim$$ = 100, $CellContext`offset$$ = 50, 
         Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
         Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, 
         Typeset`animvar$$ = 1, Typeset`name$$ = "\"untitled\"", 
         Typeset`specs$$ = {{{
            Hold[$CellContext`offset$$], 50, "memory start"}, 0, 512, 1}, {{
            Hold[$CellContext`lim$$], 100, "computation limit"}, 1, 10000, 
           10}, {{
            Hold[$CellContext`i$$], 1, "computation step"}, 1, 
           Dynamic[$CellContext`len$$], 1}, {{
            Hold[$CellContext`format$$], "signed", "format"}, {
           "signed", "2's complement"}}}, Typeset`size$$ = {
         720., {297., 306.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
         Typeset`skipInitDone$$ = False, $CellContext`offset$2782$$ = 
         0, $CellContext`lim$2783$$ = 0, $CellContext`i$2784$$ = 
         0, $CellContext`format$2785$$ = False}, 
         DynamicBox[Manipulate`ManipulateBoxes[
          1, StandardForm, 
           "Variables" :> {$CellContext`format$$ = "signed", $CellContext`i$$ = 
             1, $CellContext`lim$$ = 100, $CellContext`offset$$ = 50}, 
           "ControllerVariables" :> {
             Hold[$CellContext`offset$$, $CellContext`offset$2782$$, 0], 
             Hold[$CellContext`lim$$, $CellContext`lim$2783$$, 0], 
             Hold[$CellContext`i$$, $CellContext`i$2784$$, 0], 
             Hold[$CellContext`format$$, $CellContext`format$2785$$, False]}, 
           "OtherVariables" :> {
            Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
             Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
             Typeset`specs$$, Typeset`size$$, Typeset`update$$, 
             Typeset`initDone$$, Typeset`skipInitDone$$}, "Body" :> Column[{
              Row[{
                Button[
                "Reload code", $CellContext`code$$ = 
                  "STR R11 , 0010 ; this is a comment and is terminated by: ;\
\nl2 : LDR R1 , 0012 ; this is another comment: note spaces! ;\nl2a : MOV R7 \
, # 10 ; prep the 10th memory address in R7 ;\nl2b : LDR R11 , R7 ; read its \
contents into R11 ;\n12c : STR R8 , R7 ; store R8 into the location ;\nl1 : \
SUB R10 , R8 , R3\nl3 : MOV R7 , # -0019 ; not every line needs a comment ;\n\
l3a : MVN R8 , # 4096 ; may create a negative number! ;\nl4 : MOV R5 , R2 ; ;\
\nl5 : CMP R2 , R3 ; empty comments (see above) must also have spaces ;\nl6 : \
BEQ l10\nl7 : ADD R12 , R10 , # 00066\nMOV R3 , R2\nHALT"; \
$CellContext`codeRun$$ = $CellContext`asLoadRunDecoded[$CellContext`code$$, \
$CellContext`offset$$, $CellContext`lim$$, 16]; $CellContext`len$$ = 
                  Length[$CellContext`codeRun$$]; {$CellContext`memories$$, \
$CellContext`pcList$$, $CellContext`statusList$$, \
$CellContext`registerList$$} = 
                  Transpose[$CellContext`codeRun$$]; \
$CellContext`memoryRules$$ = Map[Drop[
                    
                    ArrayRules[#], -1]& , $CellContext`memories$$]; \
$CellContext`memoryRules$$ = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                    
                    Part[#, 
                    2], $CellContext`format$$]& , #]& , \
$CellContext`memoryRules$$]; $CellContext`memoryRules$$ = 
                  Map[SortBy[#, 
                    Part[#, 
                    1]& ]& , $CellContext`memoryRules$$]; $CellContext`keys$$ = 
                  Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules$$]; 
                 Null], 
                Button["Show code", $CellContext`showCode$$ = True], 
                Button["Hide code", $CellContext`showCode$$ = False]}], 
              TabView[{"base 10" -> Column[{
                   Grid[{{
                    Text["Memory"], 
                    Text["PC"], 
                    Text["Status"], 
                    Text["CIR"]}, {
                    TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys$$, $CellContext`i$$], 
                    Normal[
                    Part[$CellContext`memoryRules$$, $CellContext`i$$]]]], 
                    Part[$CellContext`pcList$$, $CellContext`i$$], 
                    Appearance -> {"Limited", 10}], 
                    Text[
                    Part[$CellContext`pcList$$, $CellContext`i$$]], 
                    Text[
                    Part[$CellContext`statusList$$, $CellContext`i$$]], 
                    Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories$$, $CellContext`i$$], 
                    
                    Part[$CellContext`pcList$$, $CellContext`i$$]], \
$CellContext`format$$]]}}], 
                   Grid[{{
                    Text["Registers"]}, {
                    Text[
                    
                    Grid[{{$CellContext`R0, $CellContext`R1, $CellContext`R2, \
$CellContext`R3, $CellContext`R4, $CellContext`R5, $CellContext`R6, \
$CellContext`R7, $CellContext`R8, $CellContext`R9, $CellContext`R10, \
$CellContext`R11, $CellContext`R12}, 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    
                    Part[$CellContext`registerList$$, \
$CellContext`i$$]]}]]}}]}], "base 2" -> Column[{
                   Grid[{{
                    Text["Memory"], 
                    Text["PC"], 
                    Text["Status"], 
                    Text["CIR"]}, {
                    TabView[
                    Thread[
                    ({#, #2}& )[
                    Part[$CellContext`keys$$, $CellContext`i$$], 
                    Normal[
                    Part[$CellContext`memoryRules$$, $CellContext`i$$]]]], 
                    Part[$CellContext`pcList$$, $CellContext`i$$], 
                    Appearance -> {"Limited", 10}], 
                    Text[
                    Part[$CellContext`pcList$$, $CellContext`i$$]], 
                    Text[
                    Part[$CellContext`statusList$$, $CellContext`i$$]], 
                    Text[
                    $CellContext`asPrint[
                    Part[
                    Part[$CellContext`memories$$, $CellContext`i$$], 
                    
                    Part[$CellContext`pcList$$, $CellContext`i$$]], \
$CellContext`format$$]]}}], 
                   Grid[{{
                    Text["Registers"]}, {
                    Text[
                    Grid[
                    
                    Transpose[{{$CellContext`R0, $CellContext`R1, \
$CellContext`R2, $CellContext`R3, $CellContext`R4, $CellContext`R5, \
$CellContext`R6, $CellContext`R7, $CellContext`R8, $CellContext`R9, \
$CellContext`R10, $CellContext`R11, $CellContext`R12}, 
                    Map[$CellContext`bf, 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList$$, $CellContext`i$$]]], 
                    Map[If[$CellContext`format$$ == "signed", 
                    $CellContext`from2sComplement[#, \
$CellContext`$wordLength], #]& , 
                    Part[$CellContext`registerList$$, $CellContext`i$$]]}], 
                    Alignment -> Right]]}}]}]}, 
               Dynamic[$CellContext`tab$$]]}], 
           "Specifications" :> {{{$CellContext`offset$$, 50, "memory start"}, 
              0, 512, 1}, {{$CellContext`lim$$, 100, "computation limit"}, 1, 
              10000, 10}, {{$CellContext`i$$, 1, "computation step"}, 1, 
              Dynamic[$CellContext`len$$], 1, Appearance -> 
              "Open"}, {{$CellContext`format$$, "signed", "format"}, {
              "signed", "2's complement"}}}, "Options" :> {}, 
           "DefaultOptions" :> {}],
          ImageSizeCache->{792., {432., 441.}},
          SingleEvaluation->True],
         Deinitialization:>None,
         DynamicModuleValues:>{},
         Initialization:>(($CellContext`code$$ = 
            "STR R11 , 0010 ; this is a comment and is terminated by: ;\nl2 : \
LDR R1 , 0012 ; this is another comment: note spaces! ;\nl2a : MOV R7 , # 10 \
; prep the 10th memory address in R7 ;\nl2b : LDR R11 , R7 ; read its \
contents into R11 ;\n12c : STR R8 , R7 ; store R8 into the location ;\nl1 : \
SUB R10 , R8 , R3\nl3 : MOV R7 , # -0019 ; not every line needs a comment ;\n\
l3a : MVN R8 , # 4096 ; may create a negative number! ;\nl4 : MOV R5 , R2 ; ;\
\nl5 : CMP R2 , R3 ; empty comments (see above) must also have spaces ;\nl6 : \
BEQ l10\nl7 : ADD R12 , R10 , # 00066\nMOV R3 , R2\nHALT"; \
$CellContext`codeRun$$ = $CellContext`asLoadRunDecoded[$CellContext`code$$, 
              60, $CellContext`lim$$, 16]; $CellContext`len$$ = 
            Length[$CellContext`codeRun$$]; {$CellContext`memories$$, \
$CellContext`pcList$$, $CellContext`statusList$$, \
$CellContext`registerList$$} = 
            Transpose[$CellContext`codeRun$$]; $CellContext`memoryRules$$ = 
            Map[Drop[
               
               ArrayRules[#], -1]& , $CellContext`memories$$]; \
$CellContext`memoryRules$$ = Map[Map[Part[#, 1, 1] -> $CellContext`asPrint[
                 
                 Part[#, 
                  2], $CellContext`format$$]& , #]& , \
$CellContext`memoryRules$$]; $CellContext`memoryRules$$ = 
            Map[SortBy[#, 
               Part[#, 
                1]& ]& , $CellContext`memoryRules$$]; $CellContext`keys$$ = 
            Map[Map[Part[#, 1]& , #]& , $CellContext`memoryRules$$]; Null); 
          Typeset`initDone$$ = True),
         SynchronousInitialization->True,
         UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
         UnsavedVariables:>{Typeset`initDone$$},
         UntrackedVariables:>{Typeset`size$$}], "Manipulate",
        Deployed->True,
        StripOnInput->False],
       Manipulate`InterpretManipulate[1]]}
    },
    DefaultBaseStyle->"Column",
    GridBoxAlignment->{"Columns" -> {{Left}}},
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
   "Column"],
  DynamicModuleValues:>{}]], "Output"]
}, Open  ]]
}, Closed]]
}, Closed]]
}, Open  ]],

Cell[CellGroupData[{

Cell["2-s complement arithmetic", "Section"],

Cell[CellGroupData[{

Cell["Notes", "Subsection"],

Cell["\<\
We want to mimic typical chip design constraints on wordsize and the \
arithmetic issues of over (or under-) flow that arise from this.  Mathematica \
has a large word size (in fact, unlimited!) but we want to explore the issues \
of word size using a much smaller value to make these issues clearer.\
\>", "Text"],

Cell[CellGroupData[{

Cell["Limitations for Mathematica users", "Subsubsection"],

Cell[TextData[{
 "We cannot use ",
 StyleBox["BaseForm", "Input"],
 " output as input"
}], "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ToExpression", "[", 
  SubscriptBox["\"\<1111111111\>\"", "\"\<2\>\""], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"ToExpression", "::", "notstrbox"}], "MessageName"], ":", 
  " ", "\<\"\[NoBreak]\\!\\(\\*SubscriptBox[\\\"\\\\\\\"1111111111\\\\\\\"\\\"\
, \\\"\\\\\\\"2\\\\\\\"\\\"]\\)\[NoBreak] is not a string or a box. \
ToExpression can only interpret strings or boxes as Wolfram Language input. \
\\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", \
ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/ToExpression/notstrbox\\\", ButtonNote -> \
\\\"ToExpression::notstrbox\\\"]\\)\"\>"}]], "Message", "MSG"],

Cell[BoxData["$Failed"], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ToString", "[", 
   RowBox[{"BaseForm", "[", 
    RowBox[{
     RowBox[{
      SuperscriptBox["2", "10"], "-", "1"}], ",", "2"}], "]"}], "]"}], "//", 
  "FullForm"}]], "Input"],

Cell[BoxData[
 TagBox[
  StyleBox["\"\<1111111111\\n          2\>\"",
   ShowSpecialCharacters->False,
   ShowStringCharacters->True,
   NumberMarks->True],
  FullForm]], "Output"]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Open  ]]
},
AutoGeneratedPackage->Automatic,
WindowSize->{1424, 842},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
TrackCellChangeTimes->False,
Magnification:>1.5 Inherited,
FrontEndVersion->"11.0 for Microsoft Windows (64-bit) (September 21, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 46, 0, 150, "Title"],
Cell[CellGroupData[{
Cell[1557, 39, 59, 0, 108, "Chapter"],
Cell[1619, 41, 105, 3, 45, "Text"],
Cell[CellGroupData[{
Cell[1749, 48, 40, 0, 58, "Subsubsection"],
Cell[CellGroupData[{
Cell[1814, 52, 91, 2, 65, "Code",
 InitializationCell->True],
Cell[1908, 56, 105, 1, 45, "Output"]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[2062, 63, 35, 0, 47, "Subsubsection"],
Cell[CellGroupData[{
Cell[2122, 67, 63, 1, 76, "Code"],
Cell[2188, 70, 128, 1, 45, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[2365, 77, 48, 0, 58, "Subsubsection"],
Cell[2416, 79, 45, 0, 38, "Text"],
Cell[2464, 81, 360, 10, 105, "Code",
 InitializationCell->True],
Cell[2827, 93, 86, 2, 76, "Code"],
Cell[2916, 97, 84, 2, 76, "Code"]
}, Closed]],
Cell[CellGroupData[{
Cell[3037, 104, 74, 0, 47, "Subsubsection"],
Cell[3114, 106, 158, 3, 45, "Text"],
Cell[CellGroupData[{
Cell[3297, 113, 293, 7, 45, "Input"],
Cell[3593, 122, 243, 7, 61, "Output"]
}, {2}]],
Cell[3848, 132, 134, 5, 45, "Text"],
Cell[3985, 139, 87, 2, 45, "Text"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4109, 146, 31, 0, 106, "Section"],
Cell[4143, 148, 140, 3, 45, "Text"],
Cell[CellGroupData[{
Cell[4308, 155, 87, 1, 58, "Subsubsection"],
Cell[4398, 158, 61, 0, 45, "Text"],
Cell[4462, 160, 316, 5, 74, "Text"],
Cell[4781, 167, 65, 0, 45, "Text"],
Cell[4849, 169, 413, 7, 102, "Text"]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[5323, 183, 25, 0, 108, "Chapter"],
Cell[CellGroupData[{
Cell[5373, 187, 52, 1, 83, "Section",
 InitializationGroup->True],
Cell[CellGroupData[{
Cell[5450, 192, 38, 0, 47, "Subsubsection"],
Cell[CellGroupData[{
Cell[5513, 196, 163, 3, 94, "Code"],
Cell[5679, 201, 41, 0, 45, "Output"],
Cell[5723, 203, 105, 1, 45, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5865, 209, 103, 2, 76, "Code"],
Cell[5971, 213, 101, 1, 45, "Output"]
}, Open  ]],
Cell[6087, 217, 82, 1, 76, "Code"]
}, Closed]],
Cell[CellGroupData[{
Cell[6206, 223, 34, 0, 47, "Subsubsection"],
Cell[6243, 225, 248, 8, 58, "Input",
 InitializationCell->True],
Cell[6494, 235, 284, 8, 69, "Input",
 InitializationCell->True],
Cell[6781, 245, 1401, 36, 244, "Input",
 InitializationCell->True]
}, Closed]]
}, Closed]],
Cell[CellGroupData[{
Cell[8231, 287, 39, 0, 83, "Section"],
Cell[CellGroupData[{
Cell[8295, 291, 58, 0, 58, "Subsection"],
Cell[CellGroupData[{
Cell[8378, 295, 53, 0, 58, "Subsubsection"],
Cell[8434, 297, 733, 13, 159, "Text"],
Cell[9170, 312, 152, 3, 45, "Text"],
Cell[9325, 317, 1063, 17, 708, "Input",
 InitializationCell->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[10425, 339, 70, 0, 58, "Subsubsection"],
Cell[10498, 341, 1335, 21, 737, "Input",
 InitializationCell->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[11870, 367, 111, 2, 58, "Subsubsection"],
Cell[CellGroupData[{
Cell[12006, 373, 361, 8, 117, "Input",
 InitializationCell->True],
Cell[12370, 383, 31, 0, 45, "Output"]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[12474, 391, 44, 0, 83, "Section"],
Cell[CellGroupData[{
Cell[12543, 395, 45, 0, 58, "Subsection"],
Cell[12591, 397, 5219, 152, 710, "Input",
 InitializationCell->True],
Cell[CellGroupData[{
Cell[17835, 553, 312, 6, 162, "Input"],
Cell[18150, 561, 154, 6, 45, "Output"],
Cell[18307, 569, 30, 0, 45, "Output"],
Cell[18340, 571, 155, 6, 45, "Output"],
Cell[18498, 579, 30, 0, 45, "Output"],
Cell[18531, 581, 154, 6, 45, "Output"]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[18734, 593, 58, 0, 58, "Subsection"],
Cell[18795, 595, 3387, 96, 378, "Input",
 InitializationCell->True],
Cell[22185, 693, 38390, 1063, 4451, "Input",
 InitializationCell->True]
}, Closed]],
Cell[CellGroupData[{
Cell[60612, 1761, 78, 0, 58, "Subsection"],
Cell[60693, 1763, 17092, 380, 1712, "Input",
 InitializationCell->True]
}, Closed]]
}, Closed]]
}, Closed]],
Cell[CellGroupData[{
Cell[77846, 2150, 26, 0, 90, "Chapter"],
Cell[CellGroupData[{
Cell[77897, 2154, 43, 0, 106, "Section"],
Cell[CellGroupData[{
Cell[77965, 2158, 52, 0, 72, "Subsection"],
Cell[78020, 2160, 73, 0, 45, "Text"],
Cell[CellGroupData[{
Cell[78118, 2164, 3578, 79, 626, "Input"],
Cell[81699, 2245, 51037, 1240, 189, "Output"]
}, {2}]]
}, Open  ]],
Cell[CellGroupData[{
Cell[132782, 3491, 44, 0, 72, "Subsection"],
Cell[CellGroupData[{
Cell[132851, 3495, 34, 0, 47, "Subsubsection"],
Cell[132888, 3497, 548, 10, 447, "Input",
 InitializationCell->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[133473, 3512, 51, 0, 58, "Subsubsection"],
Cell[133527, 3514, 349, 7, 95, "Text"],
Cell[CellGroupData[{
Cell[133901, 3525, 541, 12, 161, "Code"],
Cell[134445, 3539, 85573, 2076, 1025, "Output"]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[220067, 5621, 34, 0, 47, "Subsubsection"],
Cell[220104, 5623, 236, 6, 262, "Input",
 InitializationCell->True]
}, Closed]],
Cell[CellGroupData[{
Cell[220377, 5634, 61, 0, 47, "Subsubsection"],
Cell[220441, 5636, 378, 11, 295, "Text"]
}, Closed]],
Cell[CellGroupData[{
Cell[220856, 5652, 55, 0, 47, "Subsubsection"],
Cell[220914, 5654, 219, 6, 233, "Input",
 InitializationCell->True],
Cell[221136, 5662, 69, 1, 45, "Input"],
Cell[221208, 5665, 650, 14, 131, "Text"]
}, Closed]],
Cell[CellGroupData[{
Cell[221895, 5684, 61, 0, 47, "Subsubsection"],
Cell[221959, 5686, 155, 3, 38, "Text"],
Cell[222117, 5691, 702, 12, 447, "Input",
 InitializationCell->True],
Cell[CellGroupData[{
Cell[222844, 5707, 137, 3, 69, "Input",
 InitializationCell->True],
Cell[222984, 5712, 80708, 1950, 1025, "Output"]
}, Open  ]]
}, Closed]]
}, Closed]]
}, Open  ]],
Cell[CellGroupData[{
Cell[303765, 7670, 44, 0, 106, "Section"],
Cell[CellGroupData[{
Cell[303834, 7674, 27, 0, 58, "Subsection"],
Cell[303864, 7676, 324, 5, 102, "Text"],
Cell[CellGroupData[{
Cell[304213, 7685, 58, 0, 58, "Subsubsection"],
Cell[304274, 7687, 98, 4, 38, "Text"],
Cell[CellGroupData[{
Cell[304397, 7695, 112, 2, 45, "Input"],
Cell[304512, 7699, 570, 10, 35, "Message"],
Cell[305085, 7711, 34, 0, 45, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[305156, 7716, 211, 7, 49, "Input"],
Cell[305370, 7725, 180, 6, 45, "Output"]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Closed]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature cX#ljVyoDY1BSWNOstmR3SeR *)
